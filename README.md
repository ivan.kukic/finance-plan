# Financial Planner App

An open-source financial planning application that provides monthly and yearly budget insights. It enables Yearly and
Monthly budget planning, visualization of expenses and savings, ability to generate PDF reports and setting financial
goals.

![alt text](src/assets/images/markdown/finance-plan.png "Edit Permissions")

## Table of Contents

- [Installation](#installation)
- [Features](#features)
- [Contributing](#contributing)
- [License](#license)

## Installation

**1. Setting up an NHost account and connecting it to the app:**

1. Create an NHost account on the [NHost sajtu](https://nhost.io).
2. Create a new project and name it `FinancialPlanner`.
3. Rename the  `.env.example` file to `.env` in the project root:
   ```bash
   mv .env.example .env
5. Configure the `.env` file:
   ```env
   VITE_NHOST_REGION=eu-central-1
   VITE_NHOST_SUBDOMAIN=hqshmgvmnuwkevqzimet

**2. Importing tables into the database**

1. Open the [NHost console](https://app.nhost.io).
2. Navigate to **Database** -> **SQL Editor**.
3. Use the `tables.sql` file from the project (ensure the `Track this` and `Cascade metadata` options are enabled):
   ```bash
   ./database/tables.sql
4. Select `each specific table` just created, and click in `Edit Permissions` to set the following permissions for
   the `public` role and `select` action.
   ![alt text](src/assets/images/markdown/permissions.png "Edit Permissions")

**3. Running the React application**

1. Clone the repository:
   ```bash
   git clone https://gitlab.com/ivan.kukic/finance-plan.git
2. Navigate to the project directory:
   ```bash
   cd finance-plan
3. Install the required dependencies:
   ```bash
   yarn install
4. Start the application:
   ```bash
   yarn dev
5. The app will be available at http://localhost:<your-port> (default is 5173)

## Features

#### 🚀 Comprehensive Financial Tracking

- Monitor income, expenses, and profits in real-time
- Analyze financial performance by month, quarter, and year
- Generate instant PDF reports for complete transparency

#### 💡 Smart Goal Setting

- Define and track your financial objectives
- Visualize progress with intuitive card and list views
- Set realistic targets and stay motivated

#### 📂 Document Management

- Securely upload and organize financial documents
- Easy drag-and-drop file management
- Access documents from anywhere, anytime

#### 🔒 Secure & Private

- Google and email authentication
- Email verification
- Your financial data protected with advanced security

#### 📊 Intuitive Dashboard

- Customizable year-view financial planning
- Category-based expense tracking
- Visual representations of your financial health

## Contributing

1. Fork the repository.
2. Create a new branch for your changes:
   ```bash
   git checkout -b feature/new-feature
3. Submit a Pull Request (PR) with an explanation of your changes.

## License

This project is licensed under the [MIT License](https://opensource.org/license/mit).