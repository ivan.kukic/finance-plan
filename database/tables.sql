create or replace function set_current_timestamp_updated_at()
returns trigger as $$
begin
    new.updated_at := now();
    return new;
end;
$$ language plpgsql;

create table item
(
    id                  uuid                     default gen_random_uuid()     not null
        primary key,
    type                text                     default 'Income'::text        not null,
    name                text,
    price               numeric,
    paid                boolean                  default false                 not null,
    recurring           boolean                  default false                 not null,
    "recurringStart"    timestamp with time zone,
    "recurringEnd"      timestamp with time zone,
    note                text,
    "order"             numeric,
    created_at          timestamp with time zone default now()                 not null,
    updated_at          timestamp with time zone default now()                 not null,
    users_id            uuid                     default gen_random_uuid()     not null
        references auth.users
            on update restrict on delete cascade,
    year                integer                                                not null,
    month               integer                                                not null,
    "paidDate"          timestamp with time zone,
    "payDueDate"        timestamp with time zone,
    category            text                     default 'FinancialPlan'::text not null,
    "recurringStreamId" uuid,
    "recurringOverride" boolean                  default false                 not null
);

comment on table item is 'A versatile storage item';

comment on column item.users_id is 'Foreign Key relation with users table id column';

alter table item
    owner to nhost_hasura;

create trigger set_public_item_updated_at
    before update
    on item
    for each row
execute procedure set_current_timestamp_updated_at();

comment on trigger set_public_item_updated_at on item is 'trigger to set value of column "updated_at" to current timestamp on row update';

create table license
(
    id         uuid                     default gen_random_uuid()       not null
        primary key,
    users_id   uuid                     default gen_random_uuid()       not null
        constraint license_users_id_unique
            unique
        references auth.users
            on update restrict on delete restrict,
    type       varchar                  default '50'::character varying not null,
    created_at timestamp with time zone default now()                   not null
);

alter table license
    owner to nhost_hasura;

create table storage_bucket
(
    id         uuid                     default gen_random_uuid()       not null
        constraint sorage_backet_pkey
            primary key,
    used_on    varchar                  default '50'::character varying not null,
    user_id    uuid                                                     not null,
    note       text,
    created_at timestamp with time zone default now()                   not null,
    updated_at timestamp with time zone default now()                   not null,
    name       text                                                     not null,
    parent_id  uuid
);

alter table storage_bucket
    owner to nhost_hasura;

create table storage_file
(
    id          uuid                     default gen_random_uuid()        not null
        primary key,
    used_on     varchar                  default '50'::character varying  not null,
    files_id    uuid                                                      not null,
    user_id     uuid                                                      not null
        references auth.users
            on update restrict on delete cascade,
    note        text,
    created_at  timestamp with time zone default now()                    not null,
    updated_at  timestamp with time zone default now()                    not null,
    bucket_id   text,
    name        text,
    size        integer,
    mime_type   varchar                  default '255'::character varying not null,
    etag        text,
    is_uploaded boolean                  default false                    not null,
    public_url  text                     default 'null'::text             not null
);

alter table storage_file
    owner to nhost_hasura;

create table category
(
    id          uuid                     default gen_random_uuid() not null
        primary key,
    type        varchar                                            not null,
    name        text,
    description text,
    helper_text text,
    created_at  timestamp with time zone default now()             not null,
    updated_at  timestamp with time zone default now()             not null,
    users_id    uuid                                               not null,
    is_active   boolean                  default true,
    icon        varchar,
    icon_color  varchar
);

alter table category
    owner to nhost_hasura;