## Financial Planning App Requirements

### Software Requirements Specification (SRS)

### 1. Autentifikacija i Korisnički Profil

- Registracija novog korisnika putem email-a ili Google Sign In
- Resetovanje lozinke kroz email verifikaciju
- Email verifikacija
- Login korisnika
- Ažuriranje korisničkih podataka:
    * Ime i prezime
    * Naziv firme
    * Napomene
    * Upload profilne slike/avatara
- Logout funkcionalnost

### 2. Finansijski Dashboard

- Pregled Income, Expense i Profit informacija
- Prikaz po:
    * Godini
    * Kvartalu (Q1, Q2, Q3, Q4)
- Generisanje PDF izveštaja za:
    * Trenutni mjesec
    * Godišnji izvještaj
    * Kompletan godišnji izvještaj sa svim mjesecima

### 3. Finansijski Pregled

- Tab navigacija za sve mjesece (Jan-Dec)
- Prikaz analitike za trenutni mjesec:
    * Ukupan prihod
    * Ukupni rashod
    * Neto zarada
    * Lista Income i Expense transakcija
- Mogućnost čuvanja mjesečnih PDF izveštaja

### 4. Transakcije (Items)

- Dodavanje novih transakcija
- Ažuriranje postojećih transakcija
- Brisanje transakcija

### 5. Kategorije

- Dinamičko dodavanje kategorija
- Ažuriranje kategorija
- Brisanje kategorija
- Kategorije za Income i Expense (npr. groceries, taxes, zdravstvo, najam)

### 6. Dokumenti

- Upload dokumenata
- Pregled dokumenata
- Organizacija dokumenata u foldere
- Funkcije za dokumente:
    * Preimenovanje
    * Premještanje između foldera
    * Brisanje
- Prikaz dokumenata:
    * Card View
    * List View
- Masovni upload (drag and drop)
- Masovno brisanje dokumenata/foldera

### 7. Finansijski Ciljevi (Goals)

- Postavljanje finansijskih ciljeva
- Definisanje:
    * Naziva cilja
    * Iznosa
    * Krajnjeg datuma
- Prikaz ciljeva:
    * Card View
    * List View
- Ažuriranje ciljeva
- Brisanje ciljeva

### 8. Navigacija

#### Drawer Komponenta

- Home
    * Dashboard
- Finansije
    * Finansijski Plan
    * Ciljevi
- Kategorije
- Dokumenti

### 9. Dodatne Funkcionalnosti

- Dropdown za odabir godine u headeru
- Praćenje finansija na godišnjem nivou

## Tehnički Zahtjevi

- Jednostavan i intuitivan korisnički interfejs
- Sigurna autentifikacija
- Responsivan dizajn
- Brza i efikasna pretraga i filtriranje podataka
- Mogućnost eksportovanja izveštaja u PDF formatu

## Ciljevi Aplikacije

- Omogućiti korisnicima jednostavan pregled i kontrolu sopstvenih finansija
- Olakšati praćenje prihoda, rashoda i finansijskih ciljeva
- Pružiti jasne, pregledne izveštaje za bolje finansijsko planiranje
  cial Freedom**