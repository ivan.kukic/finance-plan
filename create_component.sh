#!/bin/bash

# Check if the correct number of arguments is provided
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 /path/to/file"
    exit 1
fi

# Extract the provided path
full_path="$1"

# Extract the directory path and file name from the provided path
dir_path=$(dirname "$full_path")
file_name=$(basename "$full_path")
# Extract the last folder name from the directory path
last_folder=$(basename "$dir_path")

# Function to convert file name to uppercase and replace delimiters with spaces
convert_file_name() {
    local name="$1"
    # Convert camel case to space separated (e.g., FolderCard -> Folder Card)
    # Replace dashes and underscores with spaces (e.g., folder-card -> folder card)
    echo "$name" | sed -E 's/([a-z])([A-Z])/\1 \2/g' | sed -E 's/[_-]/ /g' | tr '[:lower:]' '[:upper:]'
}

# Convert file name
converted_file_name=$(convert_file_name "$file_name")
converted_section_name=$(convert_file_name "$last_folder")

# Create the directory if it does not exist
mkdir -p "$dir_path"

# Create the file
full_file_name="src/$full_path.tsx"

cat > "$full_file_name" <<EOF

interface I$file_name {

}

const $file_name = () => {
    return (
        <></>
    );
};

export default $file_name;
EOF

echo "Component created at: $full_file_name"
