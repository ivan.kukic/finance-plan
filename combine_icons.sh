#!/bin/bash

# Set the directory path
dir_path="./src/shared/enum/Icons"

# Initialize the merged enums
declare -A mergedIcons

# Loop through all .tsx files in the directory
for file in "$dir_path"/*.tsx; do
  # Extract the enum name from the file name
  enum_name=$(basename "$file" | sed 's/\.tsx//')

  # Initialize the enum
  declare -A enum=()

  # Read the enum from the file
  while IFS= read -r line; do
    if grep -q "^export enum $enum_name" <<< "$line"; then
      continue
    fi
    if [[ $line =~ ^  ]]; then
      key=$(echo "$line" | sed 's/  *= *//')
      value=$(echo "$line" | sed 's/  *= *//g')
      enum[$key]="$value"
    fi
  done < "$file"

  # Merge the enum into the merged enums
  for key in "${!enum[@]}"; do
    mergedIcons[$key]="${enum[$key]}"
  done
done

# Print the merged enums
echo "export enum MergedIcons {"
for key in "${!mergedIcons[@]}"; do
  echo "  $key = '${mergedIcons[$key]}';"
done
echo "}"