import { ReactNode, useMemo } from 'react';
import {
  Breakpoint,
  CssBaseline,
  Direction,
  Mixins,
  PaletteOptions,
  StyledEngineProvider,
  ThemeOptions
} from '@mui/material';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { TypographyOptions } from "@mui/material/styles/createTypography";

import Palette from './palette';
import Typography from './typography';
import CustomShadows from './shadows';
import componentsOverride from './overrides';

export interface ThemeCustomOptions extends ThemeOptions {
  breakpoints: {
    values: {
      xs: number;
      sm: number;
      md: number;
      lg: number;
      xl: number;
    }
    down?(key: number | Breakpoint): string;
  }
  direction: Direction;
  mixins: Mixins;
  palette: PaletteOptions;
  customShadows: {
    button: string;
    text: string;
    z1: string;
  };
  typography: TypographyOptions
}

const ThemeCustomization = ({children}: { children: ReactNode }) => {
  const theme = Palette('light');

  const themeTypography = Typography(`'Public Sans', sans-serif`);
  const themeCustomShadows = useMemo(() => CustomShadows(theme), [theme]);

  const themeOptions: ThemeCustomOptions = useMemo(
    () => ({
      breakpoints: {
        values: {
          xs: 0,
          sm: 768,
          md: 1024,
          lg: 1266,
          xl: 1536
        }
      },
      direction: 'ltr' as const,
      mixins: {
        toolbar: {
          minHeight: 60,
          paddingTop: 8,
          paddingBottom: 8
        }
      },
      palette: theme.palette,
      customShadows: themeCustomShadows,
      typography: themeTypography
    }),
    [theme, themeTypography, themeCustomShadows]
  );

  const themes = createTheme(themeOptions);
  themes.components = componentsOverride(themes);

  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={themes}>
        <CssBaseline />
        {children}
      </ThemeProvider>
    </StyledEngineProvider>
  );
};

export default ThemeCustomization;