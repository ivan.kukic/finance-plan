// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { Theme } from '@mui/material/styles';

declare module '@mui/material/styles' {
  interface Theme {
    customShadows: {
      light: string;
      dark: string;
      [key: string]: string;
    };
  }

  // Allow configuration using `createTheme`
  interface ThemeOptions {
    customShadows?: {
      light?: string;
      dark?: string;
      [key: string]: string;
    };
  }
}
