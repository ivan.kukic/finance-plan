export default function HelperText() {
  return {
    MuiFormHelperText: {
      styleOverrides: {
        root: {
          '&.Mui-error': {
            // color: 'red',  // Change the error color
            fontSize: '0.775rem', // Increase font size, as an example
            marginTop: 6,
          },
        },
      },
    },
  };
}
