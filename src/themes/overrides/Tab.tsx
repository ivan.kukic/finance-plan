import { Theme } from "@mui/material";

export default function Tab(theme: Theme) {
  return {
    MuiTab: {
      styleOverrides: {
        root: {
          minHeight: 36,
          color: theme.palette.text.primary,
          // '&.Mui-selected': {
          //   textDecoration: 'none',
          //   backgroundColor: '#e3f4ff',
          //   textShadow: '1px 1px 0 #fff',
          // }
        }
      }
    },
    MuiTabs: {
      styleOverrides: {
        root: {
          minHeight: 'auto',
          marginTop: 3,
        },
        // scroller: {
        //   margin: '0 8px',
        // },
        // scrollButtons: {
        //   backgroundColor: 'transparent',
        // }
        indicator: {
          display: 'none',  // This hides the Tab indicator
        }
      }
    },
    MuiButtonBase: {
      styleOverrides: {
        root: {
          '&.MuiTab-root': {
            textTransform: 'none',
            minWidth: 'auto',
            minHeight: 'auto',
            // px: 2
            padding: '4px 16px 4px 16px',
            borderRadius: 3,
            '&.Mui-selected': {
              textDecoration: 'none',
              backgroundColor: '#e3f4ff',
              textShadow: '1px 1px 0 #fff',
            }
          }
        }
      }
    },
    // // To apply media queries
    // calcComponentWidth: {
    //   width: 'calc(100% - 70px)',
    //   margin: '0 1rem',
    //   [theme.breakpoints.up('sm')]: {
    //     width: 'calc(100% - 175px - 45px - 2.5rem)',
    //     margin: '0 2rem 0 1rem',
    //   }
    // }
  }
}
