// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-expect-error
export default function Checkbox(theme) {
  return {
    MuiCheckbox: {
      styleOverrides: {
        root: {
          color: theme.palette.secondary[300]
        }
      }
    }
  };
}
