export default function Autocomplete() {
  return {
    MuiAutocomplete: {
      styleOverrides: {
        root: {
          '& .MuiInputBase-root': {},
          '& .MuiOutlinedInput-root': {},
          '& .MuiAutocomplete-input': {},
          '& .MuiAutocomplete-inputRoot[class*="MuiOutlinedInput-root"] .MuiAutocomplete-input': {
            padding: '5px 11px 4px 3px',
          },
        },
      },
    },
  };
}
