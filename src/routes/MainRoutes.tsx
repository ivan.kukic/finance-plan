import { lazy } from 'react';
import { Navigate } from "react-router-dom";

import Loadable from "@components/utility/Loadable";
import MainLayout from '@layout/main/MainLayout';
import Authenticate from "@components/auth/Authenticate";
import { useAppSelector } from "@hooks/redux";
import { selectedYear } from "@store/reducers/yearSlice";

const Dashboard = Loadable(lazy(() => import('@components/dashboard/Dashboard')));
const Finance = Loadable(lazy(() => import('@components/finance/Finance')));
const Reports = Loadable(lazy(() => import('@components/reports/Reports')));
const Documents = Loadable(lazy(() => import('@components/documents/Documents')));
const EditProfile = Loadable(lazy(() => import('@components/profile/EditProfile')));
const ChangePassword = Loadable(lazy(() => import('@components/profile/ChangePassword')));
const Categories = Loadable(lazy(() => import('@components/category/Categories')));
const EditCategory = Loadable(lazy(() => import('@components/category/EditCategory')));
const Goals = Loadable(lazy(() => import('@components/goals/Goals')));
const EditGoal = Loadable(lazy(() => import('@components/goals/EditGoal')));

const DynamicRedirect = ({url}: { url: string }) => {
  const currentYear = useAppSelector(selectedYear);
  const currentMonth = new Date().getMonth() + 1;
  return <Navigate to={`/${url}/${currentYear}/${currentMonth}`} replace />;
};

const MainRoutes = {
  path: '/',
  element: <Authenticate />,
  children: [
    {
      path: '/',
      element: <MainLayout />,
      children: [
        {path: '/', element: <DynamicRedirect url='dashboard' />},
        {path: '/dashboard', element: <DynamicRedirect url='dashboard' />},
        {path: '/dashboard/:year/:month', element: <Dashboard />},
        {path: '/finance', element: <DynamicRedirect url='finance' />},
        {path: '/finance/:year/:month', element: <Finance />},
        {path: '/analytics', element: <DynamicRedirect url='analytics' />},
        {path: '/analytics/:year/:month', element: <Reports />},
        {path: '/documents', element: <Documents />},
        {path: '/documents/bucket/:id', element: <Documents />},
        {path: '/user-profile', element: <EditProfile />},
        {path: '/user-password', element: <ChangePassword />},
        {path: '/categories', element: <Categories />},
        {path: '/category', element: <EditCategory />},
        {path: '/goals', element: <Goals />},
        {path: '/goal', element: <EditGoal />},
      ]
    }
  ]
}

export default MainRoutes;