import AuthRoutes from './AuthRoutes';
import MainRoutes from './MainRoutes';

export const ROUTES = [
  MainRoutes,
  AuthRoutes,
]