import { lazy } from 'react';

import Loadable from "@components/utility/Loadable";
import AuthLayout from "@layout/auth";
const AuthLogin = Loadable(lazy(() => import('@components/auth/Login')));
const AuthRegister = Loadable(lazy(() => import('@components/auth/Register')));
const ResetPassword = Loadable(lazy(() => import('@components/auth/ResetPassword')));
const ChangePassword = Loadable(lazy(() => import('@components/auth/ChangePassword')));

const AuthRoutes = {
  path: '/',
  element: <AuthLayout />,
  children: [
    {
      path: '/login',
      element: <AuthLogin />
    },
    {
      path: '/register',
      element: <AuthRegister />
    },
    {
      path: '/reset-password',
      element: <ResetPassword />
    },
    {
      path: '/change-password',
      element: <ChangePassword />
    }
  ]
};

export default AuthRoutes;
