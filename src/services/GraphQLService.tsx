import { gql } from '@apollo/client';

import { ItemType } from '@shared/enum/itemType';

/*******************************************************************/

export const USER_FIELDS = `
  fragment UserFields on user {
    createdAt
    defaultRole
    displayName
    email
    avatarUrl
    metadata
    isAnonymous
    locale
    roles
    emailVerified
    phoneNumber
    phoneNumberVerified
    activeMfaType
  }
`;

/*******************************************************************/

export const ITEM_FIELDS = gql/* GraphQL */`
  fragment ItemFields on item {
    category
    type
    name
    price
    paid
    paidDate
    payDueDate
    recurring
    recurringStart
    recurringEnd
    recurringStreamId
    recurringOverride
    note
    order
    created_at
    updated_at
    year
    month
  }
`;

/*******************************************************************/

export const LICENSE_FIELDS = gql/* GraphQL */`
  fragment LicenseFields on license {
    type
  }
`;

/*******************************************************************/

export const GET_USERS_GQL = `
  query GetUser($id: uuid!) {
    users(where: { id: { _eq: $id } }) {
      id
    }
  }
`;

export const GET_USERS_QUERY = `
  query GetUser($id: uuid!) {
    users(where: { id: { _eq: $id } }) {
      id
      createdAt
      defaultRole
      displayName
      email
      avatarUrl
      metadata
      isAnonymous
      locale
      emailVerified
      phoneNumber
      phoneNumberVerified
      activeMfaType
    }
  }
`;


/*******************************************************************/

export const GET_ITEMS = gql/* GraphQL */`
  ${ITEM_FIELDS}
  query GetItems($users_id: uuid) {
    item(
      where: {
        _and: [
          { users_id: { _eq: $users_id } }
        ]
      }, 
      order_by: { created_at: asc }
    ) {
      id
      ...ItemFields
      users_id
    }
  }
`;

/*******************************************************************/

export const GET_LICENSE = gql/* GraphQL */`
  ${LICENSE_FIELDS}
  query GetLicense($users_id: uuid)  {
    license(where: { users_id: { _eq: $users_id } }) {
      id
      ...LicenseFields
      created_at
      users_id
    }
  }
`;

/*******************************************************************/

export const ADD_ITEM = gql/* GraphQL */`
  ${ITEM_FIELDS}
  mutation InsertItem($item: item_insert_input!) {
    insert_item_one(object: $item) {
      id
      ...ItemFields
      users_id
    }
  }
`;


/*******************************************************************/

export const ADD_LICENSE = gql/* GraphQL */`
  ${LICENSE_FIELDS}
  mutation InsertLicense($license: license_insert_input!) {
    insert_license_one(object: $license) {
      id
      ...LicenseFields
      users_id
    }
  }
`;

/*******************************************************************/

export const UPDATE_ITEM = gql/* GraphQL */`
  ${ITEM_FIELDS}
  mutation UpdateItem($id: uuid!, $changes: item_set_input!) {
    update_item_by_pk(pk_columns: { id: $id }, _set: $changes) {
      id
      ...ItemFields
      users_id
    }
  }
`;


/*******************************************************************/

export const UPDATE_LICENSE = gql/* GraphQL */`
  ${LICENSE_FIELDS}
  mutation UpdateLicense($id: uuid!, $changes: license_set_input!) {
    update_license_by_pk(pk_columns: { id: $id }, _set: $changes) {
      id
      ...LicenseFields
      users_id
    }
  }
`;

/*******************************************************************/

export const UPDATE_USER = gql/* GraphQL */`
  mutation UpdateUser($id: uuid!, $displayName: String!, $metadata:  jsonb!) {
    updateUser(pk_columns: { id: $id }, _set: {displayName: $displayName, metadata: $metadata}) {
      id
      email
      displayName
      metadata
      createdAt
      defaultRole
      displayName
      email
      avatarUrl
      metadata
      isAnonymous
      locale
      emailVerified
      phoneNumber
      phoneNumberVerified
      activeMfaType
    }
  }
`;

export const UPDATE_USER_METADATA = gql/* GraphQL */`
  mutation UpdateUserMetadata($id: uuid!, $metadata: jsonb!) {
    updateUser(pk_columns: { id: $id }, _set: {metadata: $metadata}) {
      id
      email
      displayName
      metadata
      createdAt
      defaultRole
      displayName
      email
      avatarUrl
      metadata
      isAnonymous
      locale
      emailVerified
      phoneNumber
      phoneNumberVerified
      activeMfaType
    }
  }
`;

/*******************************************************************/

export const DELETE_ITEM = gql/* GraphQL */`
  ${ITEM_FIELDS}
  mutation DeleteItem($id: uuid!) {
    delete_item_by_pk(id: $id) {
      id
      ...ItemFields
      users_id
    }
  }
`;


/*******************************************************************/

export const DELETE_LICENSE = gql/* GraphQL */`
  ${LICENSE_FIELDS}
  mutation DeleteLicense($id: uuid!) {
    delete_license_by_pk(id: $id) {
      id
      ...LicenseFields
      users_id
    }
  }
`;

/*******************************************************************/

export const DUMMY_FRAGMENT = gql/* GraphQL */`
  fragment DummyFragment on DummyItem {
    DummyFieldId
  }
`;

/*******************************************************************/
// USED QUERIES BELOW ARE FUNCTIONAL BUT THEY ARE NOT NEEDED
/*******************************************************************/

export const GET_INCOME_ITEMS = gql/* GraphQL */`
  ${ITEM_FIELDS}
  query GetItemsIncome($year: numeric!, $month: numeric!) {
    items(
      where: {
        type: { _eq: ${ItemType.Income} }
        year: { _eq: $year }
        month: { _eq: $month }
      },
    ) {
      id
      ...ItemFields
      users_id
    }
  }
`;

export const GET_EXPENSE_ITEMS = gql/* GraphQL */`
  ${ITEM_FIELDS}
  query GetItemsExpense($year: numeric!, $month: numeric!) {
    items(
      where: {
        type: { _eq: ${ItemType.Expense} }
        year: { _eq: $year }
        month: { _eq: $month }
      }
    ) {
      id
      ...ItemFields
      users_id
    }
  }
`;

export const GET_ITEM = gql/* GraphQL */`
  ${ITEM_FIELDS}
  query GetItem($id: uuid!) {
    item_by_pk(id: $id) {
      id
      ...ItemFields
      users_id
    }
  }
`;

export const YEAR_REPORT = gql/* GraphQL */`
  ${ITEM_FIELDS}
  query GetItemsByYear($year: numeric!) {
    item(where: { year: { _eq: $year } }) {
      id
      ...ItemFields
      users_id
    }
  }
`;
