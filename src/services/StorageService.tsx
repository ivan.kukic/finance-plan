import { gql } from '@apollo/client';

import { StorageBucket, StorageFile } from "@shared/types/Storage";

export const STORAGE_FILE_FRAGMENT_FIELDS = gql/* GraphQL */`
  fragment StorageFileFragmentFields on storage_file {
    bucket_id
    files_id
    user_id
    used_on
    etag
    is_uploaded
    mime_type
    name
    size
    created_at
    updated_at
    note
  }
`;

export const STORAGE_BUCKET_FRAGMENT_FIELDS = gql/* GraphQL */`
  fragment StorageBucketFragmentFields on storage_bucket {
    user_id
    used_on
    name
    created_at
    updated_at
    note
    parent_id
  }
`;

export const GET_STORAGE_FILES = gql/* GraphQL */`
  ${STORAGE_FILE_FRAGMENT_FIELDS}
  query MyQueryGetStorageFiles($used_on: String!, $user_id: uuid!) {
    storage_file(where: { used_on: { _eq: $used_on }, user_id: { _eq: $user_id } }, order_by: { created_at: asc }) {
      id
      ...StorageFileFragmentFields
    }
  }
`;

export const ADD_STORAGE_FILE = gql/* GraphQL */`
  ${STORAGE_FILE_FRAGMENT_FIELDS}
  mutation MyInsertMutation($storage_file: storage_file_insert_input!) {
    insert_storage_file_one(object: $storage_file) {
      id
      ...StorageFileFragmentFields
    }
  }
`;

export const UPDATE_STORAGE_FILE_BY_PK = gql/* GraphQL */`
  ${STORAGE_FILE_FRAGMENT_FIELDS}
  mutation MyUpdateMutation($id: uuid!, $storage_file: storage_file_set_input!) {
    update_storage_file_by_pk(pk_columns: { id: $id }, _set: $storage_file) {
      id
      ...StorageFileFragmentFields
    }
  }
`;

export const DELETE_STORAGE_FILE_BY_PK = gql/* GraphQL */`
  ${STORAGE_FILE_FRAGMENT_FIELDS}
  mutation MyDeleteMutation($id: uuid!) {
    delete_storage_file_by_pk(id: $id) {
      id
      ...StorageFileFragmentFields
    }
  }
`;

export const DELETE_STORAGE_FILE_BY_FILE_ID = gql/* GraphQL */`
  ${STORAGE_FILE_FRAGMENT_FIELDS}
  mutation MyDeleteMutationById($files_id: string!) {
    delete_storage_file(where: { files_id: { _eq: $files_id } }) {
      id
      ...StorageFileFragmentFields
    }
  }
`;

export const DUMMY_FRAGMENT = gql/* GraphQL */`
  fragment DummyFragment on DummyItem {
    DummyFieldId
  }
`;

export const addStorageFileCache = (cache: any, {data}: any): any => {
  cache.modify({
    fields: {
      storage_file(existingStorageFiles = []) {
        const newStorageFileRef = cache.writeFragment({
          data: data.insert_storage_file_one,
          fragment: DUMMY_FRAGMENT,
        });
        return [...existingStorageFiles, newStorageFileRef];
      },
    },
  });
};

export const deleteStorageFileCache = (cache: any, {data}: any): any => {
  const _idRemove = data.delete_storage_file_by_pk.id;
  cache.modify({
    fields: {
      storage_file(existingTaskRefs: any[], {readField}: any) {
        return existingTaskRefs.filter(
          (taskRef) => _idRemove !== readField('id', taskRef)
        );
      },
    },
  });
};

export const updateStorageFileCache = (cache: any, {data}: any): any => {
  cache.modify({
    id: cache.identify(data.update_storage_file_by_pk),
    fields: {
      item(existingItems = []) {
        const updateItemRef = cache.writeFragment({
          data: existingItems.map((storage_file: StorageFile) =>
            storage_file.id === data.update_storage_file_by_pk.id
              ? data.update_storage_file_by_pk
              : storage_file
          ),
          fragment: DUMMY_FRAGMENT,
        });
        return [updateItemRef];
      },
    },
  });
};

export const GET_STORAGE_BUCKETS = gql/* GraphQL */`
  ${STORAGE_BUCKET_FRAGMENT_FIELDS}
  query MyQueryGetStorageBuckets($used_on: String!, $user_id: uuid!) {
    storage_bucket(where: { used_on: { _eq: $used_on }, user_id: { _eq: $user_id } }, order_by: { created_at: asc }) {
      id
      ...StorageBucketFragmentFields
    }
  }
`;

export const ADD_STORAGE_BUCKET = gql/* GraphQL */`
  ${STORAGE_BUCKET_FRAGMENT_FIELDS}
  mutation MyInsertMutation($storage_bucket: storage_bucket_insert_input!) {
    insert_storage_bucket_one(object: $storage_bucket) {
      id
      ...StorageBucketFragmentFields
    }
  }
`;

export const DELETE_STORAGE_BUCKET_BY_PK = gql/* GraphQL */`
  ${STORAGE_BUCKET_FRAGMENT_FIELDS}
  mutation MyDeleteMutation($id: uuid!) {
    delete_storage_bucket_by_pk(id: $id) {
      id
      ...StorageBucketFragmentFields
    }
  }
`;

export const UPDATE_STORAGE_BUCKET_BY_PK = gql/* GraphQL */`
  ${STORAGE_BUCKET_FRAGMENT_FIELDS}
  mutation MyUpdateMutation($id: uuid!, $storage_bucket: storage_bucket_set_input!) {
    update_storage_bucket_by_pk(pk_columns: { id: $id }, _set: $storage_bucket) {
      id
      ...StorageBucketFragmentFields
    }
  }
`;

export const addStorageBucketCache = (cache: any, {data}: any): any => {
  cache.modify({
    fields: {
      storage_bucket(existingStorageBuckets = []) {
        const newStorageBucketRef = cache.writeFragment({
          data: data.insert_storage_bucket_one,
          fragment: DUMMY_FRAGMENT,
        });
        return [...existingStorageBuckets, newStorageBucketRef];
      },
    },
  });
};

export const deleteStorageBucketCache = (cache: any, {data}: any): any => {
  const _idRemove = data.delete_storage_bucket_by_pk.id;
  cache.modify({
    fields: {
      storage_bucket(existingTaskRefs: any[], {readField}: any) {
        return existingTaskRefs.filter(
          (taskRef) => _idRemove !== readField('id', taskRef)
        );
      },
    },
  });
};

export const updateStorageBucketCache = (cache: any, {data}: any): any => {
  cache.modify({
    id: cache.identify(data.update_storage_bucket_by_pk),
    fields: {
      item(existingItems = []) {
        const updateItemRef = cache.writeFragment({
          data: existingItems.map((storage_bucket: StorageBucket) =>
            storage_bucket.id === data.update_storage_bucket_by_pk.id
              ? data.update_storage_bucket_by_pk
              : storage_bucket
          ),
          fragment: DUMMY_FRAGMENT,
        });
        return [updateItemRef];
      },
    },
  });
};