import { gql } from '@apollo/client';

import { Category } from "@shared/types/Category";

export const CATEGORY_FIELDS = gql/* GraphQL */`
  fragment CategoryFields on category {
    type
    name
    description
    helper_text
    icon
    icon_color
    is_active
    created_at
    updated_at
  }
`;

/*******************************************************************/

export const GET_CATEGORIES = gql/* GraphQL */`
  ${CATEGORY_FIELDS}
  query GetCategories($users_id: uuid) {
    category(
      where: {
        _and: [
          { users_id: { _eq: $users_id } }
        ]
      }, 
      order_by: { created_at: asc }
    ) {
      id
      ...CategoryFields
      users_id
    }
  }
`;

/*******************************************************************/

export const ADD_CATEGORY = gql/* GraphQL */`
  ${CATEGORY_FIELDS}
  mutation InsertCategory($category: category_insert_input!) {
    insert_category_one(object: $category) {
      id
      ...CategoryFields
      users_id
    }
  }
`;

/*******************************************************************/

export const UPDATE_CATEGORY = gql/* GraphQL */`
  ${CATEGORY_FIELDS}
  mutation UpdateCategory($id: uuid!, $changes: category_set_input!) {
    update_category_by_pk(pk_columns: { id: $id }, _set: $changes) {
      id
      ...CategoryFields
      users_id
    }
  }
`;

/*******************************************************************/

export const DELETE_CATEGORY = gql/* GraphQL */`
  ${CATEGORY_FIELDS}
  mutation DeleteCategory($id: uuid!) {
    delete_category_by_pk(id: $id) {
      id
      ...CategoryFields
      users_id
    }
  }
`;

/*******************************************************************/

export const DUMMY_FRAGMENT = gql/* GraphQL */`
  fragment DummyFragment on DummyItem {
    DummyFieldId
  }
`;

/*******************************************************************/

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const addCategoryCache = (cache: any, {data}: any): void => {
  cache.modify({
    fields: {
      category(existingCategories = []) {
        const newCategoryRef = cache.writeFragment({
          data: data.insert_category_one,
          fragment: DUMMY_FRAGMENT,
        });
        return [...existingCategories, newCategoryRef];
      },
    },
  });
};

/*******************************************************************/

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const updateStorageFileCache = (cache: any, {data}: any): void => {
  cache.modify({
    id: cache.identify(data.update_category_by_pk),
    fields: {
      category(existingItems = []) {
        const updateItemRef = cache.writeFragment({
          data: existingItems.map((category: Category) =>
            category.id === data.update_category_by_pk.id
              ? data.update_category_by_pk
              : category
          ),
          fragment: DUMMY_FRAGMENT,
        });
        return [updateItemRef];
      },
    },
  });
};

/*******************************************************************/

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const deleteCategoryCache = (cache: any, {data}: any): void => {
  const _idRemove = data.delete_category_by_pk.id;
  cache.modify({
    fields: {
      category(existingTaskRefs: any[], {readField}: any) {
        return existingTaskRefs.filter(
          (taskRef) => _idRemove !== readField('id', taskRef)
        );
      },
    },
  });
};