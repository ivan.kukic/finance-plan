import { createSlice, PayloadAction, Draft } from '@reduxjs/toolkit';

import { FormAction } from "@shared/enum/FormAction";
import { ItemType } from "@shared/enum/itemType";
import { RootState } from "@store/Store";
import { Category } from "@shared/types/Category";
import { Document } from "@shared/types/Storage";

// Generic interface for dialog state
export interface IGenericDialogState<T> {
  isOpen: boolean;
  item: T | null;
  formAction?: FormAction;
  type?: ItemType;
}

// Function to create a generic dialog slice
export const createGenericDialogSlice = <T extends object>(name: string) => {
  const initialState: IGenericDialogState<T> = {
    isOpen: false,
    item: null,
    formAction: FormAction.Create,
    type: ItemType.Income
  };

  const slice = createSlice({
    name,
    initialState,
    reducers: {
      openDialog: (state, action: PayloadAction<Omit<IGenericDialogState<T>, 'isOpen'>>) => {
        state.isOpen = true;
        state.item = action.payload.item as Draft<T> || null;
        state.formAction = action.payload.formAction || undefined;
        state.type = action.payload.type || undefined;
      },
      closeDialog: (state) => {
        state.isOpen = false;
      }
    }
  });

  return {
    reducer: slice.reducer,
    actions: slice.actions
  };
};

// Update the selector functions
export const createSelectors = <T extends IGenericDialogState<unknown>>(sliceName: keyof RootState) => ({
  selectIsOpen: (state: RootState) => (state[sliceName] as T).isOpen,
  selectItem: (state: RootState) => (state[sliceName] as T).item,
  selectState: (state: RootState) => (state[sliceName] as T)
});

// Create specific slices for category dialog and category confirm dialog
export const dialogSlice = createGenericDialogSlice<Category>('dialog');
export const confirmDialogSlice = createGenericDialogSlice<Category>('confirmDialog');
export const bucketDialogSlice = createGenericDialogSlice<Document>('bucketDialog');
export const treeViewDialogSlice = createGenericDialogSlice<Document>('treeViewDialog');


// Export actions for slices
export const {
  openDialog: openCategoryDialog,
  closeDialog: closeCategoryDialog
} = dialogSlice.actions;

export const {
  openDialog: openCategoryConfirmDialog,
  closeDialog: closeCategoryConfirmDialog
} = confirmDialogSlice.actions;

export const {
  openDialog: openBucketDialog,
  closeDialog: closeBucketDialog
} = bucketDialogSlice.actions;

export const {
  openDialog: openTreeViewDialog,
  closeDialog: closeTreeViewDialog
} = treeViewDialogSlice.actions;


// Export selectors for slices
export const {
  selectIsOpen: selectCategoryDialogOpen,
  selectItem: selectCategoryDialogItem,
  selectState: selectCategoryDialogState
} = createSelectors<IGenericDialogState<Category>>('dialog');

export const {
  selectIsOpen: selectCategoryConfirmDialogOpen,
  selectItem: selectCategoryConfirmDialogItem,
  selectState: selectCategoryConfirmDialogState
} = createSelectors<IGenericDialogState<Category>>('confirmDialog');

export const {
  selectIsOpen: selectBucketDialogOpen,
  selectItem: selectBucketDialogItem,
  selectState: selectBucketDialogState
} = createSelectors<IGenericDialogState<Document>>('bucketDialog');

export const {
  selectIsOpen: selectTreeViewDialogOpen,
  selectItem: selectTreeViewDialogItem,
  selectState: selectTreeViewDialogState
} = createSelectors<IGenericDialogState<Document>>('treeViewDialog');

// Export reducers
export const dialog = dialogSlice.reducer;
export const confirmDialog = confirmDialogSlice.reducer;
export const bucketDialog = bucketDialogSlice.reducer;
export const treeViewDialog = treeViewDialogSlice.reducer;