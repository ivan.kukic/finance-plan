import { combineReducers } from 'redux';

import { auth } from "./authSlice";
import { menu } from "@store/reducers/menu";
import { year } from "@store/reducers/yearSlice";
import { dropdownMenu } from "@store/reducers/dropdownMenu";
import { bucketFile } from "./bucketFileSlice";
import { dialog, confirmDialog, bucketDialog, treeViewDialog } from "@store/reducers/genericDialogSlice";
import { notification } from "@store/reducers/notification";

const reducers = combineReducers({
  auth,
  menu,
  year,
  dropdownMenu,
  bucketFile,
  dialog,
  confirmDialog,
  bucketDialog,
  treeViewDialog,
  notification,
});

export default reducers;