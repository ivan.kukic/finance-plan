import { createSlice } from '@reduxjs/toolkit';

import { INotification, NotificationType } from "@shared/enum/NotificationType";
import { RootState } from "@store/Store";

const initialState: INotification = {
  type: NotificationType.Success,
  message: '',
  open: false,
};

const notificationSlice = createSlice({
  name: 'notification',
  initialState,
  reducers: {
    openNotification(state, action) {
      state.open = true;
      state.message = action.payload.message;
      state.type = action.payload.type;
    },
    closeNotification(state) {
      state.open = false;
    },
  }
});


export const {openNotification, closeNotification} = notificationSlice.actions;
export const notification = notificationSlice.reducer;

export const selectNotificationState = (state: RootState) => (state.notification)