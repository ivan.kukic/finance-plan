import { createSlice } from '@reduxjs/toolkit';
import { RootState } from "@store/Store";

export interface IDropdownState {
  dropdownMenuOpen: boolean;
}

const initialState: IDropdownState = {
  dropdownMenuOpen: false
};

const dropdownMenuSlice = createSlice({
  name: 'dropdownMenu',
  initialState,
  reducers: {
    openDropdownMenu(state, action) {
      state.dropdownMenuOpen = action.payload.dropdownMenuOpen;
    },
    closeDropdownMenu(state) {
      state.dropdownMenuOpen = false
    },
  }
});


export const {openDropdownMenu, closeDropdownMenu} = dropdownMenuSlice.actions;
export const dropdownMenu = dropdownMenuSlice.reducer;

export const selectDropdownMenuOpen = (state: RootState) => (state.dropdownMenu.dropdownMenuOpen)
