import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { Folder, File } from "@shared/types/Storage";
import { RootState } from "@store/Store";

export interface BucketFileState {
  folders: Folder[] | null;
  files: File[] | null;
}

const initialState: BucketFileState = {
  folders: null,
  files: null,
};

const bucketFileSlice = createSlice({
  name: 'bucketFile',
  initialState,
  reducers: {
    setData: (state, action: PayloadAction<BucketFileState>) => {
      state.folders = action.payload.folders;
      state.files = action.payload.files;
    },
    clearData: (state) => {
      state.folders = null;
      state.files = null;
    },
  },
});

export const {setData, clearData} = bucketFileSlice.actions;
export const bucketFile = bucketFileSlice.reducer;

export const selectBucketFile = (state: RootState) => (state.bucketFile)