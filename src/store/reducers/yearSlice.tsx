import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { RootState } from "@store/Store";

export interface YearState {
  selectedYear: number;
}

const getCurrentYear = (): number => {
  const currentYear = new Date().getFullYear();
  if (typeof window !== 'undefined' && window.localStorage) {
    const savedYear = localStorage.getItem('selectedYear');
    return savedYear ? parseInt(savedYear, 10) : currentYear;
  }
  return currentYear;
};

const initialState: YearState = {
  selectedYear: getCurrentYear()
};

const yearSlice = createSlice({
  name: 'year',
  initialState,
  reducers: {
    setYear: (state, action: PayloadAction<number>) => {
      state.selectedYear = action.payload;
      if (typeof window !== 'undefined' && window.localStorage) {
        localStorage.setItem('selectedYear', action.payload.toString());
      }
    },
  }
});

export const {setYear} = yearSlice.actions;
export const year = yearSlice.reducer;

export const selectedYear = (state: RootState) => (state.year.selectedYear)