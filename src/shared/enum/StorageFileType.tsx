export enum StorageLocationType {
  FilesAndBuckets = 'FilesAndBuckets',
  UserProfile = 'UserProfile',
  Other = 'Other',
}