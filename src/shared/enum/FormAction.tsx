export enum FormAction {
  Create = 'Create',
  Update = 'Update',
  Delete = 'Delete',
}
