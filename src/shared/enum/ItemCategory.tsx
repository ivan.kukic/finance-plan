export enum ItemCategory {
  FinancialPlan = 'FinancialPlan',
  TodoPlan = 'TodoPlan',
  InvestmentPlan = 'InvestmentPlan',
  SavingAccount = 'SavingAccount',
  Debt = 'DebtPlan',
  LoanPlan = 'LoanPlan',
  FinancePlan = 'FinancePlan'
}
