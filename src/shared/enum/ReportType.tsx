export enum ReportType {
  Year = 'Year',
  Quarter = 'Quarter',
  Month = 'Month',
}