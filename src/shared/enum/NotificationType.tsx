export enum NotificationType {
    Success = 'success',
    Info = 'info',
    Error = 'error'
}

export interface INotification {
    type: NotificationType.Success | NotificationType.Info | NotificationType.Error;
    message: string;
    open: boolean;
}