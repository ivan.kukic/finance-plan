export enum DateFormat {
  Short = 'MMM Do', // Feb 15th. 2023, 12:56:11 pm
  Standard = 'MMM Do, h:mm:ss a', // Feb 4th 12:56:11 pm
  Full = 'MMMM Do YYYY, h:mm:ss a', // February 4th 2023, 12:56:11 pm
}
