export enum LicenseType {
    Free = 'FREE',
    Premium = 'PREMIUM',
    Trial = 'TRIAL',
}