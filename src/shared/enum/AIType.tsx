export enum AIType {
  ChatGPT = 'chatgpt',
  Claude = 'claude',
  Perplexity = 'perplexity'
}