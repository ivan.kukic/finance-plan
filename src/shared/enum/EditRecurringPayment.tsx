export enum EditRecurringPayment {
    ThisPayment = 'This payment',
    ThisAndFollowingPayments = 'This and following payments',
    AllPayments = 'All payments',
}