export enum ItemType {
  Income = 'Income',
  Expense = 'Expense',
  Goals = 'Goals',
  // Earning = 'Earning',
  // Todo = 'Todo',
  // Saving = 'Saving',
  // Debt = 'Debt',
  // Loan = 'Loan',
  // Financials = 'Financials',
}