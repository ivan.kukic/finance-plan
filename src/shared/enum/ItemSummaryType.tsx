export enum ItemSummaryType {
  Row = 'SUMMARY_ROW',
  Column = 'SUMMARY_COLUMN'
}