export enum ItemCategoryName {
  FinancialPlan = 'Financial plan',
  TodoPlan = 'Todo plan',
  InvestmentPlan = 'Investment plan',
  SavingAccount = 'Saving account plan',
  Debt = 'Debt plan',
  LoanPlan = 'Loan plan',
  FinancePlan = 'Finance plan'
}