export interface StorageFile {
  id?: string
  used_on?: string
  files_id?: string
  user_id?: string
  note?: string
  created_at?: string
  updated_at?: string
  bucket_id?: string | null;
  name?: string;
  size?: number;
  mime_type?: string;
  etag?: string;
  is_uploaded?: boolean;
  public_url?: string;
}

export interface StorageBucket {
  id?: string
  used_on?: string
  user_id?: string
  note?: string
  created_at?: string
  updated_at?: string
  name?: string;
  parent_id?: string | null;
}

export interface Folder extends StorageBucket {
  type: DocumentType.Folder;
}

export interface File extends StorageFile {
  type: DocumentType.File;
}

export enum DocumentType {
  File = 'File',
  Folder = 'Folder'
}

export type Document = Folder | File;

export interface Breadcrumb {
  id?: string | null | undefined;
  name: string | undefined;
}