import { ReactNode } from "react";
import { Item } from "@shared/types/Item";
import { Category } from "@shared/types/Category";
import { ItemType } from "@shared/enum/itemType";

export interface CrudList {
  id?: string | undefined
  icon?: ReactNode | null
  icon_color?: string | null
  title?: string | null
  subtitle?: string | null
  actionText?: string | null
  field?: Item | Category | null | undefined
}

export interface CrudListItem {
  id?: string | undefined;
  name?: string;
  category?: string | null;
  type?: ItemType;
  icon?: string;
  icon_color?: string;
  price?: number | null;
  helper_text?: string;
  note?: string | null;
}

export interface EntityList<T> {
  items: CrudList[];
  onEdit?: (field: T | null) => void;
  onDelete?: (field: T | null) => void;
  noOfItems?: number;
}