import { ItemType } from "@shared/enum/itemType";

export interface Category {
  id?: string;
  type?: ItemType
  name?: string
  description?: string
  helper_text?: string
  icon?: string
  icon_color?: string
  is_active?: boolean
  created_at?: string
  updated_at?: string
  users_id?: string
}