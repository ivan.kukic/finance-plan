import {FormAction} from '../enum/FormAction';
import {ItemCategory} from '../enum/ItemCategory';
import {ItemType} from '../enum/itemType';

/**
 * Used within Financial plan module
 */
export interface Item {
    category?: ItemCategory;
    type?: ItemType;
    id?: string | null;
    name?: string | null;
    price?: number | string | null;
    paid?: boolean;
    paidDate?: string | null;
    payDueDate?: string | null;
    recurring?: boolean | null;
    recurringStart?: string | null;
    recurringEnd?: string | null;
    recurringStreamId?: string;
    recurringOverride?: boolean;
    note?: string | null;
    order?: number | string | null;
    created_at?: string | null;
    updated_at?: string | null;
    users_id?: string | null;
    year?: number | null;
    month?: number | null;
    __typename?: string | null;
    rootStreamItem?: Item | boolean;
}

export interface DefaultItemDataProps {
    category: ItemCategory;
    year: number;
    month: number;
    users_id: string | null;
}

export interface InitialItemDataProps extends DefaultItemDataProps {
    type: ItemType;
}

/**
 * Used to extend ItemContainer & ItemForm props
 */
export interface InitialItemFormProps {
    type: ItemType;
    initialItem: Item;
    loadItemForm: any;
    showItemForm?: boolean;
    setShowItemForm?: any;
    openedFormId?: string | null | undefined;
    setOpenedFormId?: any;
}

/**
 * Used in ItemContainer component
 */
export interface ItemContainerProps extends InitialItemFormProps {
    data: ItemObjectProps;
    children?: any;
}

/**
 * Used in ItemTableComponent
 */
export interface ItemTableProps extends InitialItemFormProps {
    data: ItemObjectProps;
    children?: any;
}

/**
 * Used in ItemForm component
 */
export interface ItemFormProps extends InitialItemFormProps {
    item: Item;
    action: FormAction;
    month?: number;
}

/**
 * Used in ItemSummary component
 */
export interface ItemSummaryProps {
    itemSummary: any;
    title: string;
    children?: any;
    color: string;
    backgroundColor: string;
}

/**
 * Used in All Components
 */
export interface ItemObjectProps {
    items: Item[];
    total: number;
    paid: number;
    remaining: number;
    month: number;
}

/**
 * Used in ItemReport Components
 */
export interface ReportObjectProps {
    total: number;
    currentBalance?: number;
    paymentToBeReceived?: number;
    paymentToBeMade?: number;
}
