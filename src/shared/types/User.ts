export interface IUserMetadata {
  firstname: string | null;
  lastname: string | null;
  company: string | null;
  note: string | null;
  avatar_url: string | null;
  avatar_storage_file_id: string | null;
  avatar_files_id: string | null;
}

export interface IUser {
  id: string;
  createdAt: string;
  defaultRole: string;
  displayName: string;
  email: string;
  avatarUrl: string;
  metadata: IUserMetadata;
  isAnonymous: boolean;
  locale: string;
  roles: string[];
  emailVerified: boolean;
  phoneNumber: string | null;
  phoneNumberVerified: boolean;
  activeMfaType: 'totp' | null;
}