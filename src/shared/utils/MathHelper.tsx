import { ReportType } from "@shared/enum/ReportType";

export const calculatePercentageChange = (currentYear: number, previousYear: number): {
  percentage: number | null,
  difference: number
} => {
  // Calculate the absolute difference
  const difference = currentYear - previousYear;

  // Calculate the percentage change
  const percentageChange = previousYear !== 0 ? ((difference / previousYear) * 100).toFixed(2) : null;

  // Return both as formatted strings
  return {
    percentage: percentageChange !== null ? parseFloat(percentageChange as string) : null,
    difference: difference
  };
}

export const calculateQuarterFromMonth = (month = 1) => (Math.floor((month - 1) / 3) + 1)

export const calculateQuarterOffset = ({quarter = 1, reportType = ReportType.Year}) => {
  switch (reportType) {
    case ReportType.Year:
      return quarter - 1;
    case ReportType.Quarter:
      return (quarter - 1) * 3;
    case ReportType.Month:
      return quarter - 1;
    default:
      return 0;
  }
};