// import Moment from 'moment';

import { ItemCategoryName } from '../enum/ItemCategoryName';
import { ItemCategory } from '../enum/ItemCategory';
import { ItemType } from '../enum/itemType';
// import {DateFormat} from '../enum/DateFormat';
import { Item } from "../types/Item";
// import moment from "moment/moment";

// Moment.locale('en');


// Month listing in English language
export const months: string[] = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

// Month listing in English language
export const monthsShort: string[] = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec',
];

export const csvHeaders = [
  {label: "Id", key: "id", type: "text"},
  {label: "Users Id", key: "users_id", type: "text"},
  {label: "Category", key: "category", type: "text"},
  {label: "Type", key: "type", type: "text"},
  {label: "Title", key: "name", type: "text"},
  {label: "Amount", key: "price", type: "number"},
  {label: "Paid", key: "paid"},
  {label: "Paid date", key: "paidDate"},
  {label: "Paid due date", key: "payDueDate"},
  {label: "Month", key: "month"},
  {label: "Year", key: "year"},
  {label: "Recurring", key: "recurring"},
  {label: "Note", key: "note"},
  {label: "Order", key: "order"},
  {label: "Created at", key: "created_at"},
  {label: "Updated at", key: "updated_at"},
  {label: "Recurring start", key: "recurringStart"},
  {label: "Recurring end", key: "recurringEnd"},
  {label: "Recurring stream id", key: "recurringStreamId"},
  {label: "Recurring override", key: "recurringOverride"},
];


// Get month name from listing
export const parseMonth = (monthId: number | null | undefined): string =>
  months[Number(monthId) - 1];

// Generates four years for selection (current-1 , current, current + 1, current +2)
export interface YearOptions {
  label: string;
  id: number;
}

export const generateYears = (): YearOptions[] => {
  const min = new Date().getFullYear() - 1;
  const max = min + 3;
  const years: YearOptions[] = [];

  for (let i = min; i <= max; i++) {
    years.push({label: String(i), id: i});
  }

  return years;
};

// Prevents Apollo caching workaround
export const getCallbackQueryName = (type: ItemType): string =>
  type === ItemType.Income ? 'GetItemsIncome' : 'GetItemsExpense';

// Moment parse date
// export const formatDateOrNull = (
//     date: string | null | any,
//     format: DateFormat = DateFormat.Standard
// ): string => (date ? Moment(date).format(format) : '-');

// export const getMonthFromDate = (date: string | null | undefined): number | null => (date ? Moment(date).month() + 1 : null)

export const formatDate = (date: Date): string => {
  const day = date.getDate();
  const month = monthsShort[date.getMonth()];
  const year = date.getFullYear();

  const hours = date.getHours().toString().padStart(2, '0');
  const minutes = date.getMinutes().toString().padStart(2, '0');

  return `${month} ${day}, ${year} ${hours}:${minutes}`;
}

// Create formatter for currency as EUR
export const currencyFormatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'EUR',
});

// Format currency
export const formatNumberOrNull = (
  number: number | string | null | undefined
): string => currencyFormatter.format(Number(number));

// Format percentage
export const formatPercentOrNull = (
  number: number | string | null | undefined
): string | null => {

  if (number === null || number === undefined || Number(number) <= 0) {
    return null;
  }

  return Number(number).toFixed(2) + '%';
}

// Parse category name
export const parseCategory = (
  category: ItemCategory | undefined
): ItemCategoryName | string => {
  switch (category) {
    case ItemCategory.FinancialPlan: {
      return ItemCategoryName.FinancialPlan;
    }
    case ItemCategory.Debt: {
      return ItemCategoryName.Debt;
    }
    case ItemCategory.SavingAccount: {
      return ItemCategoryName.SavingAccount;
    }
    default: {
      return 'There is no category name for the given category!';
    }
  }
};

// Parse Item Form
// const excludeInsertKeys: any[] = ['created_at', 'updated_at', 'id', '__typename'];
// const excludeUpdateKeys: any[] = ['id', '__typename', 'created_at', 'users_id'];

// Before submitting a form, we remove all: empty string, undefined and null fields
// export const parseInsertData = (item: any) => {
//     const newItem: any = {};
//
//     Object.keys(item).forEach((k) => {
//         if (!excludeInsertKeys.includes(k)) {
//             newItem[k] = item[k] === '' ? null : item[k];
//         }
//     });
//
//     return newItem;
// };

// Before submitting a form, we remove all: empty string, undefined and null fields
// export const parseUpdateData = (item: any) => {
//     const newItem: any = {};
//
//     Object.keys(item).forEach((k) => {
//         if (!excludeInsertKeys.includes(k) && !excludeUpdateKeys.includes(k)) {
//             newItem[k] = item[k] === '' ? null : item[k];
//         }
//     });
//
//     return newItem;
// };

// export const itemTypes = (Object.keys(ItemType) as (keyof typeof ItemType)[]).map((key, index) => ItemType[key]);

export const cloneItem = (item: Item, month: number | null = null) => {
  const newItem = clone(item);
  if (month) {
    newItem.month = month;
  }
  return newItem;
}

export const clone = (item: Item): Item => (JSON.parse(JSON.stringify(item)));

// export const toISOString = (str: string) => {
//     return moment.utc(str, 'YYYY/MM/DD').toISOString()
// }

export const capitalizeFirstLetter = (str: string | null | undefined): string | null | undefined => {
  if (!str) return str;
  return str.charAt(0).toUpperCase() + str.slice(1);
}

export const avatarSX = {
  width: 36,
  height: 36,
  fontSize: '1rem'
};

export const actionSX = {
  mt: 0.75,
  ml: 1,
  top: 'auto',
  right: 'auto',
  alignSelf: 'flex-start',
  transform: 'none'
};