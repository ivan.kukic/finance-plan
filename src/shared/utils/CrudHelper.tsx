import { BankOutlined, CreditCardOutlined } from "@ant-design/icons";

import { CrudCategory } from "@shared/enum/CrudCategory";
import { formatNumberOrNull } from "@utils/Helpers";
import { ItemType } from "@shared/enum/itemType";
import { IconComponent } from "@components/utility/IconComponent";
import { IconColors } from "@shared/enum/IconColors";
import { CrudList, CrudListItem } from "@shared/types/Crud";

export const getCategoryIconByType = (category: unknown, type: string | ItemType, color = '#1677ff', size = 16) => {
  if (!category && type === ItemType.Income) return <BankOutlined style={{fontSize: size, color: color}} />;
  if (!category && type === ItemType.Expense) return <CreditCardOutlined style={{fontSize: size, color: color}} />;

  return <IconComponent iconName={`${category}Outlined`} color={color} size={size} />
}

export const renderCrudList = (items: CrudListItem[], category: CrudCategory): CrudList[] => {
  return items.map((item: CrudListItem) => {
    switch (category) {
      case CrudCategory.Item:
        return {
          id: item.id,
          icon: getCategoryIconByType(item.category && item.category !== 'FinancialPlan' ? item.category : null, item.type || ItemType.Income, item.icon_color ? IconColors[item.icon_color as keyof typeof IconColors] : undefined),
          title: item.name,
          subtitle: !item.note || item.note === '' ? (item.type === ItemType.Income ? 'Standard Income Item' : 'Standard Expense Item') : item.note || undefined,
          actionText: formatNumberOrNull(item.price),
          field: item
        }

      case CrudCategory.Category:
        return {
          id: item.id,
          icon: getCategoryIconByType(item.icon || null, item.type || ItemType.Income, item.icon_color ? IconColors[item.icon_color as keyof typeof IconColors] : undefined),
          icon_color: item.icon_color ? IconColors[item.icon_color as keyof typeof IconColors] : undefined,
          title: item.name,
          subtitle: !item.helper_text || item.helper_text === '' ? (item.type === ItemType.Income ? 'Standard Income Category' : 'Standard Expense Category') : item.helper_text || null,
          actionText: null,
          field: item
        }

      default:
        return {}
    }
  });
}