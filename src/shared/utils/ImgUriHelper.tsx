import { ReactNode } from "react";

import { getDataUri } from "@utils/ChartHelper";
import { ReportType } from "@shared/enum/ReportType";
import { calculateQuarterOffset } from "@utils/MathHelper";
import PieChart from "@components/dashboard/PieChart";

const getMonthsAndOffset = (reportType: ReportType, quarter: number) => {
  const noOfMonths = reportType === ReportType.Year ? 12 : 3;
  const quarterOffset = calculateQuarterOffset({quarter, reportType});
  return {noOfMonths, quarterOffset};
}

export async function getImageURIArray({reportType = ReportType.Year, quarter = 1}) {
  const {noOfMonths, quarterOffset} = getMonthsAndOffset(reportType, quarter);

  return await Promise.all(
    Array.from({length: noOfMonths}, async (_, i) => {
      const chartId = `PieChartPdf-${quarterOffset + i + 1}`;
      try {
        return await getDataUri(chartId, 300, 205);
      } catch (error) {
        console.error(`Failed to fetch URI for ${chartId}:`, error);
        return null;
      }
    })
  );
}

interface IGetPieChartArray {
  reportType: ReportType;
  quarter: number;
  income: number[];
  expense: number[];
}

export const getPieChartArray = ({
                                   reportType = ReportType.Year,
                                   quarter = 1,
                                   income = [],
                                   expense = []
                                 }: IGetPieChartArray): ReactNode[] => {
  const {noOfMonths, quarterOffset} = getMonthsAndOffset(reportType, quarter);

  return (
    Array.from({length: noOfMonths}, (_, i) => {
      const chartId = `PieChartPdf-${quarterOffset + i + 1}`;
      return (
        <PieChart
          key={chartId}
          chartId={chartId}
          series={
            [income[i], expense[i]]
          }
          height={205}
          isPdfChart={true}
        />
      );
    })
  );
}