import { formatNumberOrNull, formatPercentOrNull } from "@utils/Helpers";
import { IPdfDocument } from "@components/reports/pdf/PdfDocument";
import { Item } from "@shared/types/Item";
import { calculateTotalAmount } from "@utils/ItemHelper";
import { calculatePercentageChange } from "@utils/MathHelper";

interface IFormatAnalytics {
  title: string,
  amount: number,
  percentage: number | null,
  extra: number | null
}

const formatAnalytics = ({title, amount, percentage, extra}: IFormatAnalytics) => (
  {
    title: title,
    amount: `${formatNumberOrNull(amount)}`,
    percentage: formatPercentOrNull(percentage),
    extra: formatNumberOrNull(extra),
    isLoss: extra ? extra < 0 : true
  }
)

export const getMonthlyAnalytics = ({items, month}: IPdfDocument) => {

  const monthlyIncomeItems: Item[] = items.yearlyIncomeItems.filter((item: Item) => item.month === month);
  const monthlyExpenseItems: Item[] = items.yearlyExpenseItems.filter((item: Item) => item.month === month);

  const monthlyIncome: number = calculateTotalAmount(monthlyIncomeItems);
  const monthlyExpense: number = calculateTotalAmount(monthlyExpenseItems);
  const monthlyEarn: number = monthlyIncome - monthlyExpense;

  const monthlyIncomeItemsPrev: Item[] = items.yearlyIncomeItemsPrev.filter((item: Item) => item.month === month);
  const monthlyExpenseItemsPrev: Item[] = items.yearlyExpenseItemsPrev.filter((item: Item) => item.month === month);

  const monthlyIncomePrev: number = calculateTotalAmount(monthlyIncomeItemsPrev);
  const yearlyExpensePrev: number = calculateTotalAmount(monthlyExpenseItemsPrev);
  const yearlyEarnPrev: number = monthlyIncomePrev - yearlyExpensePrev;

  const monthIncomePercentage = calculatePercentageChange(monthlyIncome, monthlyIncomePrev);
  const monthExpensePercentage = calculatePercentageChange(monthlyExpense, yearlyExpensePrev);
  const monthEarnPercentage = calculatePercentageChange(monthlyEarn, yearlyEarnPrev);

  return {
    income: formatAnalytics({
      title: `Income`,
      amount: monthlyIncome,
      percentage: monthIncomePercentage.percentage,
      extra: monthIncomePercentage.difference
    }),
    expense: formatAnalytics({
      title: `Expense`,
      amount: monthlyExpense,
      percentage: monthExpensePercentage.percentage,
      extra: monthExpensePercentage.difference
    }),
    earn: formatAnalytics({
      title: `Profit`,
      amount: monthlyEarn,
      percentage: monthEarnPercentage.percentage,
      extra: monthEarnPercentage.difference
    })
  }
}

export const getQuarterlyAnalytics = ({items, year, quarter}: IPdfDocument & { quarter: number }) => {
  const startMonth = (quarter - 1) * 3 + 1;
  const endMonth = quarter * 3;

  const quarterlyIncomeItems: Item[] = items.yearlyIncomeItems.filter((item: Item) =>
    item.year === year && Number(item.month) >= startMonth && Number(item.month) <= endMonth
  );
  const quarterlyExpenseItems: Item[] = items.yearlyExpenseItems.filter((item: Item) =>
    item.year === year && Number(item.month) >= startMonth && Number(item.month) <= endMonth
  );

  const quarterlyIncome: number = calculateTotalAmount(quarterlyIncomeItems);
  const quarterlyExpense: number = calculateTotalAmount(quarterlyExpenseItems);
  const quarterlyEarn: number = quarterlyIncome - quarterlyExpense;

  const quarterlyIncomeItemsPrev: Item[] = items.yearlyIncomeItemsPrev.filter((item: Item) =>
    item.year === year && Number(item.month) >= startMonth && Number(item.month) <= endMonth
  );
  const quarterlyExpenseItemsPrev: Item[] = items.yearlyExpenseItemsPrev.filter((item: Item) =>
    item.year === year && Number(item.month) >= startMonth && Number(item.month) <= endMonth
  );

  const quarterlyIncomePrev: number = calculateTotalAmount(quarterlyIncomeItemsPrev);
  const quarterlyExpensePrev: number = calculateTotalAmount(quarterlyExpenseItemsPrev);
  const quarterlyEarnPrev: number = quarterlyIncomePrev - quarterlyExpensePrev;

  const quarterIncomePercentage = calculatePercentageChange(quarterlyIncome, quarterlyIncomePrev);
  const quarterExpensePercentage = calculatePercentageChange(quarterlyExpense, quarterlyExpensePrev);
  const quarterEarnPercentage = calculatePercentageChange(quarterlyEarn, quarterlyEarnPrev);

  return {
    income: formatAnalytics({
      title: `Q${quarter} Income`,
      amount: quarterlyIncome,
      percentage: quarterIncomePercentage.percentage,
      extra: quarterIncomePercentage.difference
    }),
    expense: formatAnalytics({
      title: `Q${quarter} Expense`,
      amount: quarterlyExpense,
      percentage: quarterExpensePercentage.percentage,
      extra: quarterExpensePercentage.difference
    }),
    earn: formatAnalytics({
      title: `Q${quarter} Profit`,
      amount: quarterlyEarn,
      percentage: quarterEarnPercentage.percentage,
      extra: quarterEarnPercentage.difference
    })
  }
}

export const getYearlyAnalytics = ({items, year}: IPdfDocument) => {
  const yearlyIncomeItems: Item[] = items.yearlyIncomeItems.filter((item: Item) => item.year === year);
  const yearlyExpenseItems: Item[] = items.yearlyExpenseItems.filter((item: Item) => item.year === year);

  const yearlyIncome: number = calculateTotalAmount(yearlyIncomeItems);
  const yearlyExpense: number = calculateTotalAmount(yearlyExpenseItems);
  const yearlyEarn: number = yearlyIncome - yearlyExpense;

  const yearlyIncomeItemsPrev: Item[] = items.yearlyIncomeItemsPrev.filter((item: Item) => item.year === year);
  const yearlyExpenseItemsPrev: Item[] = items.yearlyExpenseItemsPrev.filter((item: Item) => item.year === year);

  const yearlyIncomePrev: number = calculateTotalAmount(yearlyIncomeItemsPrev);
  const yearlyExpensePrev: number = calculateTotalAmount(yearlyExpenseItemsPrev);
  const yearlyEarnPrev: number = yearlyIncomePrev - yearlyExpensePrev;

  const yearlyIncomePercentage = calculatePercentageChange(yearlyIncome, yearlyIncomePrev);
  const yearlyExpensePercentage = calculatePercentageChange(yearlyExpense, yearlyExpensePrev);
  const yearlyEarnPercentage = calculatePercentageChange(yearlyEarn, yearlyEarnPrev);

  return {
    income: formatAnalytics({
      title: `Income`,
      amount: yearlyIncome,
      percentage: yearlyIncomePercentage.percentage,
      extra: yearlyIncomePercentage.difference
    }),
    expense: formatAnalytics({
      title: `Expense`,
      amount: yearlyExpense,
      percentage: yearlyExpensePercentage.percentage,
      extra: yearlyExpensePercentage.difference
    }),
    earn: formatAnalytics({
      title: `Profit`,
      amount: yearlyEarn,
      percentage: yearlyEarnPercentage.percentage,
      extra: yearlyEarnPercentage.difference
    })
  }
}