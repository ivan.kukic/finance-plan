export interface StyleWithChildren {
  sx: {
    bgcolor: string;
    width: number;
    height: number;
    fontSize: number;
    fontWeight: string;
  };
  children: string;
}

// Define the second type
export interface StyleWithSrc {
  sx: {
    width: number;
    height: number;
  };
  src: string;
}

// Union type combining both possible structures
export type GravatarProps = StyleWithChildren | StyleWithSrc;

// Create Color for Avatar Icon from Name
export const stringToColor = (string: string): string => {
  let hash = 0;
  let i;

  for (i = 0; i < string.length; i += 1) {
    hash = string.charCodeAt(i) + ((hash << 5) - hash);
  }

  let color = '#';

  for (i = 0; i < 3; i += 1) {
    const value = (hash >> (i * 8)) & 0xff;
    color += `00${value.toString(16)}`.slice(-2);
  }

  return color;
}

// Create Avatar Icon
export const stringAvatar = (displayName = 'XYZ', email = 'XYZ', size = 32, fontSize = 13) => {
  return {
    sx: {
      bgcolor: stringToColor(displayName + email),
      width: size,
      height: size,
      fontSize: fontSize,
      fontWeight: 'bold',
      border: size === 32 ? 'none' : '1px dashed #585858c7'
    },
    children: `${displayName.split(' ')[0][0]}${displayName.split(' ')[1][0]}`,
  };
}

export const urlAvatar = (avatarUrl = '', size = 32) => {
  return {
    sx: {
      width: size,
      height: size,
      border: size === 32 ? 'none' : '1px dashed #585858c7'
    },
    src: avatarUrl || 'https://s.gravatar.com/avatar/0921f64f74b2d767ee24469c448ba415?r=g',
  };
}