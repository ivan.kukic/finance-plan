import ApexCharts from "apexcharts";
import { saveAs } from "file-saver";
import { pdf } from "@react-pdf/renderer";

export const savePdf = async (pdfDocument: JSX.Element, fileName: string): Promise<void> => {
  try {
    const asPdf = pdf();
    asPdf.updateContainer(pdfDocument);
    const pdfBlob = await asPdf.toBlob();
    saveAs(pdfBlob, fileName);
  } catch (error) {
    console.error('Error during PDF download:', error);
  }
}

export const getDataUri = async (chartId: string, width = 1200, height = 420): Promise<string | null> => {
  try {
    await ApexCharts.exec(chartId, "updateOptions", {
      chart: {
        width: width,
        height: height,
        animations: {
          enabled: false,
        },
      },
    });

    const {imgURI} = await ApexCharts.exec(chartId, "dataURI");

    return imgURI || null;

  } catch (error) {
    console.error("Error generating data URI:", error);
    return null; // Return null as a fallback option in case of an error
  }
}