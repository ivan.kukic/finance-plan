import {
  CodeOutlined,
  FileImageOutlined,
  FileOutlined,
  FilePdfOutlined, FileTextOutlined,
  FileZipOutlined, FolderOutlined,
  SoundOutlined,
  VideoCameraOutlined
} from "@ant-design/icons";

export const getIconByMimeType = (mimeType: string | undefined, size = 52) => {
  if (!mimeType) return <FileOutlined style={{fontSize: size}} />;

  if (mimeType.startsWith('image/')) return <FileImageOutlined style={{fontSize: size, color: '#ea4335'}} />;
  if (mimeType === 'application/pdf') return <FilePdfOutlined style={{fontSize: size, color: '#ea4335'}} />;
  if (mimeType.startsWith('video/')) return <VideoCameraOutlined style={{fontSize: size, color: 'purple'}} />;
  if (mimeType.startsWith('audio/')) return <SoundOutlined style={{fontSize: size, color: '#34a853'}} />;
  if (mimeType === 'application/zip' || mimeType === 'application/x-rar-compressed') return <FileZipOutlined
    style={{fontSize: size, color: 'orange'}} />;
  if (mimeType === 'text/plain') return <FileTextOutlined style={{fontSize: size, color: 'gray'}} />;
  if (mimeType.startsWith('application/json') || mimeType.startsWith('application/xml')) return <CodeOutlined
    style={{fontSize: size, color: 'black'}} />;
  if (mimeType === 'inode/directory') return <FolderOutlined style={{fontSize: size, color: 'brown'}} />;

  // Default icon for unrecognized MIME types
  return <FileOutlined style={{fontSize: size, color: '#9c9c9c'}} />;
};