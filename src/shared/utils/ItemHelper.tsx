import { Item } from "@shared/types/Item";
import { ItemType } from "@shared/enum/itemType";
import { ReportType } from "@shared/enum/ReportType";

export const getItemsPerMonth = (items: Item[]) => {
  const itemsPerMonth = [];

  for (let i = 0; i < 12; i++) {
    const monthItems: Item[] = items.filter((item: Item) => item.month === i);
    itemsPerMonth.push(monthItems)
  }

  return itemsPerMonth;
}

export const getTotal = (reportType: ReportType, yearItems: number[], quarterItems: number[]) => {
  return reportType === ReportType.Year ? yearItems : quarterItems
}

export const calculateTotalAmount = (items: Item[]): number => {
  return items.reduce((total: number, item: Item) => total + (item.price != null ? parseFloat(item.price.toString()) : 0), 0);
};

export const getYearlyTotalPerMonths = (items: Item[]) => {
  const yearIncome = [];
  const yearExpense = [];

  for (let i = 0; i < 12; i++) {
    const monthly: Item[] = items.filter((item: Item) => item.month === i + 1);
    const income: Item[] = monthly.filter((item: Item) => item.type === ItemType.Income);
    const expense: Item[] = monthly.filter((item: Item) => item.type === ItemType.Expense);
    const totalIncome: number = calculateTotalAmount(income);
    const totalExpense: number = calculateTotalAmount(expense);
    yearIncome.push(totalIncome)
    yearExpense.push(totalExpense)
  }

  return {
    income: yearIncome,
    expense: yearExpense,
  };
}

export const getQuarterlyTotalPerMonths = (items: Item[], quarter: number) => {
  const quarterIncome = [];
  const quarterExpense = [];
  const startMonth = (quarter - 1) * 3 + 1;
  const endMonth = quarter * 3;

  for (let i = startMonth; i <= endMonth; i++) {
    const monthly: Item[] = items.filter((item: Item) => item.month === i);
    const income: Item[] = monthly.filter((item: Item) => item.type === ItemType.Income);
    const expense: Item[] = monthly.filter((item: Item) => item.type === ItemType.Expense);
    const totalIncome: number = calculateTotalAmount(income);
    const totalExpense: number = calculateTotalAmount(expense);
    quarterIncome.push(totalIncome);
    quarterExpense.push(totalExpense);
  }

  return {
    income: quarterIncome,
    expense: quarterExpense,
  };
}