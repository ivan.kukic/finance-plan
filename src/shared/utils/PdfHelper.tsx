import { StyleSheet } from "@react-pdf/renderer";

export const styles = StyleSheet.create({
  page: {
    paddingTop: 35,
    paddingBottom: 35,
    paddingHorizontal: 35,
    color: '#262626',
    fontFamily: 'Roboto'
  },
  column: {
    display: 'flex',
    flexDirection: 'column'
  },
  row: {
    display: 'flex',
    flexDirection: 'row'
  },
  header: {
    fontSize: 9,
    marginBottom: 20,
    textAlign: 'left',
    color: 'grey',
  },
  footer: {
    fontSize: 9,
    color: 'grey',
    position: 'absolute',
    bottom: 30,
    left: 30,
  },
  primary: {
    color: '#1677ff',
  },
  title: {
    fontSize: 24,
  },
  subtitle: {
    fontSize: 12,
    marginBottom: 40,
    color: '#1677ff'
  },
  section: {},
  analyticsWrap: {
    marginRight: 120,
    lineHeight: 'normal'
  },
  analyticsTitle: {
    fontSize: 14,
    lineHeight: 'normal',
    color: '#8c8c8c',
    marginBottom: 10,
  },
  analyticsInner: {
    display: 'flex',
    flexDirection: 'row',
    marginBottom: 20
  },
  analyticsAmount: {
    fontSize: 18,
    fontWeight: 400,
  },
  analyticsChip: {
    fontSize: 12,
    fontWeight: 400,
    marginLeft: 90,
    // border: '1px solid grey',
    color: '#1677ff',
    alignSelf: 'flex-end',
    paddingBottom: 2,
    flexGrow: 1,
  },
  analyticsText: {
    fontSize: 10,
    color: '#8c8c8c',
    marginBottom: 10,
  },
  chartImageFull: {
    borderWidth: 0,
    marginBottom: 0,
    paddingTop: 30,
    border: 0,
    width: "100%",
    height: "auto",
  },
  listItemText: {
    fontFamily: 'Roboto',
    color: '#333',
    fontSize: 11,
    lineHeight: 'normal',
    marginBottom: 0,
    paddingTop: 0,
    paddingBottom: 0,
    paddingHorizontal: 0
  },
  viewer: {
    width: window.innerWidth - 50,
    height: window.innerHeight - 120,
  },
});