import { IUser } from "@shared/types/User";
import { GravatarProps, stringAvatar, urlAvatar } from "@shared/utils/AvatarText";

export const getAvatar = async (
  user: IUser,
  setGravatar: (avatar: GravatarProps) => void,
  size = 32,
  fontSize = 13,
) => {
  const str = user.avatarUrl;

  if (user?.metadata?.avatar_url) {
    setGravatar(urlAvatar(user.metadata.avatar_url, size));
  } else if (str.includes("https://") && !str.endsWith("404")) {
    setGravatar(urlAvatar(user.avatarUrl, size));
  } else {
    setGravatar(stringAvatar(user.displayName, user.email, size, fontSize));
  }
};
