// TODO: This application is finance projection on year, and bonus is you can trac it trough the year
// TODO: Finansiske projekcije praviš

import { StrictMode } from 'react';
import { createRoot } from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import { Provider as ReduxProvider } from 'react-redux';
import { NhostApolloProvider } from "@nhost/react-apollo";
import { NhostProvider } from "@nhost/react";
import { nhost } from "@configs/nhost";
import 'simplebar-core/dist/simplebar.css';

import { ROUTES } from "@routes/Routes";
import ThemeCustomization from "@themes/ThemesIndex";
import store from '@store/Store';
import App from "./App";

const router = createBrowserRouter(ROUTES);
const container = document.getElementById('root') as HTMLElement;
const root = createRoot(container!);

root.render(
  <StrictMode>
    <ReduxProvider store={store}>
      <ThemeCustomization>
        <NhostProvider nhost={nhost}>
          <NhostApolloProvider nhost={nhost}>
            <App>
              <RouterProvider router={router} />
            </App>
          </NhostApolloProvider>
        </NhostProvider>
      </ThemeCustomization>
    </ReduxProvider>
  </StrictMode>
)