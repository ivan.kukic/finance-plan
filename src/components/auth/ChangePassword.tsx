import { Grid, Stack, Typography } from '@mui/material';

import AuthWrapper from './AuthWrapper';
import AuthChangePassword from "@components/auth/forms/AuthChangePassword";

const ChangePassword = () => (
  <AuthWrapper>
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <Stack direction="row" justifyContent="space-between" alignItems="baseline" sx={{ mb: { xs: -0.5, sm: 0.5 } }}>
          <Typography variant="h3">Change password</Typography>
        </Stack>
      </Grid>
      <Grid item xs={12}>
        <AuthChangePassword />
      </Grid>
    </Grid>
  </AuthWrapper>
);

export default ChangePassword;
