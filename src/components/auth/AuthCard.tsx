import { ReactNode } from "react";
import { Box } from '@mui/material';

import MainCard from "@components/utility/MainCard";

const AuthCard = ({children, ...other}: { children: ReactNode }) => (
  <MainCard
    sx={{
      maxWidth: 475,
      margin: 3,
      '& > *': {
        flexGrow: 1,
        flexBasis: '50%'
      }
    }}
    content={false}
    {...other}
    border={false}
    boxShadow
  >
    <Box sx={{p: {xs: 2, sm: 3, md: 4, xl: 5}}}>{children}</Box>
  </MainCard>
);

export default AuthCard;
