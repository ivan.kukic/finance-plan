import { Outlet } from 'react-router-dom';
import { Navigate, useLocation } from 'react-router-dom';
import { useAuthenticationStatus } from '@nhost/react';

import Loader from "@components/utility/Loader";

const Authenticate = () => {
  const {isAuthenticated, isLoading} = useAuthenticationStatus();
  const location = useLocation();

  if (isLoading) {
    return <Loader />
  }

  if (!isAuthenticated) {
    return <Navigate to="/login" state={{from: location}} replace />;
  }

  return (
    <>
      <Outlet />
    </>
  );
};

export default Authenticate;
