import { ReactNode } from "react";
import { Box, Grid } from '@mui/material';

import Logo from "@components/utility/Logo";
import AuthCard from "./AuthCard";
import AuthFooter from "@components/auth/AuthFooter";

const AuthWrapper = ({children}: { children: ReactNode }) => (
  <Box sx={{minHeight: '100vh'}}>
    <Grid
      container
      direction="column"
      justifyContent="flex-end"
      sx={{
        minHeight: '100vh'
      }}
    >
      <Grid item xs={12} sx={{ml: 3}}>
        <Logo />
      </Grid>
      <Grid item xs={12}>
        <Grid
          item
          xs={12}
          container
          justifyContent="center"
          alignItems="center"
          sx={{minHeight: {xs: 'calc(100vh - 112px)'}}}
        >
          <Grid item>
            <AuthCard>{children}</AuthCard>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12} sx={{m: 3, mt: 0}}>
        <AuthFooter />
      </Grid>
    </Grid>
  </Box>
);

export default AuthWrapper;
