import { SyntheticEvent, useEffect, useState } from 'react';
import {
  Box,
  Button,
  Divider,
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  Stack,
  Typography, Alert
} from '@mui/material';
import * as Yup from 'yup';
import { Formik, FormikHelpers } from 'formik';
import { useSignUpEmailPassword } from "@nhost/react";
import { EyeOutlined, EyeInvisibleOutlined } from '@ant-design/icons';

import SocialSignIn from './SocialSignIn';
import { strengthColor, strengthIndicator } from "@utils/PasswordStrength";

interface IRegisterValues {
  firstname: string;
  lastname: string;
  email: string;
  company: string;
  password: string;
  submit: null
}

const AuthRegister = () => {
  const [level, setLevel] = useState<{ color: string, label: string } | undefined>(undefined);
  const [showPassword, setShowPassword] = useState(false);
  const [passwordCount, setPasswordCount] = useState(0);
  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = (event: SyntheticEvent) => {
    event.preventDefault();
  };

  const changePasswordStrength = (value: string) => {
    setLevel(strengthColor(strengthIndicator(value)));
    setPasswordCount(passwordCount + 1);
  };

  const {signUpEmailPassword, error} = useSignUpEmailPassword();

  useEffect(() => {
    changePasswordStrength('');
  }, []);

  const [showMessage, setShowMessage] = useState(false);

  const onSubmit = async (_values: IRegisterValues, {
    setErrors,
    setStatus,
    setSubmitting
  }: FormikHelpers<IRegisterValues>) => {
    try {

      const response = await signUpEmailPassword(_values.email, _values.password, {
        displayName: `${_values.firstname} ${_values.lastname}`.trim(),
        metadata: {
          firstname: _values.firstname,
          lastname: _values.lastname,
          company: _values.company,
          note: null,
          avatar_url: null,
          avatar_storage_file_id: null,
          avatar_files_id: null
        },
      });
      if (response?.error === null) {
        setShowMessage(true);
      }

      setStatus({success: false});
      setSubmitting(false);

    } catch (err) {
      // Type assertion to Error
      const error = err as Error;
      setStatus({success: false});
      setErrors({submit: error.message});
      setSubmitting(false);
    }
  }

  return (
    <>
      {showMessage ? (
        <Stack sx={{width: '100%', pt: 0}} spacing={2}>
          <Alert severity="info">
            Thank You for Signing Up <br />
            Please check your inbox for an email with a link to verify your account.
          </Alert>
          ` </Stack>
      ) : (
        <Formik
          initialValues={{
            firstname: '',
            lastname: '',
            email: '',
            company: '',
            password: '',
            submit: null
          }}
          validationSchema={Yup.object().shape({
            firstname: Yup.string().max(255).required('First Name is required'),
            lastname: Yup.string().max(255).required('Last Name is required'),
            email: Yup.string().email('Must be a valid email').max(255).required('Email is required'),
            password: Yup.string().min(8).max(255).required('Password is required')
          })}
          onSubmit={onSubmit}
        >
          {({errors, handleChange, handleSubmit, isSubmitting, touched, values}) => (
            <form noValidate onSubmit={handleSubmit}>
              <Grid container spacing={3}>
                <Grid item xs={12} md={6}>
                  <Stack spacing={1}>
                    <InputLabel htmlFor="firstname-signup">First Name*</InputLabel>
                    <OutlinedInput
                      id="firstname-login"
                      type="firstname"
                      value={values.firstname}
                      name="firstname"
                      onChange={handleChange}
                      placeholder="First name"
                      fullWidth
                      error={Boolean(touched.firstname && errors.firstname)}
                      autoFocus
                    />
                    {touched.firstname && errors.firstname && (
                      <FormHelperText error id="helper-text-firstname-signup">
                        {errors.firstname}
                      </FormHelperText>
                    )}
                  </Stack>
                </Grid>
                <Grid item xs={12} md={6}>
                  <Stack spacing={1}>
                    <InputLabel htmlFor="lastname-signup">Last Name*</InputLabel>
                    <OutlinedInput
                      fullWidth
                      error={Boolean(touched.lastname && errors.lastname)}
                      id="lastname-signup"
                      type="lastname"
                      value={values.lastname}
                      name="lastname"
                      onChange={handleChange}
                      placeholder="Last name"
                      inputProps={{}}
                    />
                    {touched.lastname && errors.lastname && (
                      <FormHelperText error id="helper-text-lastname-signup">
                        {errors.lastname}
                      </FormHelperText>
                    )}
                  </Stack>
                </Grid>
                <Grid item xs={12}>
                  <Stack spacing={1}>
                    <InputLabel htmlFor="company-signup">Company</InputLabel>
                    <OutlinedInput
                      fullWidth
                      error={Boolean(touched.company && errors.company)}
                      id="company-signup"
                      value={values.company}
                      name="company"
                      onChange={handleChange}
                      placeholder="Company"
                      inputProps={{}}
                    />
                    {touched.company && errors.company && (
                      <FormHelperText error id="helper-text-company-signup">
                        {errors.company}
                      </FormHelperText>
                    )}
                  </Stack>
                </Grid>
                <Grid item xs={12}>
                  <Stack spacing={1}>
                    <InputLabel htmlFor="email-signup">Email*</InputLabel>
                    <OutlinedInput
                      fullWidth
                      error={Boolean(touched.email && errors.email)}
                      id="email-login"
                      type="email"
                      value={values.email}
                      name="email"
                      onChange={handleChange}
                      placeholder="Email"
                      inputProps={{}}
                    />
                    {touched.email && errors.email && (
                      <FormHelperText error id="helper-text-email-signup">
                        {errors.email}
                      </FormHelperText>
                    )}
                  </Stack>
                </Grid>
                <Grid item xs={12}>
                  <Stack spacing={1}>
                    <InputLabel htmlFor="password-signup">Password*</InputLabel>
                    <OutlinedInput
                      fullWidth
                      error={Boolean(touched.password && errors.password)}
                      id="password-signup"
                      type={showPassword ? 'text' : 'password'}
                      value={values.password}
                      name="password"
                      onChange={(e) => {
                        handleChange(e);
                        changePasswordStrength(e.target.value);
                      }}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={handleClickShowPassword}
                            onMouseDown={handleMouseDownPassword}
                            edge="end"
                            size="large"
                            tabIndex={-1}
                          >
                            {showPassword ? <EyeOutlined /> : <EyeInvisibleOutlined />}
                          </IconButton>
                        </InputAdornment>
                      }
                      placeholder="Password"
                      inputProps={{}}
                    />
                    {touched.password && errors.password && (
                      <FormHelperText error id="helper-text-password-signup">
                        {errors.password}
                      </FormHelperText>
                    )}
                  </Stack>
                  <FormControl fullWidth sx={{mt: 2}}>
                    {passwordCount > 3 && (
                      <Grid container spacing={2} alignItems="center">
                        <Grid item>
                          <Box sx={{bgcolor: level?.color, width: 85, height: 8, borderRadius: '7px'}} />
                        </Grid>
                        <Grid item>
                          <Typography variant="subtitle1" fontSize="0.75rem">
                            {level?.label}
                          </Typography>
                        </Grid>
                      </Grid>
                    )}
                  </FormControl>
                </Grid>
                {errors.submit && (
                  <Grid item xs={12}>
                    <FormHelperText error>{errors.submit}</FormHelperText>
                  </Grid>
                )}
                <Grid item xs={12}>
                  {/*<AnimateButton>*/}
                  <Button disableElevation disabled={isSubmitting} fullWidth size="large" type="submit"
                          variant="contained" color="primary">
                    Create Account
                  </Button>
                  {/*</AnimateButton>*/}
                </Grid>
                {error?.message ? (
                  <Grid item xs={12}>
                    <Stack sx={{width: '100%', mt: 2}} spacing={2}>
                      <Alert severity="error">{error?.message}</Alert>
                    </Stack>
                  </Grid>
                ) : null}
                <Grid item xs={12}>
                  <Divider>
                    <Typography variant="caption">Sign up with</Typography>
                  </Divider>
                </Grid>
                <Grid item xs={12}>
                  <SocialSignIn />
                </Grid>
              </Grid>
            </form>
          )}
        </Formik>
      )}
    </>
  );
};

export default AuthRegister;
