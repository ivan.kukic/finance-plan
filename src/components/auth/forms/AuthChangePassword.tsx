import { SyntheticEvent, useEffect, useState } from 'react';

import {
  Box,
  Button,
  FormControl,
  FormHelperText,
  Grid,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  Stack,
  Typography, Alert, Link
} from '@mui/material';
import * as Yup from 'yup';
import { Formik, FormikHelpers } from 'formik';
import { useChangePassword } from "@nhost/react";
import { EyeOutlined, EyeInvisibleOutlined } from '@ant-design/icons';

import { strengthColor, strengthIndicator } from "@utils/PasswordStrength";

interface IChangePasswordValues {
  password: string;
  passwordConfirm: string;
  submit: null
}

const AuthChangePassword = () => {
  const [level, setLevel] = useState<{ color: string, label: string } | undefined>(undefined);
  const [showPassword, setShowPassword] = useState(false);
  const [passwordCount, setPasswordCount] = useState(0);
  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = (event: SyntheticEvent) => {
    event.preventDefault();
  };

  const changePasswordStrength = (value: string) => {
    setLevel(strengthColor(strengthIndicator(value)));
    setPasswordCount(passwordCount + 1);
  };

  const {changePassword, isError, error, isSuccess} = useChangePassword();

  useEffect(() => {
    changePasswordStrength('');
  }, []);

  const onSubmit = async (_values: IChangePasswordValues, {
    setErrors,
    setStatus,
    setSubmitting
  }: FormikHelpers<IChangePasswordValues>) => {
    try {
      await changePassword(_values.password);
      setStatus({success: false});
      setSubmitting(false);

    } catch (err) {
      // Type assertion to Error
      const error = err as Error;
      setStatus({success: false});
      setErrors({submit: error.message});
      setSubmitting(false);
    }
  }

  return (
    <>
      {isSuccess ? (
        <Stack sx={{width: '100%', pt: 0}} spacing={2}>
          <Alert severity="info">
            Your password has been successfully changed.
          </Alert>
          <Link href="/src/components/Dashboard" variant="body2">
            Access your dashboard.
          </Link>
        </Stack>
      ) : (
        <Formik
          initialValues={{
            password: '',
            passwordConfirm: '',
            submit: null
          }}
          validationSchema={Yup.object().shape({
            password: Yup.string().min(8).max(255).required('Password is required'),
            passwordConfirm: Yup.string().min(8).max(255).required('Password is required').oneOf([Yup.ref("password")], "Passwords do not match")
          })}
          onSubmit={onSubmit}
        >
          {({errors, handleChange, handleSubmit, isSubmitting, touched, values}) => (
            <form noValidate onSubmit={handleSubmit}>
              <Grid container spacing={3}>

                <Grid item xs={12}>
                  <Stack spacing={1}>
                    <InputLabel htmlFor="password-signup">Password*</InputLabel>
                    <OutlinedInput
                      fullWidth
                      error={Boolean(touched.password && errors.password)}
                      id="password-signup"
                      type={showPassword ? 'text' : 'password'}
                      value={values.password}
                      name="password"
                      onChange={(e) => {
                        handleChange(e);
                        changePasswordStrength(e.target.value);
                      }}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={handleClickShowPassword}
                            onMouseDown={handleMouseDownPassword}
                            edge="end"
                            size="large"
                            tabIndex={-1}
                          >
                            {showPassword ? <EyeOutlined /> : <EyeInvisibleOutlined />}
                          </IconButton>
                        </InputAdornment>
                      }
                      placeholder="Password"
                      inputProps={{}}
                      autoFocus
                    />
                    {touched.password && errors.password && (
                      <FormHelperText error id="helper-text-password-signup">
                        {errors.password}
                      </FormHelperText>
                    )}
                  </Stack>
                </Grid>

                <Grid item xs={12}>
                  <Stack spacing={1}>
                    <InputLabel htmlFor="passwordConfirm-signup">Confirm password*</InputLabel>
                    <OutlinedInput
                      fullWidth
                      error={Boolean(touched.passwordConfirm && errors.passwordConfirm)}
                      id="passwordConfirm-signup"
                      type={showPassword ? 'text' : 'password'}
                      value={values.passwordConfirm}
                      name="passwordConfirm"
                      onChange={(e) => {
                        handleChange(e);
                      }}
                      placeholder="Confirm password"
                      inputProps={{}}
                    />
                    {touched.passwordConfirm && errors.passwordConfirm && (
                      <FormHelperText error id="helper-text-password-signup">
                        {errors.passwordConfirm}
                      </FormHelperText>
                    )}
                  </Stack>
                  <FormControl fullWidth sx={{mt: 2}}>
                    {passwordCount > 3 && (
                      <Grid container spacing={2} alignItems="center">
                        <Grid item>
                          <Box sx={{bgcolor: level?.color, width: 85, height: 8, borderRadius: '7px'}} />
                        </Grid>
                        <Grid item>
                          <Typography variant="subtitle1" fontSize="0.75rem">
                            {level?.label}
                          </Typography>
                        </Grid>
                      </Grid>
                    )}
                  </FormControl>
                </Grid>

                {errors.submit && (
                  <Grid item xs={12}>
                    <FormHelperText error>{errors.submit}</FormHelperText>
                  </Grid>
                )}
                <Grid item xs={12}>
                  {/*<AnimateButton>*/}
                  <Button disableElevation disabled={isSubmitting} fullWidth size="large" type="submit"
                          variant="contained" color="primary">
                    Change Password
                  </Button>
                  {/*</AnimateButton>*/}
                </Grid>
                {isError ? (
                  <Grid item xs={12}>
                    <Stack sx={{width: '100%', mt: 2}} spacing={2}>
                      <Alert severity="error">{error?.message}</Alert>
                    </Stack>
                  </Grid>
                ) : null}
              </Grid>
            </form>
          )}
        </Formik>
      )}
    </>
  );
};

export default AuthChangePassword;
