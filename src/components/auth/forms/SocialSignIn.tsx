import { useNhostClient } from "@nhost/react";
import { useTheme } from '@mui/material/styles';
import { useMediaQuery, Button, Stack } from '@mui/material';

import Google from '@assets/images/icons/google.svg';

const SocialSignIn = () => {
  const theme = useTheme();
  const matchDownSM = useMediaQuery(theme.breakpoints.down('sm'));

  const nhost = useNhostClient();

  const googleHandler = async () => {
    await nhost.auth.signIn({
      provider: 'google'
    })
  };

  return (
    <Stack
      direction="row"
      spacing={matchDownSM ? 1 : 2}
      justifyContent={matchDownSM ? 'space-around' : 'space-between'}
      sx={{'& .MuiButton-startIcon': {mr: matchDownSM ? 0 : 1, ml: matchDownSM ? 0 : -0.5}}}
    >
      <Button
        variant="outlined"
        color="secondary"
        fullWidth={!matchDownSM}
        startIcon={<img src={Google} alt="Google" />}
        onClick={googleHandler}
      >
        {!matchDownSM && 'Google'}
      </Button>
    </Stack>
  );
};

export default SocialSignIn;
