import React, { SyntheticEvent } from 'react';
import { Link as RouterLink, Navigate } from 'react-router-dom';
import {
  Button,
  Checkbox,
  Divider,
  FormControlLabel,
  FormHelperText,
  Grid,
  Link,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
  Stack,
  Typography, Alert
} from '@mui/material';
import * as Yup from 'yup';
import { Formik, FormikHelpers } from 'formik';
import { useSignInEmailPassword } from "@nhost/react";
import { EyeOutlined, EyeInvisibleOutlined } from '@ant-design/icons';

import SocialSignIn from "./SocialSignIn";


// ============================|| NHOST - LOGIN ||============================ //

interface ILoginValues {
  email: string;
  password: string;
  submit: null
}

const AuthLogin = () => {
  const [checked, setChecked] = React.useState(false);

  const [showPassword, setShowPassword] = React.useState(false);
  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = (event: SyntheticEvent) => {
    event.preventDefault();
  };

  const {signInEmailPassword, isSuccess, error} = useSignInEmailPassword();

  const onSubmit = async (_values: ILoginValues, {
    setErrors,
    setStatus,
    setSubmitting
  }: FormikHelpers<ILoginValues>) => {
    try {
      await signInEmailPassword(_values.email, _values.password);
      setStatus({success: false});
      setSubmitting(false);

    } catch (err) {
      // Type assertion to Error
      const error = err as Error;
      setStatus({success: false});
      setErrors({submit: error.message});
      setSubmitting(false);
    }
  }

  if (isSuccess) {
    return <Navigate to="/" replace={true} />;
  }

  return (
    <>
      <Formik
        initialValues={{
          email: '',
          password: '',
          submit: null
        }}
        validationSchema={Yup.object().shape({
          email: Yup.string().email('Must be a valid email').max(255).required('Email is required'),
          password: Yup.string().max(255).required('Password is required')
        })}
        onSubmit={onSubmit}
      >
        {({errors, handleChange, handleSubmit, isSubmitting, touched, values}) => (
          <form noValidate onSubmit={handleSubmit}>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <Stack spacing={1}>
                  <InputLabel htmlFor="email-login">Email</InputLabel>
                  <OutlinedInput
                    id="email-login"
                    type="email"
                    value={values.email}
                    name="email"
                    onChange={handleChange}
                    placeholder="Email"
                    fullWidth
                    error={Boolean(touched.email && errors.email)}
                    autoFocus
                  />
                  {touched.email && errors.email && (
                    <FormHelperText error id="standard-weight-helper-text-email-login">
                      {errors.email}
                    </FormHelperText>
                  )}
                </Stack>
              </Grid>
              <Grid item xs={12}>
                <Stack spacing={1}>
                  <InputLabel htmlFor="password-login">Password</InputLabel>
                  <OutlinedInput
                    fullWidth
                    error={Boolean(touched.password && errors.password)}
                    id="-password-login"
                    type={showPassword ? 'text' : 'password'}
                    value={values.password}
                    name="password"
                    onChange={handleChange}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                          edge="end"
                          size="large"
                          tabIndex={-1}
                        >
                          {showPassword ? <EyeOutlined /> : <EyeInvisibleOutlined />}
                        </IconButton>
                      </InputAdornment>
                    }
                    placeholder="Password"
                  />
                  {touched.password && errors.password && (
                    <FormHelperText error id="standard-weight-helper-text-password-login">
                      {errors.password}
                    </FormHelperText>
                  )}
                </Stack>
              </Grid>

              <Grid item xs={12} sx={{mt: -1}}>
                <Stack direction="row" justifyContent="space-between" alignItems="center" spacing={2}>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={checked}
                        onChange={(event) => setChecked(event.target.checked)}
                        name="checked"
                        color="primary"
                        size="small"
                      />
                    }
                    label={<Typography variant="h6">Keep me sign in</Typography>}
                  />
                  <Link variant="h6" component={RouterLink} to="/reset-password" color="text.primary">
                    Forgot Password?
                  </Link>
                </Stack>
              </Grid>
              {errors.submit && (
                <Grid item xs={12}>
                  <FormHelperText error>{errors.submit}</FormHelperText>
                </Grid>
              )}
              <Grid item xs={12}>
                {/*<AnimateButton>*/}
                <Button disableElevation disabled={isSubmitting} fullWidth size="large" type="submit"
                        variant="contained" color="primary">
                  Login
                </Button>
                {/*</AnimateButton>*/}
              </Grid>
              {error?.message ? (
                <Grid item xs={12}>
                  <Stack sx={{width: '100%'}} spacing={0}>
                    <Alert severity="error">{error?.message}</Alert>
                  </Stack>
                </Grid>
              ) : null}
              <Grid item xs={12}>
                <Divider>
                  <Typography variant="caption"> Login with</Typography>
                </Divider>
              </Grid>
              <Grid item xs={12}>
                <SocialSignIn />
              </Grid>
            </Grid>
          </form>
        )}
      </Formik>
    </>
  );
};

export default AuthLogin;
