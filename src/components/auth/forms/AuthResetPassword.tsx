import {
  Button,
  FormHelperText,
  Grid,
  InputLabel,
  OutlinedInput,
  Stack,
  Alert
} from '@mui/material';
import * as Yup from 'yup';
import { Formik, FormikHelpers } from 'formik';
import { useResetPassword } from "@nhost/react";

interface IAuthForgotPasswordValues {
  email: string;
  submit: null
}

const AuthResetPassword = () => {
  const {resetPassword, isSent, isError, error} = useResetPassword();

  const onSubmit = async (_values: IAuthForgotPasswordValues, {
    setErrors,
    setStatus,
    setSubmitting
  }: FormikHelpers<IAuthForgotPasswordValues>) => {
    try {
      await resetPassword(_values.email, {
        redirectTo: '/change-password'
      })
      setStatus({success: false});
      setSubmitting(false);

    } catch (err) {
      // Type assertion to Error
      const error = err as Error;
      setStatus({success: false});
      setErrors({submit: error.message});
      setSubmitting(false);
    }
  }

  return (
    <>
      {isSent ? (
        <Stack sx={{width: '100%', pt: 2}} spacing={2}>
          <Alert severity="info">
            Please check your inbox for an email with a link to reset your password.
          </Alert>
        </Stack>
      ) : (
        <Formik
          initialValues={{
            email: '',
            submit: null
          }}
          validationSchema={Yup.object().shape({
            email: Yup.string().email('Must be a valid email').max(255).required('Email is required'),
          })}
          onSubmit={onSubmit}
        >
          {({errors, handleChange, handleSubmit, isSubmitting, touched, values}) => (
            <form noValidate onSubmit={handleSubmit}>
              <Grid container spacing={3}>
                <Grid item xs={12}>
                  <Stack spacing={1}>
                    <InputLabel htmlFor="email-login">Email</InputLabel>
                    <OutlinedInput
                      id="email-login"
                      type="email"
                      value={values.email}
                      name="email"
                      onChange={handleChange}
                      placeholder="Email"
                      fullWidth
                      error={Boolean(touched.email && errors.email)}
                      autoFocus
                    />
                    {touched.email && errors.email && (
                      <FormHelperText error id="standard-weight-helper-text-email-login">
                        {errors.email}
                      </FormHelperText>
                    )}
                  </Stack>
                </Grid>

                {errors.submit && (
                  <Grid item xs={12}>
                    <FormHelperText error>{errors.submit}</FormHelperText>
                  </Grid>
                )}
                <Grid item xs={12}>
                  {/*<AnimateButton>*/}
                  <Button disableElevation disabled={isSubmitting} fullWidth size="large" type="submit"
                          variant="contained" color="primary">
                    Send Reset Instructions
                  </Button>
                  {/*</AnimateButton>*/}
                </Grid>
                {isError ? (
                  <Grid item xs={12}>
                    <Stack sx={{width: '100%'}} spacing={0}>
                      <Alert severity="error">{error?.message}</Alert>
                    </Stack>
                  </Grid>
                ) : null}
              </Grid>
            </form>
          )}
        </Formik>
      )}
    </>
  );
};

export default AuthResetPassword;
