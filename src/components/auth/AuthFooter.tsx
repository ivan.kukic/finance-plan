import { Typography, Link } from '@mui/material';

const AuthFooter = () => {
  return (
    <Typography variant="subtitle2" color="secondary" component="span">
      &copy; Developed By&nbsp;
      <Link
        variant="subtitle2"
        href="https://www.linkedin.com/in/ivan-kukic"
        target="_blank"
        underline="hover"
      >
        Ivan Kukić
      </Link>
    </Typography>
  );
};

export default AuthFooter;