import { Dispatch, SetStateAction } from "react";
import { Box, Typography } from "@mui/material";
import { CaretDownOutlined, CaretUpOutlined } from "@ant-design/icons";

import { Item } from "@shared/types/Item";

interface IShowMore {
  expanded: boolean;
  setExpanded: Dispatch<SetStateAction<boolean>>
  items: Item[];
  visibleItems: Item[];
}

const ShowMore = ({expanded, setExpanded, items, visibleItems}: IShowMore) => {
  return (
    <Box sx={{textAlign: 'left', mt: 1, display: 'flex', alignItems: 'center', cursor: 'pointer'}}
         onClick={() => setExpanded(!expanded)}>
      {expanded ?
        <CaretUpOutlined style={{color: '#1677ff', marginRight: 6, fontSize: 12}} />
        :
        <CaretDownOutlined style={{color: '#1677ff', marginRight: 6, fontSize: 12}} />}
      <Typography
        component="span"
        variant="caption"
        sx={{color: `primary.main`}}
      >
        {expanded ? 'Show less' : `Show ${items.length - visibleItems.length} more`}
      </Typography>
    </Box>
  );
};

export default ShowMore;
