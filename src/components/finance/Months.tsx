import { SyntheticEvent, useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";

import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import { monthsShort } from "@utils/Helpers";

interface IMonths {
  month: number;
  year: number;
}

const Months = ({month, year}: IMonths) => {
  const navigate = useNavigate();
  const tabsRef = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    if (tabsRef.current) {
      const activeTab = tabsRef.current.querySelector(`[role="tab"][aria-selected="true"]`);
      if (activeTab) {
        (activeTab as HTMLElement).focus();
      }
    }
  }, [month]);

  const handleMonthChange = (_event: SyntheticEvent, month: number) => {
    navigate(`/finance/${year}/${month + 1}`);
  };

  return (
    <Tabs
      ref={tabsRef}
      value={month - 1}
      onChange={(handleMonthChange)}
      variant="scrollable"
      selectionFollowsFocus={true}
    >
      {monthsShort.map((m, index) =>
        <Tab
          sx={{color: '#1677ff', fontWeight: '500', fontSize: 14}}
          label={m}
          key={m}
          onFocus={((e: SyntheticEvent) => handleMonthChange(e, index))}
          disableRipple
        />
      )}
    </Tabs>
  );
};

export default Months;