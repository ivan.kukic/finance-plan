// TODO: Generate prompt for ChatGPT and copy it to clipboard
// TODO: See how to generate advanced prompt and what data to pass
import { useState } from "react";
import {
  Avatar,
  Box, IconButton,
  List,
  ListItemAvatar,
  ListItemButton,
  ListItemSecondaryAction,
  ListItemText,
  Typography
} from '@mui/material';
import { CompassOutlined, DeleteOutlined, EditOutlined, TransactionOutlined } from "@ant-design/icons";

import MainCard from '@components/utility/MainCard';
import { Item } from "@shared/types/Item";
import { actionSX, avatarSX, capitalizeFirstLetter, formatNumberOrNull } from "@utils/Helpers";
import ShowMore from "@components/finance/ShowMore";
import { ItemType } from "@shared/enum/itemType";
import { ItemCategory } from "@shared/enum/ItemCategory";

const TransactionList = ({items}: { items: Item[] }) => {
  const [expanded, setExpanded] = useState(false);
  const limit = 6;

  const [hoveredItem, setHoveredItem] = useState<string | null>(null);

  const handleMouseEnter = (id: string) => setHoveredItem(id);
  const handleMouseLeave = () => setHoveredItem(null);

  const visibleItems = expanded ? items : items?.slice(0, limit);

  return (
    <MainCard sx={{mt: 2}} content={false}>
      <List
        component="nav"
        sx={{
          px: 0,
          py: 0,
          '& .MuiListItemButton-root': {
            py: 1.5,
            position: 'relative',
            '& .MuiAvatar-root': avatarSX,
            '& .MuiListItemSecondaryAction-root': {...actionSX, position: 'relative'}
          }
        }}
      >
        {visibleItems && visibleItems.map((item: Item, index: number) =>
          <ListItemButton
            divider={index !== visibleItems.length - 1}
            key={item.id}
            onMouseEnter={() => handleMouseEnter(item.id || '')}
            onMouseLeave={handleMouseLeave}
          >

            {hoveredItem === item.id && (
              <Box
                sx={{
                  position: 'absolute',
                  top: 0,
                  bottom: 0,
                  right: 0,
                  width: 'auto',
                  height: 'auto',
                  display: 'flex',
                  justifyContent: 'flex-end',
                  alignItems: 'center',
                  gap: 1,
                  p: 1.6,
                  backgroundColor: 'rgba(255, 255, 255, .96)',
                  borderLeft: '1px solid #e6ebf1',
                  zIndex: 999
                }}
              >
                <IconButton><EditOutlined style={{color: '#1677ff'}} /></IconButton>
                <IconButton><DeleteOutlined style={{color: '#1677ff'}} /></IconButton>
              </Box>
            )}

            {item.type === ItemType.Income &&
                <ListItemAvatar>
                    <Avatar
                        sx={{
                          color: 'primary.main',
                          bgcolor: 'primary.lighter'
                        }}
                    >
                      {item.category === ItemCategory.FinancialPlan &&
                          <TransactionOutlined />
                      }
                      {item.category === ItemCategory.TodoPlan &&
                          <CompassOutlined />
                      }
                    </Avatar>
                </ListItemAvatar>
            }
            <ListItemText
              primary={<Typography variant="subtitle1" noWrap
                                   sx={{fontWeight: 'normal'}}>{capitalizeFirstLetter(item.name)}</Typography>}
              secondary={item.type === ItemType.Income ? 'Earned through business, investments or royalties' : ''}
              sx={{pr: {xs: 3, sm: 3, md: 3, lg: 6}}} />
            <ListItemSecondaryAction>
              <Typography variant="subtitle1" sx={{fontWeight: 600}}>
                {formatNumberOrNull(item.price)}
              </Typography>
            </ListItemSecondaryAction>
          </ListItemButton>
        )}
      </List>

      {items && items.length > limit && visibleItems && (
        <Box sx={{ml: 2, mt: -.5, pb: 1}}>
          <ShowMore
            expanded={expanded}
            setExpanded={setExpanded}
            items={items}
            visibleItems={visibleItems}
          />
        </Box>
      )}
    </MainCard>
  );
}

export default TransactionList;