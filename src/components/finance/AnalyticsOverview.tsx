import { ReactNode } from "react";
import { Box, Chip, Grid, Stack, Typography } from '@mui/material';
import { OverridableStringUnion } from '@mui/types';
import { RiseOutlined, FallOutlined } from '@ant-design/icons';

import MainCard from '@components/utility/MainCard';

type ColorType = OverridableStringUnion<
  "default" | "secondary" | "primary" | "error" | "info" | "success" | "warning"
>;

interface IOverview {
  color?: ColorType;  // Use the refined type here
  title?: string;
  amount?: string;
  percentage?: string | null;
  isLoss?: boolean;
  extra?: ReactNode | string;
}

const AnalyticsOverview = ({
                             color = 'primary',
                             title,
                             amount,
                             percentage,
                             isLoss,
                             extra,
                           }: IOverview) => {

  return (
    <MainCard contentSX={{p: 2.25}}>
      <Stack spacing={0.5}>
        <Typography variant="h6" color="textSecondary">
          {title}
        </Typography>
        <Grid container alignItems="center">
          <Grid item>
            <Typography variant="h4" color="inherit">{amount}</Typography>
          </Grid>
          {percentage && (
            <Grid item>
              <Chip
                color={!isLoss ? color : 'warning'}
                icon={
                  <>
                    {!isLoss && <RiseOutlined style={{fontSize: '0.75rem', color: 'inherit'}} />}
                    {isLoss && <FallOutlined style={{fontSize: '0.75rem', color: 'inherit'}} />}
                  </>
                }
                label={percentage}
                sx={{ml: 1.25, pl: 1}}
                size="small"
              />
            </Grid>
          )}
        </Grid>
      </Stack>
      {extra && (
        <Box sx={{pt: 2.25}}>
          <Typography variant="caption" color="textSecondary">
            You made {' '}
            <Typography component="span" variant="caption" sx={{color: `${!isLoss ? color : 'warning'}.main`}}>
              {extra}
            </Typography>{' '}
            {!isLoss ? 'more' : 'less'} then last year
          </Typography>
        </Box>
      )}
    </MainCard>
  );
}

export default AnalyticsOverview;