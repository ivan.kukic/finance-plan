// TODO: Add Income Type with Icon <'Passive Income' | 'Active Income'>
// TODO: Add option to save report
// TODO: Add preferred finance suggestions (eg. Conservative, Enjoy, Invest, SAve, Repay depth etc..)

// TODO; Add Investment Tracking:
// TODO: If users have investments, track their portfolio and predict future growth based on contributions.
import { lazy, Suspense, useCallback, useEffect, useState } from "react";
import { User, useUserData } from "@nhost/react";
import { useParams } from "react-router-dom";
import { Box, Button, Grid, Stack, Typography } from "@mui/material";
import { FileExcelOutlined, FilePdfOutlined, PlusCircleOutlined } from "@ant-design/icons";

import { Item } from "@shared/types/Item";
import Loader from "@components/utility/Loader";
import Error from "@components/utility/Error";
import AnalyticsOverview from "@components/finance/AnalyticsOverview";
import Months from "@components/finance/Months";
import MainCard from "@components/utility/MainCard";
import useGetItems from "@hooks/useGetItems";
import { getMonthlyAnalytics } from "@utils/AnaliticsHelper";
import CrudList from "@components/utility/CrudList";
import { renderCrudList } from "@utils/CrudHelper";
import { CrudCategory } from "@shared/enum/CrudCategory";
import { AIType } from "@shared/enum/AIType";
import AskAI from "@components/utility/AskAI";
import { CrudList as ICrudList, CrudListItem } from "@shared/types/Crud";
import { useAppSelector } from "@hooks/redux";
import { selectedYear } from "@store/reducers/yearSlice";
import { months } from "@utils/Helpers";
import { calculateTotalAmount } from "@utils/ItemHelper";
import ChartLoadingPlaceholder from "@components/utility/ChartLoadingPlaceholder";
import ButtonLoadingPlaceholder from "@components/utility/ButtonLoadingPlaceholder";
import CsvExport from "@components/reports/CsvExport";
import { getDataUri, savePdf } from "@utils/ChartHelper";
import PdfDocument from "@components/reports/pdf/PdfDocument";
import { ReportType } from "@shared/enum/ReportType";

const PieChart = lazy(() => import("@components/dashboard/PieChart"));

const Finance = () => {
  const user: User | null = useUserData();
  const currentYear: number = useAppSelector(selectedYear);
  const currentMonth: number = Math.min(Math.max(parseInt(useParams().month as string, 10) || new Date().getMonth() + 1, 1), 12);

  const [year, setYear] = useState<number>(currentYear);
  const [month, setMonth] = useState<number>(currentMonth);
  const [isDownloading, setIsDownloading] = useState<boolean>(false);
  const [showPdfChart, setShowPdfChart] = useState<boolean>(false);

  const {loading, error, data} = useGetItems({userId: user?.id, year});

  useEffect(() => {
    setMonth(currentMonth);
    setYear(currentYear);
  }, [currentYear, currentMonth]);

  const handleAddIncome = useCallback((event: { preventDefault: () => void; }) => {
    event.preventDefault();
    console.log('onAddIncome')
    // Implement add income logic here
  }, [])

  const handleAddExpense = useCallback((event: { preventDefault: () => void; }) => {
    event.preventDefault();
    console.log('onAddExpense')
    // Implement add expense logic here
  }, [])

  const handlePdfDownloadClick = useCallback(async () => {
    setIsDownloading(true);
    setShowPdfChart(true);

    try {
      // Delay to ensure charts are rendered before capturing
      await new Promise(resolve => setTimeout(resolve, 100));

      // Fetch main chart URI
      const imageURI = await getDataUri('PieChartPdf', 300, 205);

      const pdfDocument = (
        <PdfDocument items={data} month={month} year={year} dataUrl={imageURI} reportType={ReportType.Month} />
      );
      const fileName = `Financial-report-${months[month - 1]}-${year}.pdf`;
      await savePdf(pdfDocument, fileName);
    } catch (error) {
      console.error("Error during PDF generation:", error);
    } finally {
      setIsDownloading(false);
      setShowPdfChart(false);
    }
  }, [data, month, year])

  if (loading) return <Loader />;
  if (error) return <Error apiError={error} />;

  const monthlyIncomeItems: Item[] = data.yearlyIncomeItems.filter((item: Item) => item.month === month);
  const monthlyExpenseItems: Item[] = data.yearlyExpenseItems.filter((item: Item) => item.month === month);
  const monthlyItems = [...monthlyIncomeItems, ...monthlyExpenseItems];

  const monthlyIncome: number = calculateTotalAmount(monthlyIncomeItems);
  const monthlyExpense: number = calculateTotalAmount(monthlyExpenseItems);

  const analytics = getMonthlyAnalytics({items: data, month: month, year});
  delete (analytics as never)['earnYear'];

  const crudIncomeItems: ICrudList[] = renderCrudList(monthlyIncomeItems as CrudListItem[], CrudCategory.Item);
  const crudExpenseItems: ICrudList[] = renderCrudList(monthlyExpenseItems as CrudListItem[], CrudCategory.Item);

  return (
    <>
      <Grid container rowSpacing={4.5} columnSpacing={2.75}>
        <Grid item xs={12} sx={{mb: -2.25}}>
          <Box sx={{display: 'flex', alignItems: 'center'}}>
            <Typography
              variant="h5"
              sx={{flexGrow: 1, mr: 3}}
            >
              {months[month - 1]}
              <span style={{
                color: 'grey',
                fontWeight: 400,
                paddingLeft: 3
              }}>({year})</span>
            </Typography>
            <Months month={month} year={year} />
            <Button color="primary" sx={{py: 0, mr: 1.2, ml: 3, fontWeight: '500', fontSize: 14}}
                    onClick={handlePdfDownloadClick} disabled={isDownloading}>
              {isDownloading ? (
                <ButtonLoadingPlaceholder text="Saving PDF..." />
              ) : (
                <>
                  <FilePdfOutlined style={{paddingRight: 9, color: '#ea4335'}} />
                  Save PDF
                </>
              )}
            </Button>
            <CsvExport items={monthlyItems}>
              <Button color="primary" sx={{py: 0, fontWeight: '500', fontSize: 14}}>
                <FileExcelOutlined style={{paddingRight: 9, color: '#34a853'}} /> Save CSV
              </Button>
            </CsvExport>
          </Box>
        </Grid>

        {analytics && Object.entries(analytics).map(([key, value]) => (
          <Grid item xs={12} sm={4} md={4} lg={4} key={key}>
            <AnalyticsOverview
              title={value.title}
              amount={value.amount}
              percentage={value.percentage}
              extra={value.extra}
              isLoss={value.isLoss}
            />
          </Grid>
        ))}

        <Grid item xs={12} sm={4} md={4} lg={4}>
          <Box sx={{display: 'flex', flexDirection: 'row', justifyContent: 'center'}}>
            <Typography variant="h5" sx={{flexGrow: 1}}>Income</Typography>
            <Button variant="text"
                    sx={{fontWeight: '500', fontSize: 14, alignSelf: "flex-start"}}
                    onClick={handleAddIncome}
                    startIcon={<PlusCircleOutlined
                      style={{fontSize: 13}}
                    />}>
              Add Income
            </Button>
          </Box>
          <CrudList
            items={crudIncomeItems}
            // onEdit={onExpenseEdit}
            // onDelete={onDelete}
            noOfItems={3}
          />
        </Grid>

        <Grid item xs={12} sm={4}>
          <Box sx={{display: 'flex', flexDirection: 'row', justifyContent: 'center'}}>
            <Typography variant="h5" sx={{flexGrow: 1}}>Expense</Typography>
            <Button variant="text"
                    sx={{fontWeight: '500', fontSize: 14, alignSelf: "flex-start"}}
                    onClick={handleAddExpense}
                    startIcon={<PlusCircleOutlined
                      style={{fontSize: 13}}
                    />}>
              Add Expense
            </Button>
          </Box>
          <CrudList
            items={crudExpenseItems}
            // onEdit={onExpenseEdit}
            // onDelete={onDelete}
            noOfItems={3}
          />
        </Grid>

        <Grid item xs={12} sm={4}>
          <Typography variant="h5">Summary</Typography>
          <MainCard sx={{mt: 2}}>
            <Stack spacing={1.5} sx={{mb: 0}}>
              <Typography variant="h6" color="secondary">Income / Expense</Typography>
            </Stack>
            <Suspense fallback={<ChartLoadingPlaceholder height={205} />}>
              <PieChart
                series={[monthlyIncome, monthlyExpense]}
                height={205}
              />
            </Suspense>
          </MainCard>
        </Grid>

        {showPdfChart && (<PieChart
          series={[monthlyIncome, monthlyExpense]}
          height={205}
          isPdfChart={true}
        />)}

        {[AIType.ChatGPT, AIType.Claude, AIType.Perplexity].map((aiType) => (
          <Grid item xs={12} md={4} key={aiType}>
            <AskAI items={monthlyItems} aiType={aiType} />
          </Grid>
        ))}
      </Grid>
    </>
  );
};

export default Finance;