import { useEffect, useState } from 'react'
import { useMutation } from "@apollo/client";
import { useUserId } from "@nhost/react";
import { Box, Grid, Typography } from "@mui/material";
import { FormikHelpers } from "formik";
import * as Yup from "yup";

import CrudForm, { IColumnRender, IFormData, IFormField } from "@components/utility/CrudForm";
import { FormAction } from "@shared/enum/FormAction";
import { ADD_CATEGORY, addCategoryCache, UPDATE_CATEGORY, updateStorageFileCache } from "@services/CategoryService";
import { openNotification } from "@store/reducers/notification";
import { NotificationType } from "@shared/enum/NotificationType";
import { Category } from "@shared/types/Category";
import { Icons } from "@shared/enum/Icons";
import { IconColors } from "@shared/enum/IconColors";
import { ItemType } from "@shared/enum/itemType";
import { useAppDispatch, useAppSelector } from "@hooks/redux";
import { closeCategoryDialog, selectCategoryDialogState } from "@store/reducers/genericDialogSlice";

const EditCategory = () => {
  const userId = useUserId();
  const dispatch = useAppDispatch();

  const {item, formAction, type} = useAppSelector(selectCategoryDialogState);

  const [addCategory] = useMutation(ADD_CATEGORY, {update: addCategoryCache});
  const [updateCategory] = useMutation(UPDATE_CATEGORY, {update: updateStorageFileCache});

  const [formFields, setFormFields] = useState<IFormField[]>(() => getInitialFormFields(item, formAction, type))

  useEffect(() => {
    setFormFields(getInitialFormFields(item, formAction, type))
  }, [item, formAction, type])

  const handleSubmit = async (values: IFormData, formikHelpers: FormikHelpers<IFormData>) => {
    try {
      const formData: Category = {
        ...values,
        icon_color: values.icon_color as string || '',
        type: type,
        users_id: userId
      }

      const {data, errors} = formAction === FormAction.Create
        ? await addCategory({variables: {category: formData}})
        : await updateCategory({variables: {id: formData.id, changes: formData}})

      if (errors) {
        throw new Error(errors[0]?.message)
      }

      if (data) {
        const response = formAction === FormAction.Create
          ? data.insert_category_one
          : data.update_category_by_pk

        dispatch(openNotification({
          type: NotificationType.Success,
          message: `Category "${response.name}" ${formAction === FormAction.Create ? 'created' : 'updated'}.`
        }))

        dispatch(closeCategoryDialog());
      }
    } catch (err) {
      const error = err as Error
      formikHelpers.setStatus({success: false})
      formikHelpers.setErrors({submit: error.message})
      formikHelpers.setSubmitting(false)

      dispatch(openNotification({
        type: NotificationType.Error,
        message: error.message
      }))
    }
  }

  const handleClose = () => {
    dispatch(closeCategoryDialog());
  }

  const validationSchema = Yup.object().shape({
    name: Yup.string().max(255).required('Category Name is required'),
    description: Yup.string().max(255),
    helper: Yup.string().max(255),
  }) as unknown as Yup.ObjectSchema<IFormData>;

  const columnRender: IColumnRender = {
    xs: 12,
    sm: 12,
    md: 6,
    lg: 6,
    xl: 6,
  };

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Box sx={{display: 'flex', alignItems: 'center', mb: 2}}>
          <Typography variant="h5">
            {formAction} {type} Category {formAction === FormAction.Update ? `'${item?.name}'` : ''}
          </Typography>
        </Box>
      </Grid>
      <Grid item xs={12}>
        <CrudForm
          onSubmit={handleSubmit}
          onClose={handleClose}
          formFields={formFields}
          formMode={formAction || FormAction.Create}
          validationSchema={validationSchema}
          columnRender={columnRender}
        />
      </Grid>
    </Grid>
  )
};

const getInitialFormFields = (data: Category | null, formAction: FormAction = FormAction.Create, type: ItemType = ItemType.Income): IFormField[] => {
  const icons = Object.entries(Icons).map(([key, value]) => ({key, value}))
  const iconColors = Object.entries(IconColors).map(([key, value]) => ({key, value}))

  const fields: IFormField[] = [
    {
      id: 'name',
      label: 'Name',
      name: 'name',
      type: 'text',
      required: true,
      value: data?.name || '',
      helperText: 'Please enter Category Name',
    },
    {
      id: 'helper_text',
      label: 'Helper Text',
      name: 'helper_text',
      type: 'text',
      required: false,
      value: data?.helper_text || '',
      helperText: 'Please enter Helper Text',
    },
    {
      id: 'description',
      label: 'Description',
      name: 'description',
      type: 'textarea',
      required: false,
      value: data?.description || '',
      helperText: 'Please enter Category Description',
    },
    {
      id: 'icon',
      label: 'Icon',
      name: 'icon',
      type: 'autocomplete',
      required: true,
      value: data?.icon || '',
      helperText: 'Please choose Category Icon',
      options: icons,
      children: [
        {
          id: 'icon-color',
          label: 'Icon Color',
          name: 'icon_color',
          type: 'dropdown',
          required: false,
          value: data?.icon_color || '',
          helperText: 'Please choose Icon Color',
          options: iconColors
        },
      ]
    },
    {
      id: 'preview-component',
      label: 'Icon Preview',
      name: 'icon-preview',
      type: 'component',
      required: false,
      value: {iconName: 'icon', color: 'icon_color', type: type},
      hidden: false,
    },
  ]

  if (formAction === FormAction.Update) {
    fields.push({
      id: 'id',
      label: 'Id',
      name: 'id',
      type: 'text',
      required: true,
      value: data?.id || null,
      hidden: true
    })
  }

  return fields
}

export default EditCategory;