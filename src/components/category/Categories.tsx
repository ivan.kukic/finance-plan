import { memo, useMemo } from "react";
import { User, useUserData } from "@nhost/react";
import { Box, Button, Dialog, DialogContent, Grid, Typography } from "@mui/material";
import { PlusCircleOutlined } from "@ant-design/icons";

import { ItemType } from "@shared/enum/itemType";
import Loader from "@components/utility/Loader";
import Error from "@components/utility/Error";
import useGetCategories from "@hooks/useGetCategories";
import { Category } from "@shared/types/Category";
import EditCategory from "@components/category/EditCategory";
import Notification from "@components/utility/Notification";
import CategoryConfirmDialog from "@components/category/CategoryConfirmDialog";
import CrudList from "@components/utility/CrudList";
import { renderCrudList } from "@utils/CrudHelper";
import { CrudListItem } from "@shared/types/Crud";
import { CrudCategory } from "@shared/enum/CrudCategory";
import { FormAction } from "@shared/enum/FormAction";
import { useAppDispatch, useAppSelector } from "@hooks/redux";
import {
  openCategoryDialog,
  closeCategoryDialog,
  openCategoryConfirmDialog,
  selectCategoryConfirmDialogOpen,
  selectCategoryDialogOpen
} from "@store/reducers/genericDialogSlice";

const Categories = () => {
  const user: User | null = useUserData();
  const dispatch = useAppDispatch();

  const dialogOpen = useAppSelector(selectCategoryDialogOpen);
  const confirmDialogOpen = useAppSelector(selectCategoryConfirmDialogOpen);

  const {loading, error, data} = useGetCategories({userId: user?.id});

  const {incomeItems, expenseItems} = useMemo(() => {
    if (!data?.items) return {incomeItems: [], expenseItems: []};
    return {
      incomeItems: data.items.filter((item: Category) => item.type === ItemType.Income),
      expenseItems: data.items.filter((item: Category) => item.type === ItemType.Expense)
    };
  }, [data?.items]);

  const crudIncomeItems = useMemo(() => renderCrudList(incomeItems as CrudListItem[], CrudCategory.Category), [incomeItems]);
  const crudExpenseItems = useMemo(() => renderCrudList(expenseItems as CrudListItem[], CrudCategory.Category), [expenseItems]);

  const handleEdit = (type: ItemType) => (field: Category | null) => {
    dispatch(openCategoryDialog({
      item: field,
      formAction: FormAction.Update,
      type
    }));
  };

  const handleAdd = (type: ItemType) => () => {
    dispatch(openCategoryDialog({
      item: null,
      formAction: FormAction.Create,
      type
    }));
  };

  const handleDelete = (field: Category | null) => {
    dispatch(openCategoryConfirmDialog({
      item: field,
      formAction: FormAction.Delete,
      type: ItemType.Income
    }));
  };

  if (loading) return <Loader />;
  if (error) return <Error apiError={error} />;

  return (
    <>
      <Grid container rowSpacing={4.5} columnSpacing={2.75}>
        <Grid item xs={12} sx={{mb: -2.25}}>
          <Box display="flex" alignItems="center">
            <Typography variant="h5" sx={{flexGrow: 1, mr: 3}}>Categories</Typography>
          </Box>
        </Grid>

        {[
          {title: 'Income', items: crudIncomeItems, type: ItemType.Income},
          {title: 'Expense', items: crudExpenseItems, type: ItemType.Expense},
          {title: 'Goal', items: crudExpenseItems, type: ItemType.Expense}
        ].map(({title, items, type}) => (
          <Grid item xs={12} sm={6} key={title}>
            <Box display="flex" flexDirection="row" justifyContent="space-between" alignItems="center">
              <Typography variant="h5">{title}</Typography>
              <Button
                variant="text"
                size="small"
                onClick={handleAdd(type)}
                startIcon={<PlusCircleOutlined style={{fontSize: 13}} />}
                sx={{fontWeight: '500', fontSize: 14}}
              >
                Add {title} Category
              </Button>
            </Box>
            <CrudList<Category>
              items={items}
              onEdit={handleEdit(type)}
              onDelete={handleDelete}
            />
          </Grid>
        ))}

      </Grid>
      <Dialog open={dialogOpen ?? false} onClose={() => dispatch(closeCategoryDialog())}>
        <DialogContent>
          <EditCategory />
        </DialogContent>
      </Dialog>
      <Notification />
      <CategoryConfirmDialog
        dialogOpen={confirmDialogOpen}
        title="Please Confirm Delete"
        description="Are you sure you want to delete this category?"
      />
    </>
  );
};

const MemoizedCategories = memo(Categories);
export default MemoizedCategories;