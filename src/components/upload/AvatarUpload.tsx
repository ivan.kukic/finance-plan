import { useState, ChangeEvent, useRef, useEffect } from "react";
import Avatar from "@mui/material/Avatar";
import IconButton from "@mui/material/IconButton";
import { styled } from "@mui/system";
import { Stack, Typography } from "@mui/material";
import { useNhostClient } from "@nhost/react";
import { useMutation } from "@apollo/client";
import { CameraOutlined } from "@ant-design/icons";

import { GravatarProps } from "@utils/AvatarText";
import { UPDATE_USER_METADATA } from "@services/GraphQLService";
import { selectUser, setUser } from "@store/reducers/authSlice";
import { openNotification } from "@store/reducers/notification";
import { NotificationType } from "@shared/enum/NotificationType";
import {
  ADD_STORAGE_FILE,
  DELETE_STORAGE_FILE_BY_PK,
  addStorageFileCache,
  deleteStorageFileCache
} from "@services/StorageService";
import { StorageFile } from "@shared/types/Storage";
import { StorageLocationType } from "@shared/enum/StorageFileType";
import { getAvatar } from "@utils/GravatarUtill";
import { useAppDispatch, useAppSelector } from "@hooks/redux";

const AvatarContainer = styled("div")({
  position: "relative",
  display: "inline-block",
  "&:hover .uploadButton": {
    opacity: 1,
  },
});

const StyledAvatar = styled(Avatar)({
  width: 124,
  height: 124,
});

const UploadButton = styled(IconButton)({
  position: "absolute",
  width: '100%',
  height: '100%',
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  backgroundColor: "rgba(0, 0, 0, 0.65)", // Semi-transparent background
  borderRadius: "50%",
  color: "white",
  opacity: 0,
  transition: "opacity 0.3s",
  "&:hover": {
    backgroundColor: "rgba(0, 0, 0, 0.7)", // Darker on hover
  },
});

const HiddenInput = styled("input")({
  display: "none",
});

const AvatarUpload = () => {
    const nhost = useNhostClient();
    const dispatch = useAppDispatch();
    const user = useAppSelector(selectUser);

    const [gravatar, setGravatar] = useState<GravatarProps | null>(null);
    const fileInputRef = useRef<HTMLInputElement>(null);

    const [updateUser] = useMutation(UPDATE_USER_METADATA);
    const [addStorageFile] = useMutation(ADD_STORAGE_FILE, {update: addStorageFileCache});
    const [deleteStorageFileByPk] = useMutation(DELETE_STORAGE_FILE_BY_PK, {update: deleteStorageFileCache});

    const handleClick = () => {
      if (fileInputRef.current) {
        fileInputRef.current.click(); // Triggers the file input click event
      }
    };

    const handleUploadClick = (event: ChangeEvent<HTMLInputElement>): void => {
      const file = event.target.files?.[0];
      if (file) {
        uploadAvatar(file);
      }
    };

    // Upload Avatar image to File Storage and Update User Profile
    const uploadAvatar = async (file: File) => {
        try {
          const {fileMetadata: metadata, error: uploadError} = await nhost.storage.upload({file});

          if (uploadError) {
            throw new Error(uploadError.message);
          }

          // Get file public URL
          const publicUrl: string = nhost.storage.getPublicUrl({fileId: metadata.id,});

          const storageFile: StorageFile = {
            used_on: StorageLocationType.UserProfile,
            bucket_id: metadata.bucketId,
            files_id: metadata.id,
            user_id: user?.id,
            name: metadata.name,
            size: metadata.size,
            mime_type: metadata.mimeType,
            etag: metadata.etag,
            is_uploaded: metadata.isUploaded,
            note: '',
            public_url: publicUrl
          }

          const {
            data: storage_file_data,
            errors: add_storage_file_errors
          } = await addStorageFile({variables: {storage_file: storageFile}});

          if (add_storage_file_errors) {
            throw new Error(add_storage_file_errors[0]?.message);
          }

          // Delete from "storage.files"
          if (user?.metadata?.avatar_files_id) {
            const {error} = await nhost.storage.delete({fileId: user.metadata.avatar_files_id});

            if (error) {
              throw new Error(error.message);
            }
          }

          // Delete from "public.storage_file"
          if (user?.metadata?.avatar_storage_file_id) {
            const {errors: delete_storage_file_error} = await deleteStorageFileByPk({variables: {id: user.metadata.avatar_storage_file_id}});

            if (delete_storage_file_error) {
              throw new Error(delete_storage_file_error[0]?.message);
            }
          }

          const {data, errors} = await updateUser({
            variables: {
              id: user?.id,
              metadata: {
                ...user?.metadata,
                avatar_url: publicUrl,
                avatar_storage_file_id: storage_file_data.insert_storage_file_one.id,
                avatar_files_id: metadata.id
              },
            }
          });

          if (errors) {
            throw new Error(errors[0]?.message);
          }

          if (data) {
            dispatch(setUser(data.updateUser));
            dispatch(openNotification({
              type: NotificationType.Success,
              message: "Avatar on user profile has been successfully updated."
            }))
          }
        } catch
          (err) {
          // Type assertion to Error
          const error = err as Error;
          dispatch(openNotification({
            type: NotificationType.Error,
            message: error.message
          }))
        }
      }
    ;

    useEffect(() => {
      if (user) {
        getAvatar(user, setGravatar, 124, 39).then();
      }
    }, [user?.displayName, user?.metadata?.avatar_url]);

    return (
      <AvatarContainer>
        <StyledAvatar alt={user?.displayName} {...gravatar} />
        <HiddenInput
          accept="image/*,.png,.jpg,.jpeg,.gif,.bmp,.webp,.ico"
          id="avatar-upload"
          type="file"
          onChange={handleUploadClick}
          ref={fileInputRef}
        />
        <label htmlFor="avatar-upload">
          <UploadButton className="uploadButton" onClick={handleClick}>
            <Stack spacing={.2} sx={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
              <CameraOutlined style={{fontSize: '2rem'}} />
              <Typography>Choose profile picture</Typography>
            </Stack>
          </UploadButton>
        </label>
      </AvatarContainer>
    );
  }
;

export default AvatarUpload;
