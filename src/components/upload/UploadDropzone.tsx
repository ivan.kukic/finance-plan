import { Box, Link, List, ListItem, ListItemIcon, ListItemText, Typography } from '@mui/material';
import { styled } from "@mui/system";
import { useDropzone } from 'react-dropzone';
import { FileOutlined } from "@ant-design/icons";

import UploadDrop from '@assets/images/files/upload-icon.svg';

interface UploadProps {
  onDrop: (acceptedFiles: File[]) => void;
  accept?: { [key: string]: string[] }; // Optional prop to specify accepted file types
  multiple?: boolean; // Optional prop to allow multiple file uploads
}

const DragDropArea = styled(Box)(({theme}) => ({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center',
  height: '200px',
  border: '1px dashed #8c8c8c',
  borderRadius: '6px',
  backgroundColor: '#f9f9f9',
  cursor: 'pointer',
  transition: 'background-color 0.3s ease',

  '&:hover': {
    backgroundColor: '#e0e0e0',
  },

  '&:hover .icon': {
    color: theme.palette.primary.main, // Changes icon color to primary color on hover
    opacity: .6
  },
}));

const UploadDropzone = ({onDrop}: UploadProps) => {
  const {acceptedFiles, getRootProps, getInputProps, isDragActive} = useDropzone({
    onDrop,
    multiple: true,
  });

  const listItems = acceptedFiles.map(file => (
    <ListItem key={file.name}>
      <ListItemIcon>
        <FileOutlined />
      </ListItemIcon>
      <ListItemText
        primary={file.name}
        secondary={`${(file.size / 1024 / 1024).toFixed(5)} MB`}
      />
    </ListItem>
  ));

  return (
    <>
      <DragDropArea
        {...getRootProps()}
        sx={{
          padding: '20px',
          textAlign: 'center',
          cursor: 'pointer',
          borderRadius: '6px',
          height: '100%',
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
          minHeight: 150,
          mb: 1
        }}
        id="upload-handler"
      >
        <input {...getInputProps()} />
        <img
          src={UploadDrop}
          alt='Drop files'
          className='icon'
          style={{fontSize: 60, color: '#9e9e9e', transition: 'color 0.3s ease', opacity: isDragActive ? .6 : 1}}
        />
        {!isDragActive ? (
          <Box sx={{textAlign: 'left', ml: 3}}>
            <Typography variant="h5" color="seconday">
              Drag & drop or select file
            </Typography>
            <Typography variant="body2" color="seconday">
              Drop files here or click <Link>browse</Link> to select files from your machine
            </Typography>
          </Box>
        ) : (
          <Box sx={{textAlign: 'left', ml: 3, opacity: .6}}>
            <Typography variant="h5" color="seconday">
              Drag & drop or select file
            </Typography>
            <Typography variant="body2" color="seconday">
              Drop files here or click <Link>browse</Link> to select files from your machine
            </Typography>
          </Box>
        )}
      </DragDropArea>
      {listItems.length > 0 && false &&
          <Box>
              <Typography variant="h6">Files ({listItems.length})</Typography>
              <List dense={true}>
                {listItems}
              </List>
          </Box>
      }
    </>
  );
};

export default UploadDropzone;