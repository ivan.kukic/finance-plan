import { useCallback } from "react";
import { useNhostClient, useUserId } from "@nhost/react";
import { useMutation } from "@apollo/client";

import { StorageFile } from "@shared/types/Storage";
import { StorageLocationType } from "@shared/enum/StorageFileType";
import { NotificationType } from "@shared/enum/NotificationType";
import { openNotification } from "@store/reducers/notification";
import { ADD_STORAGE_FILE, addStorageFileCache } from "@services/StorageService";
import UploadDropzone from "./UploadDropzone";
import { useAppDispatch } from "@hooks/redux";

const UploadFilesBuckets = ({folderId}: { folderId: string | null | undefined }) => {
  const nhost = useNhostClient();
  const userId = useUserId()
  const dispatch = useAppDispatch();

  const [addStorageFile] = useMutation(ADD_STORAGE_FILE, {update: addStorageFileCache});

  // Upload Avatar image to File Storage and Update User profile
  const uploadFile = async (file: File) => {
    try {
      const {fileMetadata: metadata, error: uploadError} = await nhost.storage.upload({file});

      if (uploadError) {
        throw new Error(uploadError.message);
      }

      const storageFile: StorageFile = {
        used_on: StorageLocationType.FilesAndBuckets,
        bucket_id: folderId,
        files_id: metadata?.id,
        user_id: userId,
        name: metadata?.name,
        size: metadata?.size,
        mime_type: metadata?.mimeType,
        etag: metadata?.etag,
        is_uploaded: metadata?.isUploaded,
        note: '',
      }

      const {
        data,
        errors: add_storage_file_errors
      } = await addStorageFile({variables: {storage_file: storageFile}});

      if (add_storage_file_errors) {
        throw new Error(add_storage_file_errors[0]?.message);
      }

      if (data) {
        dispatch(openNotification({
          type: NotificationType.Success,
          message: `File "${data.insert_storage_file_one.name}" has been successfully uploaded.`
        }))
      }
    } catch (err) {
      // Type assertion to Error
      const error = err as Error;
      dispatch(openNotification({
        type: NotificationType.Error,
        message: error.message
      }))
    }
  };

  const uploadFilesAndNotify = async (acceptedFiles: File[]) => {
    const uploadPromises = acceptedFiles.map(async (file) => await uploadFile(file));

    // Wait for all the file uploads to complete
    await Promise.all(uploadPromises);

    // After all files are uploaded, dispatch the notification
    dispatch(openNotification({
      type: NotificationType.Success,
      message: `All files have been successfully uploaded.`
    }));
  };

  const handleDrop = useCallback((acceptedFiles: File[]) => uploadFilesAndNotify(acceptedFiles), [folderId]);

  return (
    <UploadDropzone onDrop={handleDrop} />
  )
}

export default UploadFilesBuckets;