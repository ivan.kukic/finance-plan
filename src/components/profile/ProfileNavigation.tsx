import { Link } from "react-router-dom";
import {
  Grid,
  Typography,
  Stack,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  List,
} from "@mui/material";
import { useTheme } from "@mui/material/styles";
import { SettingOutlined, UserOutlined } from "@ant-design/icons";

import MainCard from "@components/utility/MainCard";
import AvatarUpload from "@components/upload/AvatarUpload";
import { useAppSelector } from "@hooks/redux";
import { selectUser } from "@store/reducers/authSlice";

const ProfileNavigation = ({index}: { index: number }) => {
  const theme = useTheme();
  const user = useAppSelector(selectUser);

  return (
    <MainCard contentSX={{p: 2.25}}>
      <Stack spacing={3.5}>
        <Grid
          container
          direction="column"
          justifyContent="center"
          alignItems="center"
        >
          <Grid item sx={{mt: 3}}>
            <AvatarUpload />
          </Grid>
          <Stack spacing={1} sx={{mt: 3, textAlign: "center", maxWidth: '100%'}}>
            <Typography variant="h5" color="textPrimary" noWrap title={user?.displayName}>{user?.displayName}</Typography>
            <Typography variant="body1" color="textSecondary" noWrap title={user?.email}>{user?.email}</Typography>
          </Stack>
        </Grid>
        <Grid container direction="column">
          <Grid item>
            <List component="nav"
                  sx={{p: 0, '& .MuiListItemIcon-root': {minWidth: 32, color: theme.palette.grey[500]}}}>
              <ListItemButton component={Link} selected={index === 0} to="/user-profile">
                <ListItemIcon>
                  <UserOutlined />
                </ListItemIcon>
                <ListItemText primary="User Profile" />
              </ListItemButton>
              <ListItemButton component={Link} selected={index === 1} to="/user-password">
                <ListItemIcon>
                  <SettingOutlined />
                </ListItemIcon>
                <ListItemText primary="Settings" />
              </ListItemButton>
            </List>
          </Grid>
        </Grid>
      </Stack>
    </MainCard>
  );
};

export default ProfileNavigation;
