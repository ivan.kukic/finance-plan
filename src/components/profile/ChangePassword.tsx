import { SyntheticEvent, useEffect, useState } from "react";
import { Link as RouterLink } from "react-router-dom";
import {
  Box,
  Button, FormControl,
  FormHelperText,
  Grid, IconButton, InputAdornment,
  InputLabel, List, ListItem, ListItemText,
  OutlinedInput,
  Stack,
  Typography,
  Divider
} from "@mui/material";
import * as Yup from "yup";
import { Formik, FormikHelpers } from "formik";
import { useChangePassword } from "@nhost/react";
import { EyeInvisibleOutlined, EyeOutlined, LineOutlined } from "@ant-design/icons";

import { openNotification } from "@store/reducers/notification";
import { NotificationType } from "@shared/enum/NotificationType";
import MainCard from "@components/utility/MainCard";
import ProfileNavigation from "@components/profile/ProfileNavigation";
import Notification from "@components/utility/Notification";
import { strengthColor, strengthIndicator } from "@utils/PasswordStrength";
import { useAppDispatch } from "@hooks/redux";

interface IChangePasswordValues {
  oldPassword: string;
  password: string;
  passwordConfirm: string;
  submit: null
}

const ChangePassword = () => {
  const selectedIndex = 1;
  const dispatch = useAppDispatch();

  const [level, setLevel] = useState<{ color: string, label: string } | undefined>(undefined);
  const [showOldPassword, setShowOldPassword] = useState(false);
  const [showNewPassword, setShowNewPassword] = useState(false);
  const [passwordCount, setPasswordCount] = useState(0);
  const handleClickShowOldPassword = () => {
    setShowOldPassword(!showOldPassword);
  };

  const handleClickShowNewPassword = () => {
    setShowNewPassword(!showNewPassword);
  };

  const handleMouseDownPassword = (event: SyntheticEvent) => {
    event.preventDefault();
  };

  const changePasswordStrength = (value: string) => {
    setLevel(strengthColor(strengthIndicator(value)));
    setPasswordCount(passwordCount + 1);
  };

  const {changePassword, isError, error} = useChangePassword();

  useEffect(() => {
    changePasswordStrength('');
  }, []);

  const onSubmit = async (_values: IChangePasswordValues, {
    setErrors,
    setStatus,
    setSubmitting
  }: FormikHelpers<IChangePasswordValues>) => {
    try {
      await changePassword(_values.password);
      setStatus({success: false});
      setSubmitting(false);
      if (isError) {
        dispatch(openNotification({
          type: NotificationType.Error,
          message: error?.message,
        }))
      } else {
        dispatch(openNotification({
          type: NotificationType.Success,
          message: "Password has been successfully changed."
        }))
      }
    } catch (err) {
      // Type assertion to Error
      const error = err as Error;
      setStatus({success: false});
      setErrors({submit: error.message});
      setSubmitting(false);
      dispatch(openNotification({
        type: NotificationType.Error,
        message: error.message
      }))
    }
  }

  return (
    <div className="ChangePassword">
      <Grid container rowSpacing={4.5} columnSpacing={2.75}>
        <Grid item xs={12} sx={{mb: -2.25}}>
          <Typography variant="h5">Account Settings</Typography>
        </Grid>
        <Grid item xs={12} sm={6} md={3} lg={3}>
          <ProfileNavigation index={selectedIndex} />
        </Grid>
        <Grid item xs={12} sm={6} md={9} lg={9}>
          <MainCard contentSX={{p: 2.25}} title="Change password">
            <Formik
              initialValues={{
                oldPassword: '',
                password: '',
                passwordConfirm: '',
                submit: null
              }}
              validationSchema={Yup.object().shape({
                oldPassword: Yup.string().min(8, 'Old Password must be at least 8 characters').max(255).required('Old Password is required'),
                password: Yup.string().min(8, 'New Password must be at least 8 characters').max(255).required('New Password is required'),
                passwordConfirm: Yup.string().min(8, 'Confirm Password must be at least 8 characters').max(255).required('Confirm Password is required').oneOf([Yup.ref("password")], "Passwords do not match")
              })}
              enableReinitialize={true}
              onSubmit={onSubmit}
            >
              {({errors, handleChange, handleSubmit, isSubmitting, touched, values}) => (
                <form noValidate onSubmit={handleSubmit}>
                  <Grid container spacing={5}>
                    <Grid item xs={12} md={6}>
                      <Grid item xs={12} sx={{mb: 4}}>
                        <Stack spacing={1}>
                          <InputLabel htmlFor="oldPassword-signup">Old Password*</InputLabel>
                          <OutlinedInput
                            fullWidth
                            error={Boolean(touched.oldPassword && errors.oldPassword)}
                            id="oldPassword-signup"
                            type={showOldPassword ? 'text' : 'password'}
                            value={values.oldPassword}
                            name="oldPassword"
                            onChange={(e) => {
                              handleChange(e);
                            }}
                            endAdornment={
                              <InputAdornment position="end">
                                <IconButton
                                  aria-label="toggle oldPassword visibility"
                                  onClick={handleClickShowOldPassword}
                                  onMouseDown={handleMouseDownPassword}
                                  edge="end"
                                  size="large"
                                  tabIndex={-1}
                                >
                                  {showOldPassword ? <EyeOutlined /> : <EyeInvisibleOutlined />}
                                </IconButton>
                              </InputAdornment>
                            }
                            placeholder="Old Password"
                            inputProps={{}}
                            autoFocus
                          />
                          {touched.oldPassword && errors.oldPassword && (
                            <FormHelperText error id="helper-text-oldPassword-signup">
                              {errors.oldPassword}
                            </FormHelperText>
                          )}
                        </Stack>
                      </Grid>
                      <Grid item xs={12} sx={{mb: 4}}>
                        <Stack spacing={1}>
                          <InputLabel htmlFor="password-signup">Password*</InputLabel>
                          <OutlinedInput
                            fullWidth
                            error={Boolean(touched.password && errors.password)}
                            id="password-signup"
                            type={showNewPassword ? 'text' : 'password'}
                            value={values.password}
                            name="password"
                            onChange={(e) => {
                              handleChange(e);
                              changePasswordStrength(e.target.value);
                            }}
                            endAdornment={
                              <InputAdornment position="end">
                                <IconButton
                                  aria-label="toggle password visibility"
                                  onClick={handleClickShowNewPassword}
                                  onMouseDown={handleMouseDownPassword}
                                  edge="end"
                                  size="large"
                                  tabIndex={-1}
                                >
                                  {showNewPassword ? <EyeOutlined /> : <EyeInvisibleOutlined />}
                                </IconButton>
                              </InputAdornment>
                            }
                            placeholder="New Password"
                            inputProps={{}}
                          />
                          {touched.password && errors.password && (
                            <FormHelperText error id="helper-text-password-signup">
                              {errors.password}
                            </FormHelperText>
                          )}
                        </Stack>
                        {passwordCount > 3 && (
                          <FormControl fullWidth sx={{mt: 2}}>

                            <Grid container spacing={2} alignItems="center">
                              <Grid item>
                                <Box sx={{bgcolor: level?.color, width: 85, height: 8, borderRadius: '7px'}} />
                              </Grid>
                              <Grid item>
                                <Typography variant="subtitle1" fontSize="0.75rem">
                                  {level?.label}
                                </Typography>
                              </Grid>
                            </Grid>
                          </FormControl>
                        )}
                      </Grid>
                      <Grid item xs={12}>
                        <Stack spacing={1}>
                          <InputLabel htmlFor="passwordConfirm-signup">Confirm password*</InputLabel>
                          <OutlinedInput
                            fullWidth
                            error={Boolean(touched.passwordConfirm && errors.passwordConfirm)}
                            id="passwordConfirm-signup"
                            type={showNewPassword ? 'text' : 'password'}
                            value={values.passwordConfirm}
                            name="passwordConfirm"
                            onChange={(e) => {
                              handleChange(e);
                            }}
                            placeholder="Confirm password"
                            inputProps={{}}
                          />
                          {touched.passwordConfirm && errors.passwordConfirm && (
                            <FormHelperText error id="helper-text-password-signup">
                              {errors.passwordConfirm}
                            </FormHelperText>
                          )}
                        </Stack>
                      </Grid>
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <Box sx={{p: {xs: 2, sm: 1, md: 4}}}>
                        < Typography variant="h5">Strong passwords should contain:</Typography>
                        <List sx={{
                          width: '100%',
                          borderColor: 'divider',
                          backgroundColor: 'background.paper',
                        }}>
                          <ListItem>
                            <LineOutlined />
                            <ListItemText primary="At least 8 characters" sx={{pl: 2}} />
                          </ListItem>
                          <Divider component="li" />
                          <ListItem>
                            <LineOutlined />
                            <ListItemText primary="At least 1 lower letter (a-z)" sx={{pl: 2}} />
                          </ListItem>
                          <Divider component="li" />
                          <ListItem>
                            <LineOutlined />
                            <ListItemText primary="At least 1 uppercase letter (A-Z)" sx={{pl: 2}} />
                          </ListItem>
                          <Divider component="li" />
                          <ListItem>
                            <LineOutlined />
                            <ListItemText primary="At least 1 number (0-9)" sx={{pl: 2}} />
                          </ListItem>
                          <Divider component="li" />
                          <ListItem>
                            <LineOutlined />
                            <ListItemText primary="At least 1 special characters" sx={{pl: 2}} />
                          </ListItem>
                        </List>
                      </Box>
                    </Grid>
                  </Grid>
                  <Grid container direction="row" justifyContent="flex-end" sx={{mt: 1.8}}>
                    <Button disableElevation disabled={isSubmitting} variant="outlined" color="primary"
                            component={RouterLink} to="/dashboard">
                      Cancel
                    </Button>
                    <Button disableElevation disabled={isSubmitting} type="submit" variant="contained"
                            color="primary" sx={{ml: 2}}>
                      Change Password
                    </Button>
                  </Grid>
                </form>
              )}
            </Formik>
          </MainCard>
        </Grid>
      </Grid>
      <Notification />
    </div>
  )
}

export default ChangePassword;
