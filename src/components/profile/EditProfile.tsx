import { useEffect, useState } from "react";
import { Link as RouterLink } from "react-router-dom";
import {
  Button,
  FormHelperText,
  Grid,
  InputLabel,
  OutlinedInput,
  Stack,
  TextField,
  Typography,
} from "@mui/material";
import * as Yup from "yup";
import { Formik, FormikHelpers } from "formik";
import { useMutation } from "@apollo/client";

import { UPDATE_USER } from "@services/GraphQLService";
import { openNotification } from "@store/reducers/notification";
import { NotificationType } from "@shared/enum/NotificationType";
import MainCard from "@components/utility/MainCard";
import ProfileNavigation from "@components/profile/ProfileNavigation";
import Notification from "@components/utility/Notification";
import { selectUser, setUser } from "@store/reducers/authSlice";
import { useAppDispatch, useAppSelector } from "@hooks/redux";

interface IUserProfileValues {
  firstname: string;
  lastname: string;
  email: string;
  company: string;
  note: string;
  submit: null
}

const EditProfile = () => {
  const dispatch = useAppDispatch();
  const user = useAppSelector(selectUser);

  const selectedIndex = 0;

  const [initialValues, setInitialValues] = useState<IUserProfileValues>({
    firstname: '',
    lastname: '',
    email: '',
    company: '',
    note: '',
    submit: null
  });

  const [updateUser] = useMutation(UPDATE_USER);

  const onSubmit = async (_values: IUserProfileValues, {
    setErrors,
    setStatus,
    setSubmitting
  }: FormikHelpers<IUserProfileValues>) => {
    try {
      const {data} = await updateUser({
        variables:
          {
            id: user?.id,
            email: user?.email,
            displayName: `${_values.firstname} ${_values.lastname}`.trim(),
            metadata: {
              ...user?.metadata,
              firstname: _values.firstname.trim(),
              lastname: _values.lastname.trim(),
              company: _values.company.trim(),
              note: _values.note.trim()
            }
          }
      });
      if (data) {
        dispatch(setUser(data.updateUser));
        dispatch(openNotification({
          type: NotificationType.Success,
          message: "User profile has been successfully updated."
        }))
      }
    } catch (err) {
      // Type assertion to Error
      const error = err as Error;
      setStatus({success: false});
      setErrors({submit: error.message});
      setSubmitting(false);
      dispatch(openNotification({
        type: NotificationType.Error,
        message: error.message
      }))
    }
  }

  useEffect(() => {
    if (user) {
      // Update initial values with fetched data
      setInitialValues({
        firstname: user?.metadata?.firstname || '',
        lastname: user?.metadata?.lastname || '',
        email: user.email || '',
        company: user?.metadata?.company || '',
        note: user?.metadata?.note || '',
        submit: null
      });
    }
  }, [user, user?.displayName,user?.metadata?.firstname,user?.metadata?.lastname,user?.metadata?.company,user?.metadata?.note]);

  return (
    <div className="EditProfile">
      <Grid container rowSpacing={4.5} columnSpacing={2.75}>
        <Grid item xs={12} sx={{mb: -2.25}}>
          <Typography variant="h5">User Profile</Typography>
        </Grid>
        <Grid item xs={12} sm={6} md={3} lg={3}>
          <ProfileNavigation index={selectedIndex} />
        </Grid>
        <Grid item xs={12} sm={6} md={9} lg={9}>
          <MainCard contentSX={{p: 2.25}} title="User Profile">
            <Formik
              initialValues={initialValues}
              validationSchema={Yup.object().shape({
                firstname: Yup.string().max(255).required('First Name is required'),
                lastname: Yup.string().max(255).required('Last Name is required'),
                email: Yup.string().email('Must be a valid email').max(255).required('Email is required'),
              })}
              enableReinitialize={true}
              onSubmit={onSubmit}
            >
              {({errors, handleChange, handleSubmit, isSubmitting, touched, values}) => (
                <form noValidate onSubmit={handleSubmit}>
                  <Grid container spacing={3}>
                    <Grid item xs={12} md={6}>
                      <Stack spacing={1}>
                        <InputLabel htmlFor="firstname-signup">First Name</InputLabel>
                        <OutlinedInput
                          id="firstname-login"
                          type="firstname"
                          value={values.firstname}
                          name="firstname"
                          onChange={handleChange}
                          placeholder="First name"
                          fullWidth
                          error={Boolean(touched.firstname && errors.firstname)}
                          autoFocus
                        />
                        {touched.firstname && errors.firstname && (
                          <FormHelperText error id="helper-text-firstname-signup">
                            {errors.firstname}
                          </FormHelperText>
                        )}
                      </Stack>
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <Stack spacing={1}>
                        <InputLabel htmlFor="lastname-signup">Last Name</InputLabel>
                        <OutlinedInput
                          fullWidth
                          error={Boolean(touched.lastname && errors.lastname)}
                          id="lastname-signup"
                          type="lastname"
                          value={values.lastname}
                          name="lastname"
                          onChange={handleChange}
                          placeholder="Last name"
                          inputProps={{}}
                        />
                        {touched.lastname && errors.lastname && (
                          <FormHelperText error id="helper-text-lastname-signup">
                            {errors.lastname}
                          </FormHelperText>
                        )}
                      </Stack>
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <Stack spacing={1}>
                        <InputLabel htmlFor="email-signup">Email</InputLabel>
                        <OutlinedInput
                          fullWidth
                          id="email-login"
                          type="email"
                          value={values.email}
                          name="email"
                          placeholder="Email"
                          inputProps={{}}
                          disabled
                        />
                        {touched.email && errors.email && (
                          <FormHelperText error id="helper-text-email-signup">
                            {errors.email}
                          </FormHelperText>
                        )}
                      </Stack>
                    </Grid>
                    <Grid item xs={12} md={6}>
                      <Stack spacing={1}>
                        <InputLabel htmlFor="company-signup">Company</InputLabel>
                        <OutlinedInput
                          fullWidth
                          error={Boolean(touched.company && errors.company)}
                          id="company-signup"
                          value={values.company}
                          name="company"
                          onChange={handleChange}
                          placeholder="Company"
                          inputProps={{}}
                        />
                        {touched.company && errors.company && (
                          <FormHelperText error id="helper-text-company-signup">
                            {errors.company}
                          </FormHelperText>
                        )}
                      </Stack>
                    </Grid>
                    <Grid item xs={12}>
                      <Stack spacing={1}>
                        <InputLabel htmlFor="note">Note</InputLabel>
                        <TextField
                          multiline
                          fullWidth
                          minRows={5}
                          error={Boolean(touched.note && errors.note)}
                          id="note"
                          value={values.note}
                          name="note"
                          onChange={handleChange}
                          placeholder="Note"
                          inputProps={{}}
                          sx={{mt: 6}}
                        ></TextField>
                      </Stack>
                    </Grid>
                  </Grid>
                  <Grid container direction="row" justifyContent="flex-end" sx={{mt: 2.5}}>
                    <Button disableElevation disabled={isSubmitting} variant="outlined" color="primary"
                            component={RouterLink} to="/dashboard">
                      Cancel
                    </Button>
                    <Button disableElevation disabled={isSubmitting} type="submit" variant="contained"
                            color="primary" sx={{ml: 2}}>
                      Update Profile
                    </Button>
                  </Grid>
                </form>
              )}
            </Formik>
          </MainCard>
        </Grid>
      </Grid>
      <Notification />
    </div>
  )
}

export default EditProfile;
