import { useMemo } from "react";
import { Page, View, Image, Text } from "@react-pdf/renderer";

import { styles } from "@utils/PdfHelper";
import { IGetItems } from "@hooks/useGetItems";
import { getMonthlyAnalytics } from "@utils/AnaliticsHelper";
import PdfList from "@components/reports/pdf/PdfList";
import { Item } from "@shared/types/Item";
import PdfAnalyticsTitle from "@components/reports/pdf/PdfAnalyticsTitle";
import { months } from "@utils/Helpers";
import PdfAnalytics from "@components/reports/pdf/PdfAnalytics";
import { ReportType } from "@shared/enum/ReportType";

export interface IPdfAnalyticsMonth {
  items: IGetItems;
  month?: number;
  year?: number;
  quarter?: number;
  dataUrl?: string | null;
  reportType?: ReportType;
}

const PdfAnalyticsMonth = ({
                             items,
                             month = 1,
                             year = new Date().getFullYear(),
                             quarter = 1,
                             dataUrl,
                             reportType = ReportType.Year,
                           }: IPdfAnalyticsMonth) => {
  const title = `${months[month - 1]}, ${year || 0}`;
  const subtitle = reportType === ReportType.Quarter ? `Q${quarter}, ${months[month - 1]}` : `Month, ${months[month - 1]}`;

  const analytics = useMemo(() => getMonthlyAnalytics({items, month, year}), [items, month, year]);

  const filteredIncomeItems = useMemo(() =>
      items.incomeItems.filter((item: Item) => item.month === month && item.year === year),
    [items, month, year]
  );

  const filteredExpenseItems = useMemo(() =>
      items.expenseItems.filter((item: Item) => item.month === month && item.year === year),
    [items, month, year]
  );

  return (
    <Page size="A4" orientation="landscape" wrap={false} style={styles.page}>
      <View style={styles.column}>
        <PdfAnalyticsTitle title={title} subtitle={subtitle} />
      </View>
      <View style={styles.row}>
        {analytics && Object.entries(analytics).map(([key, value]) => (
          <PdfAnalytics key={key}
                        title={value.title}
                        amount={value.amount}
                        percentage={value.percentage}
                        extra={value.extra}
                        isLoss={value.isLoss}
          />
        ))}
      </View>
      <View style={[styles.row, {width: '100%'}]}>
        <View style={[styles.column, {width: '66.666%'}]}>
          <PdfList
            title="Income"
            items={filteredIncomeItems}
            isLoss={false}
          />
          <PdfList
            title="Expense"
            items={filteredExpenseItems}
            isLoss={false}
          />
        </View>
        {dataUrl && (
          <View style={[styles.analyticsWrap, styles.column, {width: '33.333%', marginRight: 0}]}>
            <View style={{flexGrow: 0, marginTop: 42, paddingLeft: 7}}>
              <View style={[styles.analyticsTitle, {marginBottom: 30}]}>
                <Text>Summary</Text>
              </View>
              <Image
                style={[styles.chartImageFull, {marginTop: 0, maxHeight: 180, padding: '0 24px'}]}
                src={dataUrl}
              />
            </View>
          </View>
        )}
      </View>
    </Page>
  );
};

export default PdfAnalyticsMonth;