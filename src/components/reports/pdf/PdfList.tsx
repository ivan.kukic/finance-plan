import { useMemo } from "react"
import { Text, View } from "@react-pdf/renderer";

import { styles } from "@utils/PdfHelper";
import { Item } from "@shared/types/Item";
import { capitalizeFirstLetter, formatNumberOrNull } from "@utils/Helpers";

interface IPdfList {
  title: string;
  items: Item[];
  isLoss: boolean;
}

const PdfList = ({title, items, isLoss}: IPdfList) => {
  const warningColor = '#faad14';

  const listItems = useMemo(() => items.map((item: Item, index: number) => (
    <View
      key={item.id}
      style={{
        ...styles.analyticsInner,
        marginBottom: 9
      }}
    >
      <View
        wrap={true}
        style={{
          ...styles.listItemText,
          flex: 1,
          flexGrow: 1,
          paddingRight: 60,
          flexDirection: 'row'
        }}
      >
        <Text style={{display: 'flex', paddingLeft: 15}}>{index + 1}. </Text>
        <Text style={{display: 'flex'}}>{capitalizeFirstLetter(item.name)}</Text>
      </View>

      <View
        style={{
          ...styles.listItemText,
          ...(isLoss ? {color: warningColor} : {}),
        }}
      >
        <Text style={{fontWeight: 'bold', textAlign: 'right'}}>{formatNumberOrNull(item.price)}</Text>
      </View>
    </View>
  )), [items, isLoss, warningColor])

  return (
    <View
      style={{
        ...styles.analyticsWrap,
        display: 'flex',
        flexDirection: 'column',
        padding: 0,
        paddingRight: 60,
        margin: 0,
        marginTop: 42
      }}
    >
      <View style={{...styles.analyticsTitle, marginBottom: 15}}>
        <Text>{title} Items</Text>
      </View>

      {listItems}
    </View>
  )
};

export default PdfList;