// TODO: Create link to go to application report for that month and year (and other params like category etc...)
import { Document, Font } from "@react-pdf/renderer";

import { IGetItems } from "@hooks/useGetItems";
import { ReportType } from "@shared/enum/ReportType";
import PdfAnalyticsYear from "@components/reports/pdf/PdfAnalyticsYear";
import PdfAnalyticsMonth from "@components/reports/pdf/PdfAnalyticsMonth";
import { calculateQuarterOffset } from "@utils/MathHelper";

Font.register({
  family: 'Roboto',
  src: 'https://fonts.gstatic.com/s/roboto/v16/zN7GBFwfMP4uA6AR0HCoLQ.ttf'
});

export interface IPdfDocument {
  items: IGetItems;
  month?: number;
  year?: number;
  quarter?: number;
  reportType?: ReportType;
  dataUrl?: string | null;
  dataUrls?: Array<string> | null;
}

const PdfDocument = ({
                       items,
                       month,
                       year,
                       quarter,
                       dataUrl,
                       dataUrls,
                       reportType = ReportType.Year,
                     }: IPdfDocument) => {
  const renderReportContent = (reportType: ReportType, selectedMonth: number | undefined) => {
    const noOfMonths = reportType === ReportType.Year ? 12 : 3;

    const analyticsContent = Array.from({length: noOfMonths}, (_, i) => {
        const monthOffset = reportType === ReportType.Quarter ? calculateQuarterOffset({quarter, reportType}) : 0;

        return (
          <PdfAnalyticsMonth
            key={`${monthOffset + i + 1}-${year}`}
            items={items}
            month={monthOffset + i + 1}
            year={year}
            quarter={quarter}
            reportType={reportType}
            dataUrl={dataUrls?.[i] || null}
          />
        )
      }
    );

    switch (reportType) {
      case ReportType.Year:
      case ReportType.Quarter: {
        return (
          <>
            <PdfAnalyticsYear
              items={items}
              year={year}
              quarter={quarter}
              dataUrl={dataUrl}
              reportType={reportType}
            />
            {analyticsContent}
          </>
        )
      }
      case ReportType.Month: {
        return (
          <PdfAnalyticsMonth
            items={items}
            month={selectedMonth}
            year={year}
            dataUrl={dataUrls && selectedMonth !== undefined && dataUrls[selectedMonth] ? dataUrls[selectedMonth] : dataUrl}
            reportType={reportType}
          />
        );
      }
      default: {
        return (
          <></>
        );
      }
    }
  };

  return (
    <Document>
      {renderReportContent(reportType, month)}
    </Document>
  );
};

export default PdfDocument;