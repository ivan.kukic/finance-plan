import { Page, View, Image } from "@react-pdf/renderer";

import PdfAnalytics from "@components/reports/pdf/PdfAnalytics";
import { styles } from "@utils/PdfHelper";
import { IGetItems } from "@hooks/useGetItems";
import { getQuarterlyAnalytics, getYearlyAnalytics } from "@utils/AnaliticsHelper";
import PdfAnalyticsTitle from "@components/reports/pdf/PdfAnalyticsTitle";
import { ReportType } from "@shared/enum/ReportType";

export interface IPdfAnalyticsYear {
  items: IGetItems;
  month?: number;
  year?: number;
  quarter?: number;
  dataUrl?: string | null;
  reportType?: ReportType;
}

const PdfAnalyticsYear = ({
                            items,
                            year = new Date().getFullYear(),
                            quarter = 1,
                            dataUrl,
                            reportType = ReportType.Year
                          }: IPdfAnalyticsYear) => {
  const title = `Financial report, ${year}`;
  const subtitle = `${reportType === ReportType.Year ? 'Year' : 'Q' + quarter}, ${year || 0}`;

  const analytics = items && (reportType === ReportType.Year
    ? getYearlyAnalytics({items: items, year})
    : getQuarterlyAnalytics({items: items, year, quarter}));

  return (
    <Page size="A4" orientation="landscape" style={styles.page}>
      <View style={styles.column}>
        <PdfAnalyticsTitle title={title} subtitle={subtitle} />
      </View>
      <View style={styles.row}>
        {analytics && Object.entries(analytics).map(([key, value]) => (
          <PdfAnalytics key={key}
                        title={value.title}
                        amount={value.amount}
                        percentage={value.percentage}
                        extra={value.extra}
                        isLoss={value.isLoss}
          />
        ))}
      </View>
      {dataUrl && (
        <View style={styles.column}>
          <Image style={styles.chartImageFull} src={dataUrl} />
        </View>
      )}
    </Page>
  )
};

export default PdfAnalyticsYear;