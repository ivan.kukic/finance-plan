import { useMemo } from "react";
import { View, Text } from "@react-pdf/renderer";

import { styles } from "@utils/PdfHelper";
import { formatDate } from "@utils/Helpers";

export interface IPdfAnalyticsHeader {
  title?: string,
  subtitle?: string
}

const PdfAnalyticsTitle = ({title, subtitle}: IPdfAnalyticsHeader) => {
  const header = useMemo(() => `Created at: ${formatDate(new Date())}h`, [])

  return (
    <>
      <View style={styles.header}>
        <Text fixed>{header}</Text>
      </View>
      <View style={styles.title}>
        <Text>{title || 'No Title Provided'}</Text>
      </View>
      <View style={styles.subtitle}>
        <Text>{subtitle || 'No Subtitle Provided'}</Text>
      </View>
    </>
  );
};

export default PdfAnalyticsTitle;