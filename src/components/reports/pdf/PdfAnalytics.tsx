import { useMemo } from "react";
import { Text, View } from "@react-pdf/renderer";

import { styles } from "@utils/PdfHelper";

interface IPdfAnalytics {
  title: string,
  amount: string,
  percentage: string | null,
  extra: string,
  isLoss: boolean,
}

const PdfAnalytics = ({title, amount, percentage, extra, isLoss}: IPdfAnalytics) => {
  const warningColor = '#faad14';

  const analyticsChipStyle = useMemo(() => ({
    ...styles.analyticsChip,
    ...(isLoss ? {color: warningColor} : {})
  }), [isLoss]);

  const extraTextStyle = useMemo(() => ({
    ...styles.primary,
    ...(isLoss ? {color: warningColor} : {})
  }), [isLoss])

  return (
    <View style={styles.analyticsWrap}>
      <View style={styles.analyticsTitle}>
        <Text>{title}</Text>
      </View>
      <View style={styles.analyticsInner}>
        <View style={styles.analyticsAmount}>
          <Text>{amount}</Text>
        </View>
        {percentage && (
          <View style={analyticsChipStyle}>
            <Text>{percentage}</Text>
          </View>
        )}
      </View>
      <View style={styles.analyticsText}>
        <Text>
          You made <Text style={extraTextStyle}>
          {extra}
        </Text> {isLoss ? 'less' : 'more'} than last year
        </Text>
      </View>
    </View>
  );
};

export default PdfAnalytics;