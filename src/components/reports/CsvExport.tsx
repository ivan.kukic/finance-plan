import { CSVLink } from "react-csv";
import { ReactNode } from "react";

import { Item } from "@shared/types/Item";
import { csvHeaders, monthsShort } from "@utils/Helpers";

interface ICsvExport {
  items: Item[],
  children: ReactNode;
}

const CsvExport = ({items, children}: ICsvExport) => {
  const fileName = 'Financial-report.csv'

  const data = items.map((item: Item) => ({
    ...item,
    month: monthsShort[item.month || 0]
  }));

  return (
    <CSVLink data={data}
             headers={csvHeaders}
             filename={fileName}
    >
      {children}
    </CSVLink>
  );
};

export default CsvExport;