import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { User, useUserData } from "@nhost/react";
import { PDFViewer } from "@react-pdf/renderer";

import PdfDocument from "@components/reports/pdf/PdfDocument";
import Loader from "@components/utility/Loader";
import Error from "@components/utility/Error";
import useGetItems from "@hooks/useGetItems";
import { styles } from "@utils/PdfHelper";
import { ReportType } from "@shared/enum/ReportType";
import { useAppSelector } from "@hooks/redux";
import { selectedYear } from "@store/reducers/yearSlice";
import { Item } from "@shared/types/Item";
import { getQuarterlyTotalPerMonths, getTotal, getYearlyTotalPerMonths } from "@utils/ItemHelper";
import RevenueColumnChart from "@components/dashboard/RevenueColumnChart";
import { getDataUri } from "@utils/ChartHelper";
import { getImageURIArray, getPieChartArray } from "@utils/ImgUriHelper";

const Reports = () => {
  const user: User | null = useUserData();
  const currentYear: number = useAppSelector(selectedYear);
  const currentMonth: number = Math.min(Math.max(parseInt(useParams().month as string, 10) || new Date().getMonth() + 1, 1), 12);

  const [year] = useState<number>(currentYear);
  const [month] = useState<number>(currentMonth);
  const [quarter] = useState<number>(1);
  const [dataUrl, setDataUrl] = useState<string | null>(null);
  const [dataUrls, setDataUrls] = useState<string[] | null>(null);
  const [reportType] = useState<ReportType>(ReportType.Year);

  const {loading, error, data} = useGetItems({userId: user?.id, year});

  useEffect(() => {
    const fetchData = async () => {
      try {
        // Delay to ensure charts are rendered before capturing
        await new Promise(resolve => setTimeout(resolve, 900));

        // Fetch main chart URI
        const imageURI = await getDataUri('RevenueColumnChartPdf', 1200, 420);

        // Fetch URIs for pie charts
        const imageURIArray = await getImageURIArray({reportType, quarter});

        // Filter out invalid URIs
        const validImageURIs = imageURIArray.filter(
          (uri): uri is string => uri !== '' && uri !== null
        );

        // Update states if URIs are valid
        if (imageURI) {
          setDataUrl(imageURI);
        }

        if (validImageURIs.length > 0) {
          setDataUrls(validImageURIs);
        } else {
          console.warn('No valid image URIs found for pie charts.');
          setDataUrls(null); // Fallback to null if no valid URIs
        }
      } catch (error) {
        console.error('Failed to fetch image URIs:', error);
        setDataUrl(dataUrl || null); // Use existing or null
        setDataUrls(dataUrls || null); // Use existing or null
      }
    };

    // Fetch data with a delay to prevent overlapping updates
    const timeout = setTimeout(() => fetchData(), 900);

    return () => clearTimeout(timeout); // Cleanup timeout on component unmount
  }, [currentYear, currentMonth, setDataUrl, setDataUrls, reportType, quarter]);

  if (loading || !data) return <Loader />;
  if (error) return <Error apiError={error} />;

  const items: Item[] = data.items as Item[];
  const selectedYearItems: Item[] = items.filter((item: Item) => (item.year === year)) as Item[];
  const yearlyTotalAmount = getYearlyTotalPerMonths(selectedYearItems);
  const quarterlyTotalAmount = getQuarterlyTotalPerMonths(selectedYearItems, quarter);

  const yearIncomePerMonth = yearlyTotalAmount.income;
  const yearExpensePerMonth = yearlyTotalAmount.expense;
  const quarterIncomePerMonth = quarterlyTotalAmount.income;
  const quarterExpensePerMonth = quarterlyTotalAmount.expense;

  return (
    <>
      <RevenueColumnChart
        incomeTotal={getTotal(reportType, yearIncomePerMonth, quarterIncomePerMonth)}
        expenseTotal={getTotal(reportType, yearExpensePerMonth, quarterExpensePerMonth)}
        height={420}
        isPdfChart={true}
        reportType={reportType}
        quarter={quarter}
      />

      {getPieChartArray({
        reportType,
        quarter,
        income: getTotal(reportType, yearIncomePerMonth, quarterIncomePerMonth),
        expense: getTotal(reportType, yearExpensePerMonth, quarterExpensePerMonth)
      })}

      <PDFViewer style={styles.viewer}>
        <PdfDocument
          items={data}
          month={month}
          year={year}
          quarter={quarter}
          dataUrl={dataUrl}
          dataUrls={dataUrls}
          reportType={reportType}
        />
      </PDFViewer>

    </>
  );
};

export default Reports;