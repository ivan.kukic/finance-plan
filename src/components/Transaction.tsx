// import React, { useState } from 'react';
// import { useQuery, useMutation, gql } from '@apollo/client';
// import { DataGrid, GridColDef } from '@mui/x-data-grid';
// import Tabs from '@mui/material/Tabs';
// import Tab from '@mui/material/Tab';
// import Button from '@mui/material/Button';
// import Dialog from '@mui/material/Dialog';
//
// const GET_TRANSACTIONS = gql`
//   query GetTransactions {
//     transactions {
//       id
//       date
//       description
//       amount
//       type
//     }
//   }
// `;
//
// const ADD_TRANSACTION = gql`
//   mutation AddTransaction($date: String!, $description: String!, $amount: Float!, $type: String!) {
//     addTransaction(date: $date, description: $description, amount: $amount, type: $type) {
//       id
//       date
//       description
//       amount
//       type
//     }
//   }
// `;
//
// const UPDATE_TRANSACTION = gql`
//   mutation UpdateTransaction($id: ID!, $date: String!, $description: String!, $amount: Float!, $type: String!) {
//     updateTransaction(id: $id, date: $date, description: $description, amount: $amount, type: $type) {
//       id
//       date
//       description
//       amount
//       type
//     }
//   }
// `;
//
// const DELETE_TRANSACTION = gql`
//   mutation DeleteTransaction($id: ID!) {
//     deleteTransaction(id: $id) {
//       id
//     }
//   }
// `;
//
// interface Transaction {
//   id: string;
//   date: string;
//   description: string;
//   amount: number;
//   type: string;
// }
//
// interface AddTransactionDialogProps {
//   open: boolean;
//   onClose: () => void;
//   addTransaction: (variables: any) => void;
// }
//
// interface EditTransactionDialogProps {
//   open: boolean;
//   onClose: () => void;
//   transaction: Transaction | null;
//   updateTransaction: (variables: any) => void;
// }
//
// const AddTransactionDialog: React.FC<AddTransactionDialogProps> = ({ open, onClose, addTransaction }) => {
//   // AddTransactionDialog component code...
//   return null;
// };
//
// const EditTransactionDialog: React.FC<EditTransactionDialogProps> = ({ open, onClose, transaction, updateTransaction }) => {
//   // EditTransactionDialog component code...
//   return null;
// };
//
// const TransactionList: React.FC = () => {
//   const { loading, error, data } = useQuery(GET_TRANSACTIONS);
//   const [selectedMonth, setSelectedMonth] = useState<number>(0);
//   const [openAddDialog, setOpenAddDialog] = useState<boolean>(false);
//   const [openEditDialog, setOpenEditDialog] = useState<boolean>(false);
//   const [selectedTransaction, setSelectedTransaction] = useState<Transaction | null>(null);
//
//   const [addTransaction] = useMutation(ADD_TRANSACTION, {
//     refetchQueries: [{ query: GET_TRANSACTIONS }],
//   });
//
//   const [updateTransaction] = useMutation(UPDATE_TRANSACTION, {
//     refetchQueries: [{ query: GET_TRANSACTIONS }],
//   });
//
//   const [deleteTransaction] = useMutation(DELETE_TRANSACTION, {
//     refetchQueries: [{ query: GET_TRANSACTIONS }],
//   });
//
//   if (loading) return <p>Loading...</p>;
//   if (error) return <p>Error: {error.message}</p>;
//
//   const transactions: Transaction[] = data.transactions;
//
//   const handleTabChange = (event: React.ChangeEvent<{}>, newValue: number) => {
//     setSelectedMonth(newValue);
//   };
//
//   const handleOpenAddDialog = () => {
//     setOpenAddDialog(true);
//   };
//
//   const handleCloseAddDialog = () => {
//     setOpenAddDialog(false);
//   };
//
//   const handleOpenEditDialog = (transaction: Transaction) => {
//     setSelectedTransaction(transaction);
//     setOpenEditDialog(true);
//   };
//
//   const handleCloseEditDialog = () => {
//     setOpenEditDialog(false);
//   };
//
//   const handleDeleteTransaction = (id: string) => {
//     deleteTransaction({ variables: { id } });
//   };
//
//   const calculateTotalAmount = (transactions: Transaction[]): number => {
//     return transactions.reduce((total, transaction) => total + parseFloat(transaction.amount.toString()), 0);
//   };
//
//   const getMonthlyTransactions = (transactions: Transaction[], month: number): Transaction[] => {
//     return transactions.filter(transaction => new Date(transaction.date).getMonth() === month);
//   };
//
//   const columns: GridColDef[] = [
//     { field: 'id', headerName: 'ID', width: 90 },
//     { field: 'date', headerName: 'Date', width: 150 },
//     { field: 'description', headerName: 'Description', width: 250 },
//     { field: 'amount', headerName: 'Amount', width: 150 },
//     { field: 'type', headerName: 'Type', width: 150 },
//     {
//       field: 'actions',
//       headerName: 'Actions',
//       width: 150,
//       renderCell: (params) => (
//         <div>
//           <Button variant="outlined" onClick={() => handleOpenEditDialog(params.row as Transaction)}>Edit</Button>
//           <Button variant="outlined" color="error" onClick={() => handleDeleteTransaction((params.row as Transaction).id)}>Delete</Button>
//         </div>
//       ),
//     },
//   ];
//
//   const monthlyTransactions: Transaction[] = getMonthlyTransactions(transactions, selectedMonth);
//   const monthlyIncome: number = calculateTotalAmount(monthlyTransactions.filter(transaction => transaction.type === 'income'));
//   const monthlyExpense: number = calculateTotalAmount(monthlyTransactions.filter(transaction => transaction.type === 'expense'));
//
//   const yearlyIncome: number = calculateTotalAmount(transactions.filter(transaction => transaction.type === 'income'));
//   const yearlyExpense: number = calculateTotalAmount(transactions.filter(transaction => transaction.type === 'expense'));
//
//   const monthlyTotalRow: Transaction = { id: 'monthlyTotal', date: 'Total', description: '', amount: monthlyIncome - monthlyExpense, type: '' };
//   const yearlyTotalRow: Transaction = { id: 'yearlyTotal', date: 'Total', description: '', amount: yearlyIncome - yearlyExpense, type: '' };
//
//   return (
//     <div>
//       <Tabs value={selectedMonth} onChange={handleTabChange}>
//         <Tab label="January" />
//         <Tab label="February" />
//         {/* Add more tabs for other months */}
//       </Tabs>
//       <div>
//         <h3>Income</h3>
//         <Button variant="contained" onClick={handleOpenAddDialog}>Add Income Transaction</Button>
//         <DataGrid
//           rows={monthlyTransactions.filter(transaction => transaction.type === 'income')}
//           columns={columns}
//           pageSize={5}
//           rowsPerPageOptions={[5, 10, 20]}
//           checkboxSelection
//           autoHeight
//         />
//         <p>Total Income: {monthlyIncome.toFixed(2)}</p>
//       </div>
//       <div>
//         <h3>Expense</h3>
//         <Button variant="contained" onClick={handleOpenAddDialog}>Add Expense Transaction</Button>
//         <DataGrid
//           rows={monthlyTransactions.filter(transaction => transaction.type === 'expense')}
//           columns={columns}
//           pageSize={5}
//           rowsPerPageOptions={[5, 10, 20]}
//           checkboxSelection
//           autoHeight
//         />
//         <p>Total Expense: {monthlyExpense.toFixed(2)}</p>
//       </div>
//       <div>
//         <h3>Monthly Report</h3>
//         <p>Remaining Amount: {(monthlyIncome - monthlyExpense).toFixed(2)}</p>
//         <DataGrid
//           rows={[monthlyTotalRow]}
//           columns={columns}
//           pageSize={1}
//           rowsPerPageOptions={[1]}
//           autoHeight
//           disableColumnMenu
//           disableColumnSelector
//         />
//       </div>
//       <div>
//         <h3>Yearly Report</h3>
//         <p>Yearly Income: {yearlyIncome.toFixed(2)}</p>
//         <p>Yearly Expense: {yearlyExpense.toFixed(2)}</p>
//         <p>Yearly Remaining Amount: {(yearlyIncome - yearlyExpense).toFixed(2)}</p>
//         <DataGrid
//           rows={[yearlyTotalRow]}
//           columns={columns}
//           pageSize={1}
//           rowsPerPageOptions={[1]}
//           autoHeight
//           disableColumnMenu
//           disableColumnSelector
//         />
//       </div>
//
//       {/* Add Transaction Dialog */}
//       <AddTransactionDialog
//         open={openAddDialog}
//         onClose={handleCloseAddDialog}
//         addTransaction={addTransaction}
//       />
//
//       {/* Edit Transaction Dialog */}
//       <EditTransactionDialog
//         open={openEditDialog}
//         onClose={handleCloseEditDialog}
//         transaction={selectedTransaction}
//         updateTransaction={updateTransaction}
//       />
//     </div>
//   );
// };
//
// export default TransactionList;
