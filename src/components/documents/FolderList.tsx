import { SyntheticEvent, useState } from "react";
import { useDrag, useDrop } from "react-dnd";
import { Checkbox, IconButton, ListItem, ListItemIcon, ListItemText } from "@mui/material";
import { FolderOutlined, MoreOutlined } from "@ant-design/icons";

import { DocumentType, File, Folder } from "@shared/types/Storage";
import ActionMenu from "@components/documents/ActionMenu";

interface IFolderList {
  folder: Folder;
  isSelected: boolean;
  onSelect: (item: Folder) => void;
  onDoubleClick: () => void;
  onDrop: (item: File | Folder, targetFolderId: string | undefined) => void;
}

const FolderList = ({folder, isSelected, onSelect, onDoubleClick, onDrop}: IFolderList) => {
  const [anchorEl, setAnchorEl] = useState<null | Element>(null);
  const [selectedFolder, setSelectedFolder] = useState<Folder | null>(null);

  const handleMenuClick = (event: SyntheticEvent, folder: Folder) => {
    setAnchorEl(event.currentTarget);
    setSelectedFolder(folder);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    setSelectedFolder(null);
  };

  const [{isOver, canDrop}, drop] = useDrop({
    accept: [DocumentType.File, DocumentType.Folder],
    drop: (item: Folder | File) => {
      if (item.id !== folder.id) {
        onDrop(item, folder.id);
      }
    },
    canDrop: (item: Folder | File) => item.id !== folder.id, // Prevent dropping onto itself
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  });

  const [, drag] = useDrag({
    type: DocumentType.Folder,
    item: {id: folder.id, type: DocumentType.Folder},
  });

  const isActive = canDrop && isOver;
  const folderStyle = {
    border: isActive ? '1px solid green' : '1px solid #f0f0f0',
    backgroundColor: isActive ? '#e0f7fa' : 'white',
    transition: 'background-color 0.2s, border 0.2s',
    position: 'relative'
  };

  return (
    <div onDoubleClick={onDoubleClick} ref={(node) => drag(drop(node))}>
      <ListItem key={folder.id}
                button
                disableRipple
                sx={folderStyle}
      >
        <Checkbox
          checked={isSelected}
          onChange={() => onSelect(folder)}
          sx={{mr: 2}}
        />
        <ListItemIcon sx={{mr: 2}}>
          <FolderOutlined style={{fontSize: 22, color: '#1976d2'}} />
        </ListItemIcon>
        <ListItemText primary={folder.name} />
        <IconButton onClick={(event) => handleMenuClick(event, folder)}>
          <MoreOutlined />
        </IconButton>
      </ListItem>

      <ActionMenu
        anchorEl={anchorEl}
        document={selectedFolder}
        onClose={handleMenuClose}
      />
    </div>
  );
};

export default FolderList;
