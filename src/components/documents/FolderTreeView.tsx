import { Dispatch, ReactNode, SetStateAction, useEffect, useState } from "react";
import { SimpleTreeView, TreeItem } from "@mui/x-tree-view";

import { Folder, DocumentType, Document } from "@shared/types/Storage";
import { useAppSelector } from "@hooks/redux";
import { selectTreeViewDialogItem } from "@store/reducers/genericDialogSlice";

interface IFolderTreeView {
  folders: Folder[];
  currentBucketId: string | null;
  setSelectedFolderId: Dispatch<SetStateAction<string | null>>
}

const FolderTreeView = ({folders, currentBucketId, setSelectedFolderId}: IFolderTreeView) => {
  const document = useAppSelector(selectTreeViewDialogItem) as Document;

  const [expandedItems, setExpandedItems] = useState<string[]>([]);

  const findParentFolder = (folderId: string | null | undefined): Folder | null => {
    return folders.find((folder) => folder.id === folderId) || null;
  };

  const getAllParentIds = (folderId: string | null): string[] => {
    const parentIds: string[] = [];
    let currentFolder = findParentFolder(folderId);

    while (currentFolder) {
      if (currentFolder.id) {
        parentIds.push(currentFolder.id);
        currentFolder = findParentFolder(currentFolder.parent_id);
      }
    }

    return parentIds.reverse();
  };

  const buildTree = (items: Folder[], parentId: string | null): ReactNode[] => {
    return items
      .filter(item => item.parent_id === parentId)
      .map(item => {
        let isDisabled = false;

        if (document?.type === DocumentType.Folder) {
          if ('parent_id' in document) {
            isDisabled = item.id === document?.id || item.id === document.parent_id;
          }
        } else if (document?.type === DocumentType.File) {
          if ('bucket_id' in document) {
            isDisabled = item.id === document.bucket_id;
          }
        }

        return (
          <TreeItem
            key={item.id}
            itemId={String(item.id)}
            label={item.name}
            onClick={() => setSelectedFolderId(item.id || null)}
            sx={{
              pointerEvents: isDisabled ? 'none' : 'auto',
              color: isDisabled ? '#9C9C9CFF' : '#2D2D2D',
            }}
          >
            {buildTree(items, item.id || null)}
          </TreeItem>
        )
      });
  };

  useEffect(() => {
    const initialExpandedItems: string[] = [];

    if (currentBucketId) {
      const parentIds = getAllParentIds(currentBucketId);
      initialExpandedItems.push(...parentIds);
    }

    initialExpandedItems.push('Home');
    setExpandedItems(initialExpandedItems);

  }, [currentBucketId]);

  return (
    <SimpleTreeView expansionTrigger="iconContainer"
                    expandedItems={expandedItems}
                    onItemExpansionToggle={(_event, itemId) => {
                      setExpandedItems((prev) =>
                        prev.includes(itemId)
                          ? prev.filter(id => id !== itemId)
                          : [...prev, itemId]
                      );
                    }}
    >
      <TreeItem
        key={'Home'}
        itemId={String('Home')}
        label={'Home'}
        onClick={() => setSelectedFolderId('Home')}
        sx={{
          pointerEvents: (null === (document?.type === DocumentType.Folder ? document?.parent_id : document?.bucket_id)) ? 'none' : 'auto',
          color: (null === (document?.type === DocumentType.Folder ? document?.parent_id : document?.bucket_id)) ? '#9C9C9CFF' : '#2D2D2D',
        }}
      >
        {buildTree(folders, null)}
      </TreeItem>

    </SimpleTreeView>
  );
};

export default FolderTreeView;