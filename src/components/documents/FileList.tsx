import { SyntheticEvent, useState } from "react";
import { useDrag } from "react-dnd";
import { Checkbox, IconButton, ListItem, ListItemIcon, ListItemText } from "@mui/material";
import { MoreOutlined } from "@ant-design/icons";

import { DocumentType, File } from "@shared/types/Storage";
import { getIconByMimeType } from "@utils/Documents";
import ActionMenu from "@components/documents/ActionMenu";

interface IFileList {
  file: File;
  isSelected: boolean;
  onSelect: (file: File) => void;
  onDoubleClick: () => void;
}

const FileList = ({file, isSelected, onSelect, onDoubleClick}: IFileList) => {
  const [anchorEl, setAnchorEl] = useState<null | Element>(null);
  const [selectedFile, setSelectedFile] = useState<File | null>(null);

  const handleMenuClick = (event: SyntheticEvent, file: File) => {
    setAnchorEl(event.currentTarget);
    setSelectedFile(file);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    setSelectedFile(null);
  };

  const [{isDragging}, drag] = useDrag({
    type: DocumentType.File,
    item: {id: file.id, type: DocumentType.File},
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  const fileStyle = {
    border: '1px solid #f0f0f0',
    backgroundColor: 'white',
    transition: 'background-color 0.2s, border 0.2s',
    opacity: isDragging ? 0.5 : 1
  };

  return (
    <div ref={drag} style={fileStyle} onDoubleClick={onDoubleClick}>
      <ListItem key={file.id}
                button
                disableRipple
      >
        <Checkbox
          checked={isSelected}
          onChange={() => onSelect(file)}
          sx={{mr: 2}}
        />
        <ListItemIcon sx={{mr: 2}}>
          {getIconByMimeType(file.mime_type, 22)}
        </ListItemIcon>
        <ListItemText primary={file.name} />
        <IconButton onClick={(event) => handleMenuClick(event, file)}>
          <MoreOutlined />
        </IconButton>
      </ListItem>

      <ActionMenu
        anchorEl={anchorEl}
        document={selectedFile}
        onClose={handleMenuClose}
      />
    </div>
  );
};

export default FileList;
