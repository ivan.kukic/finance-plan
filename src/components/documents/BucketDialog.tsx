import { useEffect, useState } from "react";
import { useUserId } from "@nhost/react";
import { useMutation } from "@apollo/client";
import { Formik, FormikHelpers } from "formik";
import * as Yup from "yup";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  FormHelperText,
  Grid,
  InputLabel,
  OutlinedInput,
  Stack
} from "@mui/material";

import { openNotification } from "@store/reducers/notification";
import { NotificationType } from "@shared/enum/NotificationType";
import { StorageLocationType } from "@shared/enum/StorageFileType";
import {
  ADD_STORAGE_BUCKET,
  UPDATE_STORAGE_BUCKET_BY_PK,
  UPDATE_STORAGE_FILE_BY_PK,
  addStorageBucketCache,
  updateStorageBucketCache,
  updateStorageFileCache
} from "@services/StorageService";
import { FormAction } from "@shared/enum/FormAction";
import { DocumentType } from "@shared/types/Storage";
import { closeBucketDialog, selectBucketDialogState } from "@store/reducers/genericDialogSlice";
import { useAppDispatch, useAppSelector } from "@hooks/redux";

interface IBucketValues {
  name: string;
  submit: boolean | null
}

const BucketDialog = ({currentBucketId}: { currentBucketId: string | null }) => {
  const userId = useUserId();
  const dispatch = useAppDispatch();

  const {isOpen, item, formAction} = useAppSelector(selectBucketDialogState);

  const [initialValues, setInitialValues] = useState<IBucketValues>({
    name: '',
    submit: null
  });

  const [addStorageBucket] = useMutation(ADD_STORAGE_BUCKET, {update: addStorageBucketCache});
  const [updateStorageFileByPk] = useMutation(UPDATE_STORAGE_FILE_BY_PK, {update: updateStorageFileCache});
  const [updateStorageBucketByPk] = useMutation(UPDATE_STORAGE_BUCKET_BY_PK, {update: updateStorageBucketCache});

  const onSubmit = async (_values: IBucketValues, {
    setErrors,
    setStatus,
    setSubmitting
  }: FormikHelpers<IBucketValues>) => {
    try {
      if (formAction === FormAction.Create) {
        const {data} = await addStorageBucket({
          variables: {
            storage_bucket: {
              used_on: StorageLocationType.FilesAndBuckets,
              user_id: userId,
              name: _values.name.trim(),
              note: '',
              parent_id: currentBucketId
            }
          }
        });

        if (data) {
          dispatch(openNotification({
            type: NotificationType.Success,
            message: `Folder "${data.insert_storage_bucket_one.name}" has been successfully created.`
          }))
        }

      } else if (formAction === FormAction.Update) {
        if (!item) {
          return;
        }
        if (item.type === DocumentType.File) {
          await updateStorageFileByPk({
            variables: {
              id: item.id, storage_file: {
                id: item.id,
                name: _values.name.trim(),
              }
            }
          });

        } else if (item.type === DocumentType.Folder) {
          await updateStorageBucketByPk({
            variables: {
              id: item.id, storage_bucket: {
                id: item.id,
                name: _values.name.trim(),
              }
            }
          });
        }

        dispatch(openNotification({
          type: NotificationType.Success,
          message: `Item has been successfully renamed.`
        }))

      }
    } catch (err) {
      const error = err as Error;
      setStatus({success: false});
      setErrors({submit: error.message});
      setSubmitting(false);
      dispatch(openNotification({
        type: NotificationType.Error,
        message: error.message
      }))
    }

    dispatch(closeBucketDialog());
  }

  useEffect(() => {
    setInitialValues({
      name: item?.name || '',
      submit: null
    });
  }, [isOpen, item, formAction]);

  return (
    <Dialog open={isOpen}>
      <Formik
        initialValues={initialValues}
        validationSchema={Yup.object().shape({
          name: Yup.string().max(255).required('Name is required'),
        })}
        enableReinitialize={true}
        onSubmit={onSubmit}
      >
        {({errors, handleChange, handleSubmit, isSubmitting, touched, values}) => (
          <form noValidate onSubmit={handleSubmit}>
            <DialogTitle sx={{
              fontWeight: 400,
              fontSize: '1.075rem',
              lineHeight: 1.57
            }}>{formAction === FormAction.Create ? 'Create a New Folder' : 'Rename Item'}</DialogTitle>
            <DialogContent>
              <DialogContentText sx={{
                mb: 2,
                fontWeight: 400,
                fontSize: '0.875rem',
                lineHeight: 1.57
              }}>
                {formAction === FormAction.Create ? 'Enter the name of the new folder you want to create' : 'Enter the name of the new item you want to rename'}
              </DialogContentText>

              <Grid container spacing={3}>
                <Grid item xs={12} md={12}>
                  <Stack spacing={1}>
                    <InputLabel htmlFor="name">Name</InputLabel>
                    <OutlinedInput
                      id="name"
                      type="name"
                      value={values.name}
                      name="name"
                      onChange={handleChange}
                      placeholder="Name"
                      fullWidth
                      error={Boolean(touched.name && errors.name)}
                      autoFocus
                    />
                    {touched.name && errors.name && (
                      <FormHelperText error id="helper-text-name">
                        {errors.name}
                      </FormHelperText>
                    )}
                  </Stack>
                </Grid>
              </Grid>
            </DialogContent>

            <DialogActions>
              <Button disableElevation disabled={isSubmitting} color="primary"
                      onClick={() => dispatch(closeBucketDialog())}>
                Cancel
              </Button>
              <Button disableElevation disabled={isSubmitting} type="submit" variant="contained"
                      color="primary" sx={{ml: 2}}>
                Save
              </Button>
            </DialogActions>
          </form>
        )}
      </Formik>
    </Dialog>
  );
};

export default BucketDialog;