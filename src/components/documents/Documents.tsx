import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useMutation, useQuery } from "@apollo/client";
import { useNhostClient, useUserId } from "@nhost/react";
import { HTML5Backend } from "react-dnd-html5-backend";
import { DndProvider } from "react-dnd";
import { Box, Breadcrumbs, Button, Grid, Link, List, Typography } from '@mui/material';
import { ArrowLeftOutlined } from "@ant-design/icons";

import Notification from "@components/utility/Notification";
import EmptyImage from "@components/utility/EmptyImage";
import {
  Breadcrumb,
  Document,
  DocumentType,
  File,
  Folder,
  StorageBucket,
  StorageFile
} from "@shared/types/Storage";
import { StorageLocationType } from "@shared/enum/StorageFileType";
import { openNotification } from "@store/reducers/notification";
import { NotificationType } from "@shared/enum/NotificationType";
import {
  GET_STORAGE_BUCKETS,
  GET_STORAGE_FILES,
  UPDATE_STORAGE_BUCKET_BY_PK,
  UPDATE_STORAGE_FILE_BY_PK,
  updateStorageBucketCache,
  updateStorageFileCache
} from "@services/StorageService";
import FolderCard from "@components/documents/FolderCard";
import FileCard from "@components/documents/FileCard";
import Toolbar from "@components/documents/Toolbar";
import BucketDialog from "@components/documents/BucketDialog";
import UploadFilesBuckets from "@components/upload/UploadFilesBuckets";
import FolderList from "@components/documents/FolderList";
import FileList from "@components/documents/FileList";
import useDeleteFile from "@hooks/useDeleteFile";
import useDeleteBucketsRecursively from "@hooks/useDeleteBucketRecursively";
import { setData } from "@store/reducers/bucketFileSlice";
import TreeViewDialog from "@components/documents/TreeViewDialog";
import Loader from "@components/utility/Loader";
import { useAppDispatch } from "@hooks/redux";

const Documents = () => {
  const userId = useUserId();
  const nhost = useNhostClient();
  const dispatch = useAppDispatch();

  const navigate = useNavigate();
  const {id} = useParams();

  const {deleteFile} = useDeleteFile();
  const {deleteBucketRecursively} = useDeleteBucketsRecursively();

  const [updateStorageFileByPk] = useMutation(UPDATE_STORAGE_FILE_BY_PK, {update: updateStorageFileCache});
  const [updateStorageBucketByPk] = useMutation(UPDATE_STORAGE_BUCKET_BY_PK, {update: updateStorageBucketCache});

  const [currentBucketId, setCurrentBucketId] = useState<string | null>('undefined');
  const [breadcrumbPath, setBreadcrumbPath] = useState<Breadcrumb[]>([]);
  const [selectedItems, setSelectedItems] = useState<Document[]>([]);

  const [view, setView] = useState<'card' | 'list'>('list');

  const handleSelectedItem = (item: Document) => {
    setSelectedItems((prevSelected: Document[]) =>
      prevSelected.some(selectedItem => selectedItem.id === item.id && selectedItem.type === item.type)
        ? prevSelected.filter((selectedItem) => selectedItem.id !== item.id || selectedItem.type !== item.type)
        : [...prevSelected, item]
    );
  }

  const handleSelectAllItems = () => {
    if (selectedItems?.length === dataSubfolders?.length + dataFiles?.length) {
      setSelectedItems([]);
    } else {
      setSelectedItems([...dataSubfolders, ...dataFiles]);
    }
  };

  const handleDeleteSelectedItems = async () => {
    const selectedFolders = selectedItems.filter((item: Document) => item.type === DocumentType.Folder);
    const selectedFiles = selectedItems.filter((item: Document) => item.type === DocumentType.File);

    const dataFolders = folders?.storage_bucket?.map(convertToFolder);
    const dataFiles = files?.storage_file?.map(convertToFile);

    for (const folder of selectedFolders) {
      await deleteBucketRecursively(folder as Folder, dataFolders, dataFiles);
    }

    const promises = selectedFiles?.map(async (file: Document) => await deleteFile(file as File));
    await Promise.all(promises);

    dispatch(openNotification({
      type: NotificationType.Success,
      message: `All items have been successfully deleted.`
    }));

    setSelectedItems([]);
  }

  const handleFolderDoubleClick = (folder_id: string | null) => {
    navigate(`/documents/bucket/${folder_id}`);
  };

  const handleFileDoubleClick = async (file: File) => {
    const publicUrl: string | undefined = nhost.storage.getPublicUrl({fileId: file.files_id as string});
    window.open(publicUrl, '_blank', 'noreferrer');
  };

  const handleBreadcrumbClick = (folder_id: string | null) => {
    if (folder_id) {
      navigate(`/documents/bucket/${folder_id}`);
    } else {
      navigate(`/documents`);
    }
  };

  const handleDrop = async (document: Folder | File, targetFolderId: string | undefined) => {
    if (document.type === DocumentType.File) {
      await updateStorageFileByPk({
        variables: {
          id: document.id, storage_file: {
            id: document.id,
            bucket_id: targetFolderId
          }
        }
      });

    } else if (document.type === DocumentType.Folder) {
      await updateStorageBucketByPk({
        variables: {
          id: document.id, storage_bucket: {
            id: document.id,
            parent_id: targetFolderId
          }
        }
      });
    }
  };

  const getParentFolder = (folder_id: string | null) => {
    const folder = folders?.storage_bucket.find((f: { id: string | null; }) => f.id === folder_id);
    return folder || null;
  };

  const getAllParentNodes = (folderId: string | null): { id: string | null, name: string }[] => {
    const parentNodes: { id: string | null, name: string }[] = [];
    let currentFolder = getParentFolder(folderId);

    while (currentFolder) {
      if (currentFolder.id) {
        parentNodes.push({id: currentFolder.id, name: currentFolder.name});
        currentFolder = getParentFolder(currentFolder.parent_id);
      }
    }

    return parentNodes.reverse();
  };

  const handleBackClick = () => {
    const folder = getParentFolder(currentBucketId);
    if (folder.parent_id) {
      navigate(`/documents/bucket/${folder.parent_id}`);
    } else {
      navigate(`/documents`);
    }
  };

  const {loading: loading_files, data: files} = useQuery(GET_STORAGE_FILES, {
    variables: {used_on: StorageLocationType.FilesAndBuckets, user_id: userId},
    errorPolicy: 'all'
  });

  const {loading: loading_buckets, data: folders} = useQuery(GET_STORAGE_BUCKETS, {
    variables: {used_on: StorageLocationType.FilesAndBuckets, user_id: userId},
    errorPolicy: 'all'
  });

  const convertToFolder = (bucket: StorageBucket): Folder => ({...bucket, type: DocumentType.Folder});
  const convertToFile = (file: StorageFile): File => ({...file, type: DocumentType.File});

  useEffect(() => {
    if (loading_files || loading_buckets) {
      return;
    }
    setSelectedItems([]);

    dispatch(setData({
      folders: folders?.storage_bucket?.map(convertToFolder),
      files: files?.storage_file?.map(convertToFile)
    }));

    setCurrentBucketId(id || null);

    const home = {id: null, name: 'My Documents'};
    setBreadcrumbPath(id === undefined ? [home] : [home, ...getAllParentNodes(id || null)]);

  }, [loading_files, loading_buckets, files, folders, id]);

  if (loading_files || loading_buckets || currentBucketId === 'undefined') return <Loader />;

  const getSubFolders = (parent_id: string | null): Folder[] => folders?.storage_bucket?.map(convertToFolder).filter((folder: Folder) => folder.parent_id === parent_id);
  const getFilesInFolder = (folder_id: string | null): File[] => files?.storage_file?.map(convertToFile).filter((file: File) => file.bucket_id === folder_id);

  const dataSubfolders = getSubFolders(currentBucketId);
  const dataFiles = getFilesInFolder(currentBucketId);

  return (
    <Grid container rowSpacing={4.5} columnSpacing={2.75}>
      <Grid item xs={12} sx={{display: 'flex', flexDirection: 'column'}}>

        <Box sx={{display: 'flex', alignItems: 'center', mb: 2}}>
          <Breadcrumbs sx={{flexGrow: 1, mb: 0, pb: .3}}>
            {breadcrumbPath?.map((crumb, index) => (
              <Link
                key={crumb.id}
                underline={index === breadcrumbPath?.length - 1 ? 'none' : 'hover'}
                color={index === breadcrumbPath?.length - 1 ? 'text.primary' : 'inherit'}
                onClick={() => index !== breadcrumbPath?.length - 1 && handleBreadcrumbClick(crumb.id || null)}
                sx={{cursor: index === breadcrumbPath?.length - 1 ? 'default' : 'pointer'}}
              >
                <Typography variant="h5">{crumb.name}</Typography>
              </Link>
            ))}
          </Breadcrumbs>
          {currentBucketId && (
            <Button onClick={handleBackClick} variant="outlined" size="small">
              <ArrowLeftOutlined style={{marginRight: 9}} />
              Back
            </Button>
          )}
        </Box>
        <Grid item xs={12} sx={{display: 'flex', flexDirection: 'column'}}>
          <Box sx={{
            display: 'flex',
            flexDirection: 'column',
            minHeight: 150,
            mb: 2.75,
            mt: 0,
            height: 'auto',
            width: {xs: '100%', alignSelf: 'center'}
          }}>
            <UploadFilesBuckets folderId={currentBucketId} />
          </Box>
          <Toolbar
            folders={dataSubfolders}
            files={dataFiles}
            selectedItems={selectedItems}
            handleSelectAllItems={handleSelectAllItems}
            handleDeleteSelectedItems={handleDeleteSelectedItems}
            view={view}
            setView={setView}
          />
          <DndProvider backend={HTML5Backend}>
            <Grid container spacing={2} sx={{mt: 0}}>
              {view === 'list' ?
                <Grid item xs={12} md={12} sx={{backgroundColor: 'transparent'}}>
                  <List dense={false} sx={{py: 0}}>
                    {dataSubfolders?.map((folder: Folder) => (
                      <FolderList folder={folder}
                                  isSelected={selectedItems.some(selectedItem => selectedItem.id === folder.id && selectedItem.type === DocumentType.Folder)}
                                  onSelect={handleSelectedItem}
                                  onDoubleClick={() => handleFolderDoubleClick(folder.id || null)}
                                  onDrop={handleDrop}
                                  key={folder.id}
                      />
                    ))}
                    {dataFiles?.map((file: File) => (
                      <FileList file={file}
                                isSelected={selectedItems.some(selectedItem => selectedItem.id === file.id && selectedItem.type === DocumentType.File)}
                                onSelect={handleSelectedItem}
                                onDoubleClick={() => handleFileDoubleClick(file)}
                                key={file.id}
                      ></FileList>
                    ))}
                  </List>
                </Grid>
                :
                <>
                  {dataSubfolders?.map((folder: Folder) => (
                    <Grid item xs={6} sm={4} md={3} key={folder.id}>
                      <FolderCard folder={folder}
                                  isSelected={selectedItems.some(selectedItem => selectedItem.id === folder.id && selectedItem.type === DocumentType.Folder)}
                                  onSelect={handleSelectedItem}
                                  onDoubleClick={() => handleFolderDoubleClick(folder.id || null)}
                                  onDrop={handleDrop}
                      />
                    </Grid>
                  ))}
                  {dataFiles?.map((file: File) => (
                    <Grid item xs={6} sm={4} md={3} key={file.id}>
                      <FileCard file={file}
                                isSelected={selectedItems.some(selectedItem => selectedItem.id === file.id && selectedItem.type === DocumentType.File)}
                                onSelect={handleSelectedItem}
                                onDoubleClick={() => handleFileDoubleClick(file)}
                      />
                    </Grid>
                  ))}
                </>
              }
              {dataSubfolders?.length === 0 && dataFiles?.length === 0 && <EmptyImage />}
            </Grid>
          </DndProvider>
        </Grid>
      </Grid>
      <Notification />
      <BucketDialog currentBucketId={currentBucketId} />
      <TreeViewDialog currentBucketId={currentBucketId} />
    </Grid>
  )
}

export default Documents;