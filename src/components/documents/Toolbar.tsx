import { SyntheticEvent } from "react";
import { Box, Button, Checkbox, Chip, ToggleButton, ToggleButtonGroup, Tooltip } from "@mui/material";
import { MenuOutlined, PlusCircleOutlined, TableOutlined } from "@ant-design/icons";

import { File, Folder, Document } from "@shared/types/Storage";
import { FormAction } from "@shared/enum/FormAction";
import { openBucketDialog } from "@store/reducers/genericDialogSlice";
import { useAppDispatch } from "@hooks/redux";

interface IToolbar {
  folders: Folder[];
  files: File[];
  selectedItems: Document[];
  handleSelectAllItems: () => void;
  handleDeleteSelectedItems: () => void;
  view: string;
  setView: (newView: "card" | "list") => void;
}

const Toolbar = ({
                   folders,
                   files,
                   selectedItems,
                   handleSelectAllItems,
                   handleDeleteSelectedItems,
                   view,
                   setView
                 }: IToolbar) => {
  const dispatch = useAppDispatch();

  const handleUploadTrigger = () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-expect-error
    document.getElementById('upload-handler').click();
  }

  const totalItems = folders?.length + files?.length;

  const handleViewChange = (_event: SyntheticEvent, newView: 'card' | 'list' | null) => {
    if (newView !== null) {
      setView(newView);
    }
  };

  return (
    <Box sx={{display: 'flex', alignItems: 'center', width: '100%', minHeight: 42}}>
      {totalItems > 0 &&
          <Tooltip title="Toggle all items">
              <Checkbox onClick={handleSelectAllItems}
                        indeterminate={selectedItems?.length > 0 && selectedItems?.length < totalItems}
                        checked={selectedItems?.length === totalItems}
                        sx={{ml: view === 'list' ? 2 : 0}}
              />
          </Tooltip>
      }

      <Box sx={{display: 'flex', alignItems: 'center', flexGrow: 1}}>
        {selectedItems?.length > 0 &&
            <Chip color="secondary" size="small" label={`${selectedItems?.length} selected`}
                  sx={{ml: .6}} />}
        {selectedItems?.length > 0 &&
            <Button variant="text" color="error" sx={{ml: 1.6, fontWeight: '500', fontSize: 14}}
                    onClick={handleDeleteSelectedItems}>
                Delete
            </Button>
        }
      </Box>

      <Box sx={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
        <Button variant="text" sx={{ml: 0, fontWeight: '500', fontSize: 14}}
                onClick={() => dispatch(openBucketDialog({
                  item: null,
                  formAction: FormAction.Create
                }))}
                startIcon={<PlusCircleOutlined style={{fontSize: 13}} />}>
          Add folder
        </Button>
        <Button variant="contained" sx={{ml: 2, fontWeight: '500', fontSize: 14}} onClick={handleUploadTrigger}>
          Upload
        </Button>

        <ToggleButtonGroup
          value={view}
          exclusive
          onChange={handleViewChange}
          sx={{ml: 2}}
        >
          <ToggleButton value="list" aria-label="card view">
            <MenuOutlined />
          </ToggleButton>
          <ToggleButton value="card" aria-label="list view">
            <TableOutlined />
          </ToggleButton>
        </ToggleButtonGroup>
      </Box>
    </Box>
  );
};

export default Toolbar;