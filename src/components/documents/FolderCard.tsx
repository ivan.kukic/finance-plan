import { SyntheticEvent, useState } from "react";
import { useDrag, useDrop } from "react-dnd";
import { Card, CardActionArea, CardContent, Checkbox, IconButton, Typography } from "@mui/material";
import { FolderOutlined, MoreOutlined } from "@ant-design/icons";

import { DocumentType, Folder, File } from "@shared/types/Storage";
import ActionMenu from "@components/documents/ActionMenu";

interface IFolderCard {
  folder: Folder;
  isSelected: boolean;
  onSelect: (item: Folder) => void;
  onDoubleClick: () => void;
  onDrop: (item: File | Folder, targetFolderId: string | undefined) => void;
}

const FolderCard = ({folder, isSelected, onSelect, onDoubleClick, onDrop}: IFolderCard) => {
  const [anchorEl, setAnchorEl] = useState<null | Element>(null);
  const [selectedFolder, setSelectedFolder] = useState<Folder | null>(null);

  const handleMenuClick = (event: SyntheticEvent, folder: Folder) => {
    setAnchorEl(event.currentTarget);
    setSelectedFolder(folder);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    setSelectedFolder(null);
  };

  const [{isOver, canDrop}, drop] = useDrop({
    accept: [DocumentType.File, DocumentType.Folder],
    drop: (item: Folder | File) => {
      if (item.id !== folder.id) {
        onDrop(item, folder.id);
      }
    },
    canDrop: (item: Folder | File) => item.id !== folder.id, // Prevent dropping onto itself
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  });

  const [, drag] = useDrag({
    type: DocumentType.Folder,
    item: {id: folder.id, type: DocumentType.Folder},
  });

  const isActive = canDrop && isOver;
  const folderStyle = {
    border: isActive ? '1px solid green' : '1px solid #f0f0f0',
    backgroundColor: isActive ? '#e0f7fa' : 'white',
    transition: 'background-color 0.2s, border 0.2s',
    position: 'relative'
  };

  return (
    <div onDoubleClick={onDoubleClick} ref={(node) => drag(drop(node))}>
      <Card variant="outlined" sx={folderStyle}>
        <CardActionArea>
          <CardContent style={{textAlign: 'center'}}>
            <FolderOutlined style={{fontSize: 52, color: isActive ? 'green' : '#1976d2'}} />
            <Typography variant="h6" noWrap sx={{pt: 1.6}}>
              {folder.name}
            </Typography>
          </CardContent>
        </CardActionArea>
        <Checkbox
          checked={isSelected}
          onChange={() => onSelect(folder)}
          style={{position: 'absolute', top: 0, left: 0}}
        />
        <IconButton style={{position: 'absolute', top: 0, right: 0}}
                    onClick={(event) => handleMenuClick(event, folder)}>
          <MoreOutlined />
        </IconButton>
      </Card>

      <ActionMenu
        anchorEl={anchorEl}
        document={selectedFolder}
        onClose={handleMenuClose}
      />

    </div>
  );
};

export default FolderCard;
