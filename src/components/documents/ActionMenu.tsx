import { ListItemIcon, Menu, MenuItem } from "@mui/material";
import { ArrowRightOutlined, DeleteOutlined, EditOutlined } from "@ant-design/icons";

import { DocumentType, Document, File, Folder } from "@shared/types/Storage";
import { selectBucketFile } from "@store/reducers/bucketFileSlice";
import useDeleteBucketsRecursively from "@hooks/useDeleteBucketRecursively";
import useDeleteFile from "@hooks/useDeleteFile";
import { FormAction } from "@shared/enum/FormAction";
import { useAppDispatch, useAppSelector } from "@hooks/redux";
import { openBucketDialog, openTreeViewDialog } from "@store/reducers/genericDialogSlice";

interface IActionMenu {
  anchorEl: Element | null;
  document: Folder | File | null;
  onClose: () => void;
}

const ActionMenu = ({anchorEl, document, onClose}: IActionMenu) => {
  const dispatch = useAppDispatch();
  const {folders, files} = useAppSelector(selectBucketFile);

  const {deleteFile} = useDeleteFile();
  const {deleteBucketRecursively} = useDeleteBucketsRecursively();

  const handleRename = () => {
    if (!document) {
      return;
    }

    dispatch(openBucketDialog({item: document as Document, formAction: FormAction.Update}));
    onClose();
  };

  const handleMove = () => {
    if (!document) {
      return;
    }

    dispatch(openTreeViewDialog({item: document as Document}));
    onClose();
  };

  const handleDelete = async () => {
    if (!document || !folders || !files) {
      return;
    }
    onClose();

    if (document.type === DocumentType.File) {
      await deleteFile(document as File);
    } else if (document.type === DocumentType.Folder) {
      await deleteBucketRecursively(document as Folder, folders, files);
    }
  };

  return (
    <Menu
      anchorEl={anchorEl}
      open={Boolean(anchorEl)}
      onClose={onClose}
    >
      <MenuItem onClick={handleRename}>
        <ListItemIcon style={{minWidth: '26px'}}>
          <EditOutlined />
        </ListItemIcon>
        Rename
      </MenuItem>
      <MenuItem onClick={handleMove}>
        <ListItemIcon style={{minWidth: '26px'}}>
          <ArrowRightOutlined />
        </ListItemIcon>
        Move
      </MenuItem>
      <MenuItem onClick={handleDelete}>
        <ListItemIcon style={{minWidth: '26px'}}>
          <DeleteOutlined />
        </ListItemIcon>
        Delete
      </MenuItem>
    </Menu>
  );
};

export default ActionMenu;
