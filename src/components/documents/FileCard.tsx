import { SyntheticEvent, useState } from "react";
import { Card, CardActionArea, CardContent, Checkbox, IconButton, Typography } from "@mui/material";
import { useDrag } from "react-dnd";
import { MoreOutlined } from "@ant-design/icons";

import { getIconByMimeType } from "@utils/Documents";
import { File, DocumentType } from "@shared/types/Storage";
import ActionMenu from "@components/documents/ActionMenu";

interface IFileCard {
  file: File;
  isSelected: boolean;
  onSelect: (file: File) => void;
  onDoubleClick: () => void;
}

const FileCard = ({file, isSelected, onSelect, onDoubleClick}: IFileCard) => {
  const [anchorEl, setAnchorEl] = useState<null | Element>(null);
  const [selectedFile, setSelectedFile] = useState<File | null>(null);

  const handleMenuClick = (event: SyntheticEvent, file: File) => {
    setAnchorEl(event.currentTarget);
    setSelectedFile(file);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    setSelectedFile(null);
  };

  const [{isDragging}, drag] = useDrag({
    type: DocumentType.File,
    item: {id: file.id, type: DocumentType.File},
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });
  return (
    <div ref={drag} style={{opacity: isDragging ? 0.5 : 1}} onDoubleClick={onDoubleClick}>
      <Card variant="outlined" sx={{position: "relative"}}>
        <CardActionArea>
          <CardContent style={{textAlign: 'center'}}>
            {getIconByMimeType(file.mime_type)}
            <Typography variant="h6" noWrap sx={{pt: 1.6}}>
              {file.name}
            </Typography>
          </CardContent>
        </CardActionArea>
        <Checkbox
          checked={isSelected}
          onChange={() => onSelect(file)}
          style={{position: 'absolute', top: 0, left: 0}}
        />
        <IconButton style={{position: 'absolute', top: 0, right: 0}}
                    onClick={(event) => handleMenuClick(event, file)}>
          <MoreOutlined />
        </IconButton>
      </Card>

      <ActionMenu
        anchorEl={anchorEl}
        document={selectedFile}
        onClose={handleMenuClose}
      />

    </div>
  );
}


export default FileCard;
