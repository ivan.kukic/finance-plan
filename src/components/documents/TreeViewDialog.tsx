import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from "@mui/material";
import { useState } from "react";
import { useMutation } from "@apollo/client";

import FolderTreeView from "@components/documents/FolderTreeView";
import { selectBucketFile } from "@store/reducers/bucketFileSlice";
import { DocumentType } from "@shared/types/Storage";
import { openNotification } from "@store/reducers/notification";
import { NotificationType } from "@shared/enum/NotificationType";
import {
  UPDATE_STORAGE_BUCKET_BY_PK,
  UPDATE_STORAGE_FILE_BY_PK, updateStorageBucketCache,
  updateStorageFileCache
} from "@services/StorageService";
import { useAppDispatch, useAppSelector } from "@hooks/redux";
import { selectTreeViewDialogState, closeTreeViewDialog } from "@store/reducers/genericDialogSlice";

interface ITreeViewDialog {
  currentBucketId: string | null,
}

const TreeViewDialog = ({currentBucketId}: ITreeViewDialog) => {
  const dispatch = useAppDispatch();
  const {folders} = useAppSelector(selectBucketFile);

  const {isOpen, item} = useAppSelector(selectTreeViewDialogState);
  const [selectedFolderId, setSelectedFolderId] = useState<string | null>(null);

  const [updateStorageFileByPk] = useMutation(UPDATE_STORAGE_FILE_BY_PK, {update: updateStorageFileCache});
  const [updateStorageBucketByPk] = useMutation(UPDATE_STORAGE_BUCKET_BY_PK, {update: updateStorageBucketCache});

  const handleMove = async () => {
    if (selectedFolderId && item) {
      const targetFolderId = selectedFolderId === 'Home' ? null : selectedFolderId;

      if (item.type === DocumentType.File) {
        await updateStorageFileByPk({
          variables: {
            id: item.id, storage_file: {
              id: item.id,
              bucket_id: targetFolderId
            }
          }
        });

      } else if (item.type === DocumentType.Folder) {
        await updateStorageBucketByPk({
          variables: {
            id: item.id, storage_bucket: {
              id: item.id,
              parent_id: targetFolderId
            }
          }
        });
      }

      setSelectedFolderId(null);

      dispatch(openNotification({
        type: NotificationType.Success,
        message: `Item have been successfully moved.`
      }));

      dispatch(closeTreeViewDialog());

    }
  };

  const handleClose = () => {
    setSelectedFolderId(null);
    dispatch(closeTreeViewDialog());
  }

  return (
    <Dialog open={isOpen} maxWidth="xs" fullWidth sx={{'& .MuiDialog-paper': {minHeight: '400px'}}}>
      <DialogTitle sx={{
        fontWeight: 400,
        fontSize: '1.075rem',
        lineHeight: 1.57
      }}>Select Target Folder</DialogTitle>
      <DialogContent dividers>
        <FolderTreeView
          folders={folders || []}
          currentBucketId={currentBucketId}
          setSelectedFolderId={setSelectedFolderId}
        ></FolderTreeView>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary">
          Cancel
        </Button>
        <Button
          onClick={handleMove}
          color="primary"
          variant="contained"
          disabled={!selectedFolderId}
        >
          Move
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default TreeViewDialog;
