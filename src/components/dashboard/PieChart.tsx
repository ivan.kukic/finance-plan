import { memo, useMemo } from "react";
import { Box, useTheme } from "@mui/material";
import ReactApexChart from 'react-apexcharts';
import { ApexOptions } from "apexcharts";

import { formatNumberOrNull } from "@utils/Helpers";

interface IPieChart {
  series: number[];
  height?: number;
  isPdfChart?: boolean;
  chartId?: string;
}

const PieChart = ({
                    series,
                    height = 205,
                    isPdfChart = false,
                    chartId = 'PieChartPdf'
                  }: IPieChart) => {
  const theme = useTheme();

  const options: ApexOptions = useMemo(() => ({
    labels: ['Income', 'Expense'],
    chart: {
      id: isPdfChart ? chartId : 'PieChartId',
      type: 'pie',
      height: height,
      toolbar: {show: false},
      animations: {
        enabled: !isPdfChart
      },
    },
    dataLabels: {
      enabled: true,
      formatter: (val: number) => `${val.toFixed(1)}%`,
      style: {
        fontSize: '15px',
        fontWeight: 500,
        colors: ['white']
      },
      dropShadow: {
        enabled: false
      }
    },
    stroke: {
      curve: 'smooth',
      width: 2
    },
    grid: {
      strokeDashArray: 0
    },
    legend: {
      position: 'bottom'
    },
    colors: [theme.palette.primary.main, theme.palette.warning.main],
    plotOptions: {
      pie: {
        dataLabels: {
          offset: -24,
          minAngleToShowLabel: 10
        }
      }
    },
    tooltip: {
      y: {
        formatter(val: number) {
          return formatNumberOrNull(val);
        },
        title: {
          formatter: (seriesName: string) => `${seriesName}:`
        }
      },
    },


  }), [chartId, height, isPdfChart, theme.palette.primary.main, theme.palette.warning.main])

  const chartStyle = isPdfChart ? {position: 'absolute', left: '-9999px', top: '-9999px'} : {};

  return (
    <Box sx={chartStyle}>
      <ReactApexChart options={options} series={series} type="pie" height={height} />
    </Box>
  )
};

const MemorizedPieChart = memo(PieChart);
export default MemorizedPieChart;