import { memo, useMemo } from 'react';
import { ApexOptions } from "apexcharts";
import ReactApexChart from 'react-apexcharts';
import { Box } from "@mui/material";
import { useTheme } from '@mui/material/styles';

import { formatNumberOrNull, monthsShort } from "@utils/Helpers";
import { ReportType } from "@shared/enum/ReportType";

interface SeriesData {
  name: string;
  data: number[];
}

interface IRevenueColumnChart {
  incomeTotal: number[],
  expenseTotal: number[],
  height?: number;
  isPdfChart?: boolean;
  reportType?: ReportType;
  quarter?: number;
}

const RevenueColumnChart = ({
                              incomeTotal,
                              expenseTotal,
                              height = 420,
                              isPdfChart = false,
                              reportType = ReportType.Year,
                              quarter = 1
                            }: IRevenueColumnChart) => {
  const theme = useTheme();
  const {primary} = theme.palette.text;
  const line = theme.palette.divider;
  const incomeColor = theme.palette.primary.main;
  const expenseColor = theme.palette.warning.main;

  const series: SeriesData[] = useMemo<SeriesData[]>(() => [
    {name: 'Income', data: incomeTotal},
    {name: 'Expense', data: expenseTotal},
  ], [incomeTotal, expenseTotal]);

  const options: ApexOptions = useMemo(() => ({
    chart: {
      id: isPdfChart ? 'RevenueColumnChartPdf' : 'RevenueColumnChartId',
      type: 'bar',
      height: height,
      toolbar: {show: false},
      animations: {
        enabled: !isPdfChart
      },
    },
    plotOptions: {
      bar: {
        columnWidth: '60%',
        borderRadius: 1,
      }
    },
    dataLabels: {enabled: false},
    stroke: {
      show: true,
      width: 8,
      colors: ['transparent']
    },
    colors: [incomeColor, expenseColor],
    xaxis: {
      categories: reportType === ReportType.Year
        ? monthsShort
        : monthsShort.slice((quarter - 1) * 3, quarter * 3),
      labels: {
        style: {
          fontSize: '13px',
          colors: Array(reportType === ReportType.Year ? 12 : 3).fill(primary),
        }
      }
    },
    yaxis: {
      labels: {
        style: {
          fontSize: '15px',
          colors: [primary],
        },
        formatter: (val: number) => formatNumberOrNull(val)
      },
    },
    grid: {borderColor: line},
    fill: {opacity: 1},
    tooltip: {
      theme: 'light',
      y: {
        formatter: (val: number) => formatNumberOrNull(val),
      }
    },
    legend: {
      show: true,
      position: 'top',
      horizontalAlign: 'right',
      fontFamily: `'Public Sans', sans-serif`,
      offsetX: 10,
      offsetY: 10,
      labels: {
        useSeriesColors: false,
        colors: 'grey.500',
      },
      markers: {size: 12},
      itemMargin: {
        horizontal: 15,
        vertical: 50,
      }
    }
  }), [height, isPdfChart, primary, line, incomeColor, expenseColor, reportType, quarter]);

  const chartStyle = isPdfChart ? {position: 'absolute', left: '-9999px', top: '-9999px'} : {};

  return (
    <Box sx={chartStyle}>
      <ReactApexChart options={options} series={series} type="bar" height={height} />
    </Box>
  );
};

const MemorizedRevenueColumnChart = memo(RevenueColumnChart);
export default MemorizedRevenueColumnChart;