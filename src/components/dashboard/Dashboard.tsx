// TODO: Review and adjust your budget quarterly -  Add component to review finance quarterly
// TODO: Use ChatGPT to write README.md file
import { lazy, Suspense, useCallback, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { User, useUserData } from "@nhost/react";
import { Box, Button, Grid, MenuItem, Select, SelectChangeEvent, Stack, Typography, } from "@mui/material";
import { FileExcelOutlined, FilePdfOutlined } from "@ant-design/icons";

import MainCard from "@components/utility/MainCard";
import Loader from "@components/utility/Loader";
import Error from "@components/utility/Error";
import { Item } from "@shared/types/Item";
import AnalyticsOverview from "@components/finance/AnalyticsOverview";
import { formatNumberOrNull } from "@utils/Helpers";
import CsvExport from "@components/reports/CsvExport";
import useGetItems from "@hooks/useGetItems";
import { calculateTotalAmount, getQuarterlyTotalPerMonths, getTotal, getYearlyTotalPerMonths } from "@utils/ItemHelper";
import { getQuarterlyAnalytics, getYearlyAnalytics } from "@utils/AnaliticsHelper";
import AskAI from "@components/utility/AskAI";
import { AIType } from "@shared/enum/AIType";
import { useAppSelector } from "@hooks/redux";
import { selectedYear } from "@store/reducers/yearSlice";
import PdfDocument from "@components/reports/pdf/PdfDocument";
import { getDataUri, savePdf } from "@utils/ChartHelper";
import ChartLoadingPlaceholder from "@components/utility/ChartLoadingPlaceholder";
import ButtonLoadingPlaceholder from "@components/utility/ButtonLoadingPlaceholder";
import { ReportType } from "@shared/enum/ReportType";
import { getImageURIArray, getPieChartArray } from "@utils/ImgUriHelper";
import { ItemType } from "@shared/enum/itemType";
import { calculateQuarterFromMonth } from "@utils/MathHelper";

const RevenueColumnChart = lazy(() => import("@components/dashboard/RevenueColumnChart"));

const Dashboard = () => {
  const user: User | null = useUserData();
  const currentYear: number = useAppSelector(selectedYear);
  const currentMonth: number = Math.min(Math.max(parseInt(useParams().month as string, 10) || new Date().getMonth() + 1, 1), 12);

  const [year, setYear] = useState<number>(currentYear);
  const [month, setMonth] = useState<number>(currentMonth);
  const [quarter, setQuarter] = useState<number>(1);
  const [isDownloading, setIsDownloading] = useState<boolean>(false);
  const [showPdfChart, setShowPdfChart] = useState<boolean>(false);
  const [reportType, setReportType] = useState<ReportType>(ReportType.Year);

  const handleReportTypeChange = (event: SelectChangeEvent<ReportType>) => {
    setReportType(event.target.value as ReportType);
    if (event.target.value === ReportType.Quarter) {
      setQuarter(calculateQuarterFromMonth(month));
    }
  };

  const handleQuarterChange = (event: SelectChangeEvent<number>) => {
    setQuarter(event.target.value as number);
  };

  const {loading, error, data} = useGetItems({userId: user?.id, year});

  useEffect(() => {
    const fetchData = async () => {
      setMonth(currentMonth);
      setYear(currentYear);
      if (reportType === ReportType.Quarter) {
        setQuarter(calculateQuarterFromMonth(month));
      }
    };

    fetchData();
  }, [currentYear, currentMonth, year, month, reportType]);

  const handlePdfDownloadClick = useCallback(async () => {
    setIsDownloading(true);
    setShowPdfChart(true);

    try {
      // Delay to ensure charts are rendered before capturing
      await new Promise(resolve => setTimeout(resolve, 100));

      // Fetch main chart URI
      const imageURI = await getDataUri('RevenueColumnChartPdf');

      // Fetch URIs for pie charts
      const imageURIArray = await getImageURIArray({reportType, quarter});

      // Filter out invalid URIs
      const validImageURIs = imageURIArray.filter(
        (uri): uri is string => uri !== '' && uri !== null
      );

      const pdfDocument = (
        <PdfDocument
          items={data}
          month={month}
          year={year}
          quarter={quarter}
          dataUrl={imageURI}
          dataUrls={validImageURIs}
          reportType={reportType}
        />
      );
      const fileName = `Financial-report-${reportType.toLowerCase()}-${year}${reportType === ReportType.Quarter ? `-Q${quarter}` : ''}.pdf`;
      await savePdf(pdfDocument, fileName);
    } catch (error) {
      console.error("Error during PDF generation:", error);
    } finally {
      setIsDownloading(false);
      setShowPdfChart(false);
    }
  }, [data, year, quarter, month, reportType]);

  if (loading) return <Loader />;
  if (error) return <Error apiError={error} />;

  const items: Item[] = data.items;

  const yearlyItems: Item[] = items.filter((item: Item) => (item.year === year)) as Item[];
  const quarterlyItems = yearlyItems.filter(item => calculateQuarterFromMonth(Number(item.month)) === quarter);
  const [yearlyTotalAmount, quarterlyTotalAmount] = [getYearlyTotalPerMonths(yearlyItems), getQuarterlyTotalPerMonths(yearlyItems, quarter)];

  const yearProfit = calculateTotalAmount(data.yearlyIncomeItems) - calculateTotalAmount(data.yearlyExpenseItems);
  const quarterProfit = calculateTotalAmount(quarterlyItems.filter(item => item.type === ItemType.Income)) -
    calculateTotalAmount(quarterlyItems.filter(item => item.type === ItemType.Expense));

  const yearIncomePerMonth = yearlyTotalAmount.income;
  const yearExpensePerMonth = yearlyTotalAmount.expense;
  const quarterIncomePerMonth = quarterlyTotalAmount.income;
  const quarterExpensePerMonth = quarterlyTotalAmount.expense;

  const analytics = reportType === ReportType.Year
    ? getYearlyAnalytics({items: data, year})
    : getQuarterlyAnalytics({items: data, year, quarter});

  return (
    <Grid container rowSpacing={4.5} columnSpacing={2.75}>
      <Grid item xs={12} sx={{mb: -2.25}}>
        <Box sx={{display: 'flex', alignItems: 'center'}}>
          <Typography variant="h5" sx={{flexGrow: 1, mr: 3}}>Dashboard</Typography>
          <Select value={reportType}
                  onChange={handleReportTypeChange}
                  sx={{display: 'flex', alignItems: 'center', mr: 3, minWidth: 100}}
          >
            <MenuItem value={ReportType.Year}>{year} Yearly</MenuItem>
            <MenuItem value={ReportType.Quarter}>{year} Quarterly</MenuItem>
          </Select>
          {reportType === ReportType.Quarter && (
            <Select value={quarter}
                    onChange={handleQuarterChange}
                    sx={{display: 'flex', alignItems: 'center', mr: 3, minWidth: 60}}
            >
              <MenuItem value={1}>Q1</MenuItem>
              <MenuItem value={2}>Q2</MenuItem>
              <MenuItem value={3}>Q3</MenuItem>
              <MenuItem value={4}>Q4</MenuItem>
            </Select>
          )}
          <Button color="primary" sx={{py: 0, mr: 1.2, fontWeight: '500', fontSize: 14}}
                  onClick={handlePdfDownloadClick} disabled={isDownloading}>
            {isDownloading ? (
              <ButtonLoadingPlaceholder text="Saving PDF..." />
            ) : (
              <>
                <FilePdfOutlined style={{paddingRight: 9, color: '#ea4335'}} />
                Save PDF
              </>
            )}
          </Button>
          <CsvExport items={yearlyItems}>
            <Button color="primary" sx={{py: 0, fontWeight: '500', fontSize: 14}}>
              <FileExcelOutlined style={{paddingRight: 9, color: '#34a853'}} /> Save CSV
            </Button>
          </CsvExport>
        </Box>
      </Grid>

      {analytics && Object.entries(analytics).map(([key, value]) => (
        <Grid item xs={12} sm={6} md={4} key={key}>
          <AnalyticsOverview
            title={value.title}
            amount={value.amount}
            percentage={value.percentage}
            extra={value.extra}
            isLoss={value.isLoss}
          />
        </Grid>
      ))}

      <Grid item xs={12}>
        <MainCard>
          <Stack spacing={1.5} sx={{mb: -12}}>
            <Typography variant="h6" color="secondary">
              {reportType === ReportType.Year ? ReportType.Year : `Q${quarter}`} Profit
            </Typography>
            <Typography variant="h4">
              {formatNumberOrNull(reportType === ReportType.Year ? yearProfit : quarterProfit)}
            </Typography>
          </Stack>
          <Suspense fallback={<ChartLoadingPlaceholder height={420} />}>
            <RevenueColumnChart
              incomeTotal={getTotal(reportType, yearIncomePerMonth, quarterIncomePerMonth)}
              expenseTotal={getTotal(reportType, yearExpensePerMonth, quarterExpensePerMonth)}
              height={420}
              reportType={reportType}
              quarter={quarter}
            />
          </Suspense>
        </MainCard>
      </Grid>

      {showPdfChart && (
        <>
          <RevenueColumnChart
            incomeTotal={getTotal(reportType, yearIncomePerMonth, quarterIncomePerMonth)}
            expenseTotal={getTotal(reportType, yearExpensePerMonth, quarterExpensePerMonth)}

            height={420}
            isPdfChart={true}
            reportType={reportType}
            quarter={quarter}
          />

          {getPieChartArray({
            reportType,
            quarter,
            income: getTotal(reportType, yearIncomePerMonth, quarterIncomePerMonth),
            expense: getTotal(reportType, yearExpensePerMonth, quarterExpensePerMonth)
          })}
        </>
      )}

      {[AIType.ChatGPT, AIType.Claude, AIType.Perplexity].map((aiType) => (
        <Grid item xs={12} md={4} key={aiType}>
          <AskAI items={reportType === ReportType.Year ? yearlyItems : quarterlyItems} aiType={aiType} />
        </Grid>
      ))}
    </Grid>
  );
};

export default Dashboard;