import ReactApexChart from 'react-apexcharts';
import { useTheme } from "@mui/material";
import { formatNumberOrNull } from "@utils/Helpers";

// chart options
const barCharOptions = (colors: string[], fontColors: string[]): ApexCharts.ApexOptions => ({
  labels: ['Income', 'Expense'],
  chart: {
    height: 450,
    type: 'pie',
    toolbar: {
      show: false
    }
  },
  dataLabels: {
    enabled: true,
    formatter: (val: number) => `${val.toFixed(1)}%`,
    style: {
      fontSize: '15px',
      fontWeight: 500,
      colors: fontColors
    },
    dropShadow: {
      enabled: false
    }
  },
  stroke: {
    curve: 'smooth',
    width: 2
  },
  grid: {
    strokeDashArray: 0
  },
  legend: {
    position: 'bottom'
  },
  colors: colors,
  plotOptions: {
    pie: {
      dataLabels: {
        offset: -24,
        minAngleToShowLabel: 10
      }
    }
  },
  tooltip: {
    y: {
      formatter(val: number) {
        return formatNumberOrNull(val);
      },
      title: {
        formatter: (seriesName: string) => `${seriesName}:`
      }
    },
  },
});

interface IPieChart {
  series: number[];
  labels?: string[];
}

const PieChart = ({series}: IPieChart) => {
  const theme = useTheme();
  const chartColors = [theme.palette.primary.main, theme.palette.warning.main];
  const fontColors = ['white'];

  return <ReactApexChart options={barCharOptions(chartColors, fontColors)} series={series} type="bar" height={237} />;
};

export default PieChart;
