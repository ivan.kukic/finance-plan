import { useState, useEffect } from 'react';
import { useTheme, Theme } from '@mui/material/styles';
import ReactApexChart from 'react-apexcharts';

// chart options
const areaChartOptions: ApexCharts.ApexOptions = {
  chart: {
    height: 450,
    type: 'area',
    toolbar: {
      show: false
    }
  },
  dataLabels: {
    enabled: false
  },
  stroke: {
    curve: 'smooth',
    width: 2
  },
  grid: {
    strokeDashArray: 0
  }
};

interface IncomeAreaChartProps {
  slot: 'month' | 'week';  // Restrict to either 'month' or 'week'
  incomePerMonthTotal: number[],
  expensePerMonthTotal: number[],
}

const IncomeAreaChart = ({slot, incomePerMonthTotal, expensePerMonthTotal}: IncomeAreaChartProps) => {
  const theme: Theme = useTheme();

  const {primary, secondary} = theme.palette.text;
  const line = theme.palette.divider;

  const [options, setOptions] = useState<ApexCharts.ApexOptions>(areaChartOptions);

  useEffect(() => {
    setOptions((prevState) => ({
      ...prevState,
      colors: [theme.palette.primary.main, theme.palette.warning.main],
      xaxis: {
        categories:
          slot === 'month'
            ? ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            : ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
        labels: {
          style: {
            colors: Array(12).fill(secondary) // Dynamic array length depending on slot
          }
        },
        axisBorder: {
          show: true,
          color: line
        },
        tickAmount: slot === 'month' ? 11 : 7
      },
      yaxis: {
        labels: {
          style: {
            colors: [secondary]
          }
        }
      },
      grid: {
        borderColor: line
      },
      tooltip: {
        theme: 'light'
      }
    }));
  }, [primary, secondary, line, theme, slot]);

  const [series, setSeries] = useState<ApexAxisChartSeries>([
    {
      name: 'Income',
      data: [0, 86, 28, 115, 48, 210, 136]
    },
    {
      name: 'Expense',
      data: [0, 43, 14, 56, 24, 105, 68]
    }
  ]);

  useEffect(() => {
    setSeries([
      {
        name: 'Income',
        data: slot === 'month' ? incomePerMonthTotal : [31, 40, 28, 51, 42, 109, 100]
      },
      {
        name: 'Expense',
        data: slot === 'month' ? expensePerMonthTotal : [11, 32, 45, 32, 34, 52, 41]
      }
    ]);
  }, [slot]);

  return <ReactApexChart options={options} series={series} type="area" height={450} />;
};

export default IncomeAreaChart;
