import { Form, Formik, FormikErrors, FormikHelpers, FormikTouched } from "formik";
import { ObjectSchema } from "yup";
import {
  Button,
  FormHelperText,
  Grid,
  InputLabel,
  OutlinedInput,
  MenuItem,
  Select,
  Checkbox,
  FormControlLabel,
  Autocomplete,
  Box,
  TextField,
  Typography,
  Avatar,
  ListItemIcon,
  ListItemText,
  Stack,
} from '@mui/material';

import { FormAction } from "@shared/enum/FormAction";
import { IconComponent } from "@components/utility/IconComponent";
import { getCategoryIconByType } from "@utils/CrudHelper";
import { ItemType } from "@shared/enum/itemType";
import { IconColors } from "@shared/enum/IconColors";

export interface IFormData {
  [key: string]: string | boolean | number | unknown;
  submit?: boolean;
}

export interface IFormField {
  id: string;
  name: string;
  label: string;
  type: 'text' | 'textarea' | 'dropdown' | 'checkbox' | 'autocomplete' | 'component';
  required?: boolean;
  value?: string | boolean | number | null | undefined | { iconName: string; color: string; type: ItemType };
  options?: { key: string; value: string | number }[];
  helperText?: string;
  hidden?: boolean;
  children?: IFormField[];
}

interface IFormProps {
  onSubmit: (_values: IFormData, formikHelpers: FormikHelpers<IFormData>) => void | Promise<void>;
  onClose: () => void;
  formFields: IFormField[];
  formMode: FormAction.Create | FormAction.Update | FormAction.Delete;
  validationSchema: ObjectSchema<IFormData>;
  columnRender?: IColumnRender;
}

export interface IColumnRender {
  xs?: number;
  sm?: number;
  md?: number;
  lg?: number;
  xl?: number;
}

type OptionType = {
  key: string;
  value: string | number | null;
};

const isIconItem = (value: unknown): value is { iconName: string; color: string; type: ItemType } => {
  return typeof value === 'object' && value !== null && 'iconName' in value && 'color' in value;
};

const CrudForm = ({onSubmit, onClose, formFields, formMode, validationSchema, columnRender = {}}: IFormProps) => {
  const initializeValues = (fields: IFormField[]): IFormData => {
    return fields.reduce((acc, field) => {
      if (field.type !== 'component') {
        acc[field.name] = field.value ?? '';
        field.children?.forEach(child => {
          if (child.type !== 'component') {
            acc[child.name] = child.value ?? '';
          }
        });
      }
      return acc;
    }, {} as IFormData);
  };

  const initialValues = initializeValues(formFields);

  const cr = {
    xs: 12,
    sm: 12,
    md: 6,
    lg: 6,
    xl: 6,
    ...columnRender // Combine the default grid properties with columnRender
  };

  const renderField = (field: IFormField, values: IFormData, touched: FormikTouched<IFormData>, errors: FormikErrors<IFormData>, setFieldValue: FormikHelpers<IFormData>["setFieldValue"]) => {
    const selectedValue = field.options?.find(option => option.key === values[field.name])?.value || '';

    switch (field.type) {
      case 'text':
      case 'textarea':
        return (
          <OutlinedInput
            multiline={field.type === 'textarea'}
            fullWidth
            minRows={field.type === 'textarea' ? 5 : undefined}
            id={field.id}
            name={field.name}
            value={values[field.name]}
            onChange={(e) => setFieldValue(field.name, e.target.value)}
            label={field.label}
            required={field.required}
            placeholder={field.label}
            error={Boolean(touched[field.name] && errors[field.name])}
          />
        );
      case 'dropdown':
        return (
          <Select
            fullWidth
            id={field.id}
            name={field.name}
            value={selectedValue}
            onChange={(e) => setFieldValue(field.name, field.options?.find((option) => option.value === e.target.value)?.key)}
            displayEmpty
            renderValue={(value) => {
              const selectedOption = field.options?.find((option) => option.value === value);
              return value === ''
                ? (
                  <span style={{color: "rgba(0, 0, 0, .36)"}}>{field.label}</span>)
                : (
                  <Box display="flex" alignItems="center">
                    <ListItemIcon>
                      <Box sx={{backgroundColor: selectedOption?.value ?? null, width: 16, height: 16}} />
                    </ListItemIcon>
                    <ListItemText primary={selectedOption?.key} />
                  </Box>
                );
            }}
            sx={{
              padding: 0,
              '& .MuiSelect-select': {
                padding: '6px 10px',
                minHeight: 30,
                lineHeight: 2,
              }
            }}
          >
            <MenuItem value="" sx={{color: 'text.secondary'}}>
              {field.label}
            </MenuItem>
            {field.options?.map((option) => (
              <MenuItem key={option.key} value={option.value}>
                <ListItemIcon>
                  <Box sx={{backgroundColor: option.value ?? null, width: 16, height: 16}} />
                </ListItemIcon>
                <ListItemText primary={option.key} />
              </MenuItem>
            ))}
          </Select>
        );
      case 'autocomplete':
        return (
          <Autocomplete<OptionType>
            options={field.options ?? []}
            autoHighlight
            getOptionLabel={(option) => option.key}
            value={field.options?.find((option) => option.key === values[field.name]) ?? null}
            onChange={(_event, newValue) => setFieldValue(field.name, newValue?.key ?? null)}
            renderOption={(props, option) => {
              const {key, ...optionProps} = props;
              return (
                <Box
                  key={key}
                  component="li"
                  sx={{
                    '& > span': {mr: 2, flexShrink: 0, display: 'block', lineHeight: 2},
                    '& > span > svg': {height: 16, width: 16, mt: 1},
                  }}
                  {...optionProps}
                >
                  <IconComponent iconName={option.value} />
                  <Typography variant="body1">{option.key}</Typography>
                </Box>
              );
            }}
            renderInput={(params) => (
              <TextField
                {...params}
                placeholder={field.label}
                sx={{
                  '& > label': {mr: 2, flexShrink: 0, lineHeight: 1.6},
                }}
              />
            )}
            size="small"
          />
        );
      case 'checkbox':
        return (
          <FormControlLabel
            control={
              <Checkbox
                checked={Boolean(values[field.name])}
                onChange={(e) => setFieldValue(field.name, e.target.checked)}
                id={field.id}
              />
            }
            label={field.label}
          />
        );
      case 'component':
        if (isIconItem(field.value)) {
          const iconName = field.value.iconName;
          const color = values[field.value.color] !== ''
            ? IconColors[values[field.value.color] as keyof typeof IconColors]
            : IconColors.Blue;
          const icon = typeof values[iconName] === 'string' ? values[iconName] : null;

          return (
            <Box sx={{
              borderRadius: '4px',
              border: '1px solid #d9d9d9',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              minHeight: 90
            }}>
              <Avatar sx={{bgcolor: color ? `${color}1A` : 'primary.lighter', width: 56, height: 56}}>
                {getCategoryIconByType(icon, field.value.type || ItemType.Income, color, 21)}
              </Avatar>
            </Box>
          );
        }
        return null;
      default:
        return null;
    }
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      enableReinitialize
      onSubmit={onSubmit}
    >
      {({values, errors, touched, setFieldValue, handleSubmit, isSubmitting}) => (
        <Form noValidate onSubmit={handleSubmit}>
          <Grid container spacing={3}>
            {formFields.map((field) => (
              <Grid
                item
                xs={cr.xs}
                sm={cr.sm}
                md={cr.md}
                lg={cr.lg}
                xl={cr.xl}
                key={field.id}
                sx={{display: field.hidden ? 'none' : 'block'}}
              >
                <Stack spacing={1}>
                  <InputLabel htmlFor={field.name}>{field.label}</InputLabel>
                  {renderField(field, values, touched, errors, setFieldValue)}
                  {touched[field.name] && errors[field.name] && (
                    <FormHelperText error id={`helper-text-${field.name}`}>
                      {errors[field.name] as string}
                    </FormHelperText>
                  )}
                </Stack>

                {field.children?.map((childField) => (
                  <Stack spacing={1} sx={{mt: 2.7}} key={childField.id}>
                    <InputLabel htmlFor={childField.name}>{childField.label}</InputLabel>
                    {renderField(childField, values, touched, errors, setFieldValue)}
                    {touched[childField.name] && errors[childField.name] && (
                      <FormHelperText error id={`helper-text-${childField.name}`}>
                        {errors[childField.name] as string}
                      </FormHelperText>
                    )}
                  </Stack>
                ))}
              </Grid>
            ))}
          </Grid>
          <Grid container justifyContent="flex-end" sx={{mt: 2.5}}>
            <Button
              disabled={isSubmitting}
              color="primary"
              onClick={onClose}
            >
              Cancel
            </Button>
            <Button
              disabled={isSubmitting}
              type="submit"
              variant="contained"
              color="primary"
              sx={{ml: 2}}
            >
              {formMode === FormAction.Update ? 'Update' : 'Create'}
            </Button>
          </Grid>
        </Form>
      )}
    </Formik>
  );
};

export default CrudForm;