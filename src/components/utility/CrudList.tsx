import { memo, useCallback, useState } from "react";
import {
  Avatar,
  Box,
  IconButton,
  List,
  ListItemAvatar,
  ListItemButton,
  ListItemSecondaryAction,
  ListItemText,
  Typography,
  Fade,
} from "@mui/material";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";

import MainCard from "@components/utility/MainCard";
import { actionSX, avatarSX, capitalizeFirstLetter } from "@utils/Helpers";
import ShowMore from "@components/finance/ShowMore";
import { EntityList, CrudList as ICrudList } from "@shared/types/Crud";

const CrudList = <T, >({items, onEdit, onDelete, noOfItems = 6}: EntityList<T>) => {
  const [expanded, setExpanded] = useState(false);
  const [hoveredItem, setHoveredItem] = useState<string | null>(null);

  const handleMouseEnter = useCallback((id: string) => setHoveredItem(id), []);
  const handleMouseLeave = useCallback(() => setHoveredItem(null), []);

  const visibleItems = expanded ? items : items.slice(0, noOfItems);

  const renderListItem = (item: ICrudList, index: number) => (
    <ListItemButton
      divider={index !== visibleItems.length - 1}
      key={item.id}
      onMouseEnter={() => handleMouseEnter(item.id || '')}
      onMouseLeave={handleMouseLeave}
      onClick={() => onEdit?.(item.field as T || null)}
      disableTouchRipple
      sx={{
        backgroundColor: index % 2 === 0 ? 'transpareent' : '#f5f5f580', // Stripe effect
        '&:hover': {
          transition: 'box-shadow 225ms ease-in-out', // Default MUI Fade duration
          boxShadow: '0 1px 2px 0 rgba(60, 64, 67, .3)',
        },
      }}
    >
      <Fade in={hoveredItem === item.id}>
        <Box
          sx={{
            position: 'absolute',
            top: 0,
            bottom: 0,
            right: 0,
            display: 'flex',
            alignItems: 'center',
            gap: 1,
            p: 1.6,
            backgroundColor: 'background.paper',
            borderLeft: 1,
            borderColor: 'divider',
            zIndex: 1,
            boxShadow: '-1px 0 1px rgba(60, 64, 67, 0.12)',
            transition: 'box-shadow 225ms ease-in-out', // Sync with Fade
          }}
          onClick={(e) => e.stopPropagation()}
        >
          <IconButton
            onClick={(e) => {
              e.stopPropagation();
              onEdit?.(item.field as T || null);
            }}
            aria-label="Edit"
          >
            <EditOutlined />
          </IconButton>
          <IconButton
            onClick={(e) => {
              e.stopPropagation();
              onDelete?.(item.field as T || null);
            }}
            aria-label="Delete"
          >
            <DeleteOutlined />
          </IconButton>
        </Box>
      </Fade>

      {item.icon && (
        <ListItemAvatar>
          <Avatar
            sx={{
              color: item.icon_color || 'primary.main',
              bgcolor: item.icon_color ? `${item.icon_color}1A` : 'primary.lighter',
            }}
          >
            {item.icon}
          </Avatar>
        </ListItemAvatar>
      )}
      <ListItemText
        primary={
          <Typography variant="subtitle1" noWrap>
            {capitalizeFirstLetter(item.title)}
          </Typography>
        }
        secondary={capitalizeFirstLetter(item.subtitle)}
        sx={{
          pr: {xs: 3, sm: 3, md: 3, lg: 6},
          '& .MuiListItemText-secondary': {
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            textOverflow: 'ellipsis',
          },
        }}
      />
      <ListItemSecondaryAction sx={{display: 'flex', flexDirection: 'row'}}>
        <Typography variant="subtitle1" fontWeight={600}>
          {item.actionText}
        </Typography>
      </ListItemSecondaryAction>
    </ListItemButton>
  );

  return (
    <MainCard sx={{mt: 2}} content={false}>
      <List
        component="nav"
        sx={{
          px: 0,
          py: 0,
          '& .MuiListItemButton-root': {
            py: 1.5,
            position: 'relative',
            '& .MuiAvatar-root': avatarSX,
            '& .MuiListItemSecondaryAction-root': {...actionSX, position: 'relative'},
          },
        }}
      >
        {visibleItems.map(renderListItem)}
      </List>

      {items.length > noOfItems && (
        <Box sx={{ml: 2, mt: -0.5, pb: 1}}>
          <ShowMore
            expanded={expanded}
            setExpanded={setExpanded}
            items={items}
            visibleItems={visibleItems}
          />
        </Box>
      )}
    </MainCard>
  );
}

const MemoizedCrudList = memo(CrudList) as typeof CrudList;
export default MemoizedCrudList;