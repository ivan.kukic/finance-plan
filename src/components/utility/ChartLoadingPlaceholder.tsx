import { Box, CircularProgress } from "@mui/material";

interface ChartLoadingPlaceholderProps {
  height?: number;
}

const ChartLoadingPlaceholder = ({height = 420}: ChartLoadingPlaceholderProps) => (
  <Box
    sx={{
      height: `${height}px`,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'background.paper',
      borderRadius: 1,
    }}
  >
    <CircularProgress />
  </Box>
);

export default ChartLoadingPlaceholder;