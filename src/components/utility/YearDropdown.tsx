import { memo, useEffect } from 'react';
import { useLocation, useNavigate, useParams } from "react-router-dom";
import { MenuItem, Select, SelectChangeEvent } from "@mui/material";

import { useAppDispatch, useAppSelector } from "@hooks/redux";
import { selectedYear, setYear } from "@store/reducers/yearSlice";

const YearDropdown = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const location = useLocation();
  const {year: paramYear} = useParams<{ year: string }>();

  const stateYear = useAppSelector(selectedYear);
  const currentYear = new Date().getFullYear();

  const dropdownSelectedYear = paramYear || stateYear.toString();
  const years = Array.from({length: 9}, (_, i) => currentYear - 4 + i);

  useEffect(() => {
    if (paramYear) {
      dispatch(setYear(parseInt(paramYear, 10)));
    }
  }, [dispatch, paramYear]);

  const handleYearChange = (event: SelectChangeEvent<string>) => {
    const selectedYear = Number(event.target.value);
    dispatch(setYear(selectedYear));

    const newPath = location.pathname.replace(/\/\d{4}\//, `/${selectedYear}/`);
    navigate(newPath);
  };

  return (
    <Select
      value={dropdownSelectedYear.toString()}
      onChange={handleYearChange}
      sx={{
        minWidth: 100,
        mr: 2,
        height: 40,
        '& .MuiSelect-select': {
          py: 0.5,
          px: 1.5
        }
      }}
    >
      {years.map((year) => (
        <MenuItem key={year} value={year.toString()}>
          {year}
        </MenuItem>
      ))}
    </Select>
  );
};

const MemorizedYearDropdown = memo(YearDropdown);
export default MemorizedYearDropdown;
