import { Box, CircularProgress } from "@mui/material";

interface ButtonLoadingPlaceholderProps {
  text?: string;
}

const ButtonLoadingPlaceholder = ({text = 'Loading...'}: ButtonLoadingPlaceholderProps) => (
  <Box>
    <CircularProgress
      size={24}
      sx={{position: 'absolute', top: '50%', left: '50%', marginTop: '-12px', marginLeft: '-12px'}}
    />
    <span className="sr-only">{text}</span>
  </Box>
);

export default ButtonLoadingPlaceholder;