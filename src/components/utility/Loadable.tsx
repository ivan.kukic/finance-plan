import { ElementType, Suspense } from 'react';

import Loader from './Loader';
import { JSX } from 'react/jsx-runtime';

const Loadable = (Component: ElementType) => (props: JSX.IntrinsicAttributes) => (
  <Suspense fallback={<Loader />}>
    <Component {...props} />
  </Suspense>
);

export default Loadable;
