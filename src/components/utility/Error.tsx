import { Link, useRouteError } from 'react-router-dom';
import { Button, Typography } from '@mui/material';
import { Container, Stack } from '@mui/system';
import { ApolloError } from "@apollo/client";

const Error = ({apiError}: { apiError: ApolloError }) => {
  const error: unknown = useRouteError();
  console.error(error || apiError);

  const getErrorMessage = (): string | undefined => {
    if (error && typeof error === 'object') {
      // Check if the error object has a 'statusText' or 'message' property
      if ('statusText' in error) {
        return (error as { statusText: string }).statusText;
      }
      if ('message' in error) {
        return (error as { message: string }).message;
      }
    }
    return apiError?.message;
  };


  return (
    <Container maxWidth="sm" sx={{pt: 5}}>
      <Typography
        component="h1"
        variant="h2"
        align="center"
        color="text.primary"
        gutterBottom
      >
        Oops!
      </Typography>
      <Typography variant="h5" align="center" color="secondary" paragraph>
        Sorry, an unexpected error has occurred.
      </Typography>
      <Typography variant="h6" align="center" color="error" paragraph>
        {getErrorMessage()}
      </Typography>
      <Stack sx={{pt: 4}} direction="row" spacing={2} justifyContent="center">
        <Button component={Link} variant="outlined" to="/">
          Back
        </Button>
      </Stack>
    </Container>
  );
};

export default Error;
