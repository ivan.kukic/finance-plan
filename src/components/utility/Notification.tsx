import { SyntheticEvent, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { Alert, Snackbar, SnackbarCloseReason } from '@mui/material';

import { INotification, NotificationType } from "@shared/enum/NotificationType";
import { closeNotification, selectNotificationState } from "@store/reducers/notification";
import { useAppDispatch, useAppSelector } from "@hooks/redux";

export interface INotificationRootState {
  notification: INotification;
}

const Notification = () => {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const {type, open, message} = useAppSelector(selectNotificationState);

  const handleClose = (_event?: SyntheticEvent | Event, reason?: SnackbarCloseReason,) => {
    if (reason === 'clickaway') {
      return;
    }
    dispatch(closeNotification())
  };

  useEffect(() => {
    return () => {
      dispatch(closeNotification()); // Reset state when component unmounts
    };
  }, [dispatch, navigate]);

  const renderContent = () => {
    switch (type) {
      case NotificationType.Success:
        return <Snackbar
          anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}
          open={open}
          autoHideDuration={4500}
          onClose={handleClose}>
          <Alert
            onClose={handleClose}
            severity="success"
            variant="filled"
            sx={{width: '100%'}}
          >
            {message}
          </Alert>
        </Snackbar>;

      case NotificationType.Info:
        return <></>;

      case NotificationType.Error:
        return <Snackbar
          anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}
          open={open}
          onClose={handleClose}>
          <Alert
            onClose={handleClose}
            severity="error"
            variant="filled"
            sx={{width: '100%'}}
          >
            Error: {message}
          </Alert>
        </Snackbar>;
    }
  };

  return (
    <>
      {renderContent()}
    </>
  )
}

export default Notification;
