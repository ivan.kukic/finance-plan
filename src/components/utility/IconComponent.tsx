import * as AntDesignIcons from "@ant-design/icons";

export const IconComponent = ({iconName, color = '#1677ff', size = 16}: {
  iconName: unknown,
  color?: string | unknown,
  size?: number
}) => {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-expect-error
  const Icon = AntDesignIcons[iconName];

  return Icon ? <Icon width="36" loading="lazy" style={{color: color, fontSize: size}} /> : null;
};