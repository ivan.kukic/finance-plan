import { CSSProperties, forwardRef, ReactNode } from 'react';
import { useTheme } from '@mui/material/styles';
import { Card, CardContent, CardHeader, Divider, Typography } from '@mui/material';

const cardHeaderSX = {
  p: 2.5,
  '& .MuiCardHeader-action': {m: '0px auto', alignSelf: 'center'}
};

interface CustomCSSProperties extends CSSProperties {
  mt?: string | number;
  mb?: string | number;
  bgcolor?: string | object;
  '& > *'?: CSSProperties;
  p?: string | number;
}

interface MainCardProps {
  border?: boolean;
  boxShadow?: boolean;
  contentSX?: CustomCSSProperties;
  darkTitle?: boolean;
  divider?: boolean;
  elevation?: number;
  secondary?: ReactNode;
  shadow?: string;
  sx?: CustomCSSProperties;
  headerSX?: any;
  title?: string | ReactNode;
  codeHighlight?: boolean;
  content?: boolean;
  children?: ReactNode;
}

const MainCard = forwardRef<HTMLDivElement, MainCardProps>(
  (
    {
      border = true,
      boxShadow,
      children,
      content = true,
      contentSX = {},
      darkTitle,
      elevation,
      secondary,
      shadow,
      sx = {},
      headerSX = cardHeaderSX,
      title,
      codeHighlight,
      ...others
    },
    ref
  ) => {
    const theme = useTheme();
    boxShadow = theme.palette.mode === 'dark' ? boxShadow || true : boxShadow;

    return (
      <Card
        elevation={elevation || 0}
        ref={ref}
        {...others}
        sx={{
          border: border ? '1px solid' : 'none',
          borderRadius: 2,
          borderColor: theme.palette.grey.A700,
          boxShadow: boxShadow && (!border || theme.palette.mode === 'dark') ? shadow || theme.customShadows.z1 : 'inherit',
          ':hover': {
            boxShadow: boxShadow ? shadow : 'inherit'
          },
          '& pre': {
            m: 0,
            p: '16px !important',
            fontFamily: theme.typography.fontFamily,
            fontSize: '0.75rem'
          },
          ...sx
        }}
      >
        {/* card header and action */}
        {!darkTitle && title && (
          <>
            <CardHeader sx={headerSX} titleTypographyProps={{variant: 'subtitle1'}} title={title} action={secondary} />
            <Divider />
          </>
        )}
        {darkTitle && title && (
          <>
            <CardHeader sx={headerSX} title={<Typography variant="h3">{title}</Typography>} action={secondary} />
            <Divider />
          </>
        )}

        {/* card content */}
        {content && <CardContent sx={contentSX}>{children}</CardContent>}
        {!content && children}

        {/* card footer - clipboard & highlighter  */}
        {codeHighlight && (
          <>
            <Divider sx={{borderStyle: 'dashed'}} />
            {children}
          </>
        )}
      </Card>
    );
  }
);

export default MainCard;