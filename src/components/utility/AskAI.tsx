import { useState, useCallback, ReactNode, useEffect } from 'react';
import { Button, Grid, Stack, Typography, Dialog, DialogContent, CircularProgress, Box } from '@mui/material';
import { CheckCircleOutlined, OpenAIOutlined as OpenAIIcon } from "@ant-design/icons";

import MainCard from '@components/utility/MainCard';
import { Item } from '@shared/types/Item';
import { AIType } from "@shared/enum/AIType";
import { ClaudeIcon, PerplexityIcon } from "@components/utility/Icons";

interface AIConfig {
  name: string;
  url: string;
  icon: ReactNode;
  message: string;
}

const aiConfigs: Record<AIType, AIConfig> = {
  [AIType.ChatGPT]: {
    name: 'ChatGPT',
    url: 'https://chatgpt.com',
    icon: <OpenAIIcon style={{fontSize: 32}} />,
    message: "Hello, let's start my OpenAI ChatGPT financial planning. Please analyze my financial data and provide me with detailed and practical financial advice that will help me in budgeting and achieving my financial goals. Here are my data:"
  },
  [AIType.Claude]: {
    name: 'Claude',
    url: 'https://claude.ai',
    icon: <ClaudeIcon />,
    message: "Hello Claude, I need your help with financial planning. Please analyze my financial data and provide detailed advice for budgeting and achieving my financial goals. Here's my data:"
  },
  [AIType.Perplexity]: {
    name: 'Perplexity',
    url: 'https://www.perplexity.ai',
    icon: <PerplexityIcon />,
    message: "Hi Perplexity AI, I'm seeking financial planning assistance. Could you analyze my financial data and offer comprehensive advice for budgeting and reaching my financial objectives? Here's my financial information:"
  }
};

interface IAskAI {
  items?: Item[];
  aiType: AIType;
}

const AskAI = ({items, aiType}: IAskAI) => {
  const [isRedirecting, setIsRedirecting] = useState(false);
  const [countdown, setCountdown] = useState(6);

  const handleClick = useCallback(() => {
    const config = aiConfigs[aiType];
    const fullMessage = config.message + JSON.stringify(items);
    navigator.clipboard.writeText(fullMessage);
    setIsRedirecting(true);
  }, [aiType, items]);

  const handleClose = useCallback(() => {
    setIsRedirecting(false);
    setCountdown(6);
  }, []);

  useEffect(() => {
    let timer: NodeJS.Timeout;
    if (isRedirecting && countdown > 0) {
      timer = setTimeout(() => setCountdown(countdown - 1), 1000);
    } else if (countdown === 0) {
      window.open(aiConfigs[aiType].url, '_blank');
      handleClose();
    }
    return () => clearTimeout(timer);
  }, [isRedirecting, countdown, aiType, handleClose]);

  const config = aiConfigs[aiType];

  return (
    <MainCard>
      <Stack spacing={3}>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <Stack>
              <Typography variant="h5" noWrap>
                {config.name} personalized advice
              </Typography>
              <Typography variant="caption" color="text.secondary">
                Get tailored advice to improve your financial potential.
              </Typography>
            </Stack>
          </Grid>
          <Grid item>
            {config.icon}
          </Grid>
        </Grid>
        <Button onClick={handleClick} size="small" variant="contained">
          Ask {config.name}
        </Button>
      </Stack>
      <Dialog open={isRedirecting} onClose={handleClose}>
        <DialogContent>
          <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center">
            <Typography variant="h4" mt={2} mb={2}>
              <CheckCircleOutlined style={{paddingRight: '12px', color: '#34a853', fontSize: 24}} /> Financial Data
              Copied Successfully
            </Typography>
            <Typography variant="h6" mt={2} mb={2}>
              <strong>Instruction:</strong> Your financial data has been copied.
              Use <strong>CTRL+V</strong> (or <strong>Command+V </strong>on Mac) to
              paste it into the input box and ask your question to <strong>{config.name}</strong>.
            </Typography>
            <CircularProgress variant="determinate" value={(countdown / 6) * 100} size={60} thickness={5} />
            <Typography variant="h6" mt={2}>Redirecting
              to <strong>{config.name}</strong> in <strong>{countdown} seconds...</strong></Typography>
            <Button onClick={handleClose} sx={{mt: 2}}>
              Cancel
            </Button>
          </Box>
        </DialogContent>
      </Dialog>
    </MainCard>
  );
};

export default AskAI;