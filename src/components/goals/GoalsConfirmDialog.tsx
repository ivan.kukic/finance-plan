import { SyntheticEvent, useEffect, useState } from "react";
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from "@mui/material";

import { Category } from "@shared/types/Category";
import { useMutation } from "@apollo/client";
import { DELETE_CATEGORY, deleteCategoryCache } from "@services/CategoryService";
import { openNotification } from "@store/reducers/notification";
import { NotificationType } from "@shared/enum/NotificationType";
import { useAppDispatch, useAppSelector } from "@hooks/redux";
import {
  closeCategoryConfirmDialog,
  selectCategoryConfirmDialogItem,
} from "@store/reducers/genericDialogSlice";

export interface IConfirmDialog {
  dialogOpen: boolean;
  title: string;
  description: string;
}

const GoalsConfirmDialog = ({dialogOpen, title, description}: IConfirmDialog) => {
  const dispatch = useAppDispatch();
  const [open, setOpen] = useState(dialogOpen);

  const [deleteCategoryByPk] = useMutation(DELETE_CATEGORY, {update: deleteCategoryCache});
  const category = useAppSelector(selectCategoryConfirmDialogItem) as Category;

  const handleDeleteCancelClick = (_e: SyntheticEvent): void => {
    _e.preventDefault();
    dispatch(closeCategoryConfirmDialog());
  };

  const handleDeleteConfirmClick = async (_e: SyntheticEvent): Promise<void> => {
    _e.preventDefault();
    if (category) {
      await deleteCategory(category)
      dispatch(closeCategoryConfirmDialog());
    }
  };

  useEffect(() => {
    setOpen(dialogOpen);
  }, [dialogOpen]);


  const deleteCategory = async (category: Category) => {
    if (category.id === undefined) {
      return;
    }

    try {
      const {errors} = await deleteCategoryByPk({variables: {id: category.id}});

      if (errors) {
        throw new Error(errors[0]?.message);
      }

      dispatch(
        openNotification({
          type: NotificationType.Success,
          message: `Category "${category.name}" has been successfully deleted.`
        })
      );
    } catch (err) {
      const error = err as Error;

      dispatch(
        openNotification({
          type: NotificationType.Error,
          message: error.message
        })
      );
    }
  };

  return (
    <Dialog
      open={open}
      onClose={handleDeleteCancelClick}
    >
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>
        <DialogContentText>
          {description}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleDeleteCancelClick} color="primary" autoFocus>
          No
        </Button>
        <Button
          onClick={handleDeleteConfirmClick}
          color="primary"
          component="button"
        >
          Yes
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default GoalsConfirmDialog;