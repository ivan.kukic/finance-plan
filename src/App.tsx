import { ReactNode, useEffect } from 'react';
import { useUserData } from '@nhost/react';

import { setUser } from '@store/reducers/authSlice';
import { IUser } from "@shared/types/User";
import { useAppDispatch } from "@hooks/redux";

const App = ({children}: { children: ReactNode }) => {
  const dispatch = useAppDispatch();
  const user = useUserData() as IUser | null;

  useEffect(() => {
    if (user) {
      dispatch(setUser(user));
    }
  }, [user, dispatch]);

  return (
    <>
      {children}
    </>
  );
}

export default App;
