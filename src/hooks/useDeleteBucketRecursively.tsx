import { Document, File, Folder } from "@shared/types/Storage";
import useDeleteBucket from "@hooks/useDeleteBuckets";
import useDeleteFile from "@hooks/useDeleteFile";

const useDeleteBucketsRecursively = () => {
  const {deleteBucket} = useDeleteBucket();
  const {deleteFile} = useDeleteFile();

  const deleteBucketRecursively = async (folder: Folder, folders: Folder[], files: File[]) => {
    const childFolders = folders.filter((f: Document) => (f as Folder).parent_id === folder.id);

    for (const childFolder of childFolders) {
      const result = await deleteBucketRecursively(childFolder, folders, files);
      folders = result.folders;
      files = result.files;
    }

    const folderFiles = files.filter(file => file.bucket_id === folder.id);
    for (const file of folderFiles) {
      await deleteFile(file as File);
    }
    const remainingFiles = files.filter(file => file.bucket_id !== folder.id);

    await deleteBucket(folder as Folder);

    const remainingFolders = folders.filter(folder => folder.id !== folder.id);

    return {
      folders: remainingFolders,
      files: remainingFiles
    };
  }

  return {deleteBucketRecursively};
};

export default useDeleteBucketsRecursively;
