import { useState } from 'react';
import { useMutation } from '@apollo/client';

import { Folder } from "@shared/types/Storage";
import { DELETE_STORAGE_BUCKET_BY_PK, deleteStorageBucketCache, } from "@services/StorageService";
import { openNotification } from "@store/reducers/notification";
import { NotificationType } from "@shared/enum/NotificationType";
import { useAppDispatch } from "@hooks/redux";

const useDeleteBucket = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState<string | null>(null);
  const dispatch = useAppDispatch();

  const [deleteStorageBucketByPk] = useMutation(DELETE_STORAGE_BUCKET_BY_PK, {update: deleteStorageBucketCache});

  const deleteBucket = async (folder: Folder) => {
    if (folder.id === undefined) {
      return;
    }

    setIsLoading(true);
    setError(null);

    try {
      const {errors} = await deleteStorageBucketByPk({variables: {id: folder.id}});

      if (errors) {
        throw new Error(errors[0]?.message);
      }

      dispatch(
        openNotification({
          type: NotificationType.Success,
          message: `Folder "${folder.name}" has been successfully deleted.`
        })
      );
    } catch (err) {
      const error = err as Error;
      setError(error.message);

      dispatch(
        openNotification({
          type: NotificationType.Error,
          message: error.message
        })
      );
    } finally {
      setIsLoading(false);
    }
  };

  return {deleteBucket, isLoading, error};
};

export default useDeleteBucket;
