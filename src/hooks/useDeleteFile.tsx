import { useState } from 'react';
import { nhost } from "@configs/nhost";
import { useMutation } from '@apollo/client';

import { File } from "@shared/types/Storage";
import { openNotification } from "@store/reducers/notification";
import { NotificationType } from "@shared/enum/NotificationType";
import { DELETE_STORAGE_FILE_BY_PK, deleteStorageFileCache } from "@services/StorageService";
import { useAppDispatch } from "@hooks/redux";

const useDeleteFile = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState<string | null>(null);
  const dispatch = useAppDispatch();

  const [deleteStorageFileByPk] = useMutation(DELETE_STORAGE_FILE_BY_PK, {update: deleteStorageFileCache});

  const deleteFile = async (file: File) => {
    if (file.id === undefined || file.files_id === undefined) {
      return;
    }

    setIsLoading(true);
    setError(null);

    try {
      const {error} = await nhost.storage.delete({fileId: file.files_id});

      if (error) {
        throw new Error(error.message);
      }

      const {errors} = await deleteStorageFileByPk({variables: {id: file.id}});

      if (errors) {
        throw new Error(errors[0]?.message);
      }

      dispatch(openNotification({
        type: NotificationType.Success,
        message: `File "${file.name}" has been successfully deleted.`
      }))

    } catch (err) {
      const error = err as Error;
      setError(error.message);

      dispatch(
        openNotification({
          type: NotificationType.Error,
          message: error.message
        })
      );

    } finally {
      setIsLoading(false);
    }
  };

  return {deleteFile, isLoading, error};
};

export default useDeleteFile;
