import { ApolloError, useQuery } from '@apollo/client';

import { GET_ITEMS } from "@services/GraphQLService";
import { Item } from "@shared/types/Item";
import { ItemType } from "@shared/enum/itemType";

export interface IGetItems {
  items: Item[] | [],
  incomeItems: Item[] | [],
  expenseItems: Item[] | [],
  yearlyIncomeItems: Item[] | [],
  yearlyExpenseItems: Item[] | [],
  yearlyIncomeItemsPrev: Item[] | [],
  yearlyExpenseItemsPrev: Item[] | [],
}

interface IUseGetItems {
  userId: string | undefined,
  year?: number,
}

interface IUseGetItemsReturn {
  loading: boolean;
  error: ApolloError | null | undefined;
  data: IGetItems;
}

const useGetItems = ({userId, year}: IUseGetItems): IUseGetItemsReturn => {
  const {loading, error, data} = useQuery(GET_ITEMS, {
    variables: {
      users_id: userId,
      ...(typeof year !== 'undefined' && {year: year}), // Include 'year' only when defined
    },
    errorPolicy: 'all'
  });

  if (loading) return {loading: true, error: error, data: {} as IGetItems};
  if (error) return {loading: false, error: error, data: {} as IGetItems};

  const currentYear = year || Number(new Date().getFullYear())

  const items: Item[] = data?.item || [];

  const incomeItems: Item[] = items.filter((item: Item) => item.type === ItemType.Income && item.year === currentYear);
  const expenseItems: Item[] = items.filter((item: Item) => item.type === ItemType.Expense && item.year === currentYear);

  const yearlyIncomeItems: Item[] = incomeItems.filter((item: Item) => item.year === currentYear);
  const yearlyExpenseItems: Item[] = expenseItems.filter((item: Item) => item.year === currentYear);

  const yearlyIncomeItemsPrev: Item[] = incomeItems.filter((item: Item) => item.year === currentYear - 1);
  const yearlyExpenseItemsPrev: Item[] = expenseItems.filter((item: Item) => item.year === currentYear - 1);

  return {
    loading: false, error: null, data: {
      items: items,
      incomeItems: incomeItems,
      expenseItems: expenseItems,
      yearlyIncomeItems: yearlyIncomeItems,
      yearlyExpenseItems: yearlyExpenseItems,
      yearlyIncomeItemsPrev: yearlyIncomeItemsPrev,
      yearlyExpenseItemsPrev: yearlyExpenseItemsPrev,
    }
  };
};

export default useGetItems;