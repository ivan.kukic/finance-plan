import { ApolloError, useQuery } from '@apollo/client';

import { ItemType } from "@shared/enum/itemType";
import { GET_CATEGORIES } from "@services/CategoryService";
import { Category } from "@shared/types/Category";

export interface IGetCategories {
  items: Category[] | [],
  incomeItems: Category[] | [],
  expenseItems: Category[] | [],
}

interface IUseGetCategories {
  userId: string | undefined,
}

interface IUseGetCategoriesReturn {
  loading: boolean;
  error: ApolloError | null | undefined;
  data: IGetCategories;
}

const useGetCategories = ({userId}: IUseGetCategories): IUseGetCategoriesReturn => {
  const {loading, error, data} = useQuery(GET_CATEGORIES, {
    variables: {
      users_id: userId,
    },
    errorPolicy: 'all'
  });

  if (loading) return {loading: true, error: error, data: {} as IGetCategories};
  if (error) return {loading: false, error: error, data: {} as IGetCategories};

  const items: Category[] = data?.category || [];

  const incomeItems: Category[] = items.filter((item: Category) => item.type === ItemType.Income);
  const expenseItems: Category[] = items.filter((item: Category) => item.type === ItemType.Expense);


  return {
    loading: false, error: null, data: {
      items: items,
      incomeItems: incomeItems,
      expenseItems: expenseItems,
    }
  };
};

export default useGetCategories;