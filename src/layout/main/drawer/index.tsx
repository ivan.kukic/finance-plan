import { useMemo } from "react";
import { Box, Drawer, useMediaQuery, useTheme } from "@mui/material";

import DrawerHeader from "./header";
import DrawerContent from "./content";
import MiniDrawerStyled from "./MiniDrawerStyled";
import { drawerWidth } from '@configs/config';

export interface MainDrawerProps {
  open: boolean;
  handleDrawerToggle: () => void;
}

const MainDrawer = ({open, handleDrawerToggle}: MainDrawerProps) => {
  const theme = useTheme();
  const matchDownMD = useMediaQuery(theme.breakpoints.down('lg'));

  // responsive drawer container
  const container = typeof window !== 'undefined' ? document.body : undefined;

  // drawer content
  const drawerContent = useMemo(() => <DrawerContent />, []);
  const drawerHeader = useMemo(() => <DrawerHeader open={open} />, [open]);

  return (
    <Box component="nav" sx={{flexShrink: {md: 0}, zIndex: 1300}} aria-label="mailbox folders">
      {!matchDownMD ? (
        <MiniDrawerStyled variant="permanent" open={open}>
          {drawerHeader}
          {drawerContent}
        </MiniDrawerStyled>
      ) : (
        <Drawer
          container={container}
          variant="temporary"
          open={open}
          onClose={handleDrawerToggle}
          ModalProps={{keepMounted: true}}
          sx={{
            display: {xs: 'block', lg: 'none'},
            '& .MuiDrawer-paper': {
              boxSizing: 'border-box',
              width: drawerWidth,
              borderRight: `1px solid ${theme.palette.divider}`,
              backgroundImage: 'none',
              boxShadow: 'inherit'
            }
          }}
        >
          {open && drawerHeader}
          {open && drawerContent}
        </Drawer>
      )}
    </Box>
  );
};

export default MainDrawer;