import { Chip, Stack, useTheme } from "@mui/material";

import DrawerHeaderStyled from "./DrawerHeaderStyled";
import Logo from "@components/utility/Logo";

const DrawerHeader = ({open}: { open: boolean }) => {
  const theme = useTheme();

  return (
    <DrawerHeaderStyled className="DrawerHeader" theme={theme} open={open}>
      <Stack direction="row" spacing={1} alignItems="center " sx={{pt: .6}}>
        <Logo />
        <Chip
          label={import.meta.env.VITE_APP_VERSION}
          size="small"
          sx={{height: 16, '& .MuiChip-label': {fontSize: '0.625rem', py: 0.25}}}
          component="a"
          href=""
          target="_blank"
          clickable
        />
      </Stack>
    </DrawerHeaderStyled>
  );
};

export default DrawerHeader;