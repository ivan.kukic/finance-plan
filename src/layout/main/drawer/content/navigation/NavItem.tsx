import { forwardRef, ForwardRefExoticComponent, RefAttributes, useEffect } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { useTheme } from '@mui/material/styles';
import { Avatar, Chip, ListItemButton, ListItemIcon, ListItemText, Typography } from '@mui/material';

import { activeItem, selectMenuState } from "@store/reducers/menu";
import { ChildItem } from "@layout/main/menu/Menu";
import { useAppDispatch, useAppSelector } from "@hooks/redux";

interface LinkProps {
  to: string;
  target?: string;
}

interface ListItemProps {
  component: HTMLAnchorElement | ForwardRefExoticComponent<LinkProps & RefAttributes<HTMLAnchorElement>> | "a";
  href?: string;
  to?: string;
  target?: string;
}

const NavItem = ({item, level}: { item: ChildItem, level: number }) => {
  const theme = useTheme();
  const dispatch = useAppDispatch();
  const {pathname} = useLocation();

  const {drawerOpen, openItem} = useAppSelector(selectMenuState);

  let itemTarget = '_self';
  if (item.target) {
    itemTarget = '_blank';
  }

  let listItemProps: ListItemProps = {
    component: forwardRef<HTMLAnchorElement, LinkProps>((props, ref) => <Link ref={ref} {...props} to={item.url}
                                                                              target={itemTarget} />)
  };
  if (item?.external) {
    listItemProps = {component: 'a', href: item.url, target: itemTarget};
  }

  const itemHandler = (id: string) => {
    dispatch(activeItem({openItem: [id]}));
  };

  const Icon = item.icon;
  const itemIcon = !item.icon ? false : <Icon style={{fontSize: drawerOpen ? '1rem' : '1.25rem'}} />;

  const isSelected = openItem.findIndex((id) => id === item.id) > -1;
  // active menu item on page load
  useEffect(() => {
    if (pathname.includes(item.url)) {
      dispatch(activeItem({openItem: [item.id]}));
    }
    // eslint-disable-next-line
  }, [pathname]);

  const textColor = 'text.primary';
  const iconSelectedColor = 'primary.main';

  return (
    <ListItemButton
      {...listItemProps}
      disabled={item.disabled}
      onClick={() => itemHandler(item.id)}
      selected={isSelected}
      sx={{
        zIndex: 1201,
        pl: drawerOpen ? `${level * 28}px` : 1.5,
        py: !drawerOpen && level === 1 ? 1.25 : 1,
        ...(drawerOpen && {
          '&:hover': {
            bgcolor: 'primary.lighter'
          },
          '&.Mui-selected': {
            bgcolor: 'primary.lighter',
            borderRight: `2px solid ${theme.palette.primary.main}`,
            color: iconSelectedColor,
            '&:hover': {
              color: iconSelectedColor,
              bgcolor: 'primary.lighter'
            }
          }
        }),
        ...(!drawerOpen && {
          '&:hover': {
            bgcolor: 'transparent'
          },
          '&.Mui-selected': {
            '&:hover': {
              bgcolor: 'transparent'
            },
            bgcolor: 'transparent'
          }
        })
      }}
    >
      {itemIcon && (
        <ListItemIcon
          sx={{
            minWidth: 28,
            color: isSelected ? iconSelectedColor : textColor,
            ...(!drawerOpen && {
              borderRadius: 1.5,
              width: 36,
              height: 36,
              alignItems: 'center',
              justifyContent: 'center',
              '&:hover': {
                bgcolor: 'secondary.lighter'
              }
            }),
            ...(!drawerOpen &&
              isSelected && {
                bgcolor: 'primary.lighter',
                '&:hover': {
                  bgcolor: 'primary.lighter'
                }
              })
          }}
        >
          {itemIcon}
        </ListItemIcon>
      )}
      {(drawerOpen || (!drawerOpen && level !== 1)) && (
        <ListItemText
          primary={
            <Typography variant="h6" sx={{color: isSelected ? iconSelectedColor : textColor}}>
              {item.title}
            </Typography>
          }
        />
      )}
      {(drawerOpen || (!drawerOpen && level !== 1)) && item.chip && (
        <Chip
          color={item.chip.color}
          variant={item.chip.variant}
          size={item.chip.size}
          label={item.chip.label}
          avatar={item.chip.avatar && <Avatar>{item.chip.avatar}</Avatar>}
        />
      )}
    </ListItemButton>
  );
};

export default NavItem;

