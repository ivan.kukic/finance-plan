import { Box, List, Typography } from '@mui/material';

import { ChildItem, NavigationItem } from "@layout/main/menu/Menu";
import { selectMenuState } from "@store/reducers/menu";
import { useAppSelector } from "@hooks/redux";
import NavItem from "./NavItem";

const NavGroup = ({item}: { item: NavigationItem }) => {
  const {drawerOpen} = useAppSelector(selectMenuState);

  const navCollapse = item.children?.map((menuItem: ChildItem) => {
    switch (menuItem.type) {
      case 'collapse':
        return (
          <Typography key={menuItem.id} variant="caption" color="error" sx={{p: 2.5}}>
            collapse - only available in paid version
          </Typography>
        );
      case 'item':
        return <NavItem key={menuItem.id} item={menuItem} level={1} />;
      default:
        return (
          <Typography key={menuItem.id} variant="h6" color="error" align="center">
            Fix - Group Collapse or Items
          </Typography>
        );
    }
  });

  return (
    <List
      subheader={
        item.title &&
        drawerOpen && (
          <Box sx={{pl: 3, mb: 1.5}}>
            <Typography variant="subtitle2" color="textSecondary">
              {item.title}
            </Typography>
          </Box>
        )
      }
      sx={{mb: drawerOpen ? 1.5 : 0, py: 0, zIndex: 0}}
    >
      {navCollapse}
    </List>
  );
};

export default NavGroup;