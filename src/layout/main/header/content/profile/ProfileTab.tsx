import { SyntheticEvent, useState } from 'react';
import { Link } from 'react-router-dom';
import { useTheme } from '@mui/material/styles';
import { List, ListItemButton, ListItemIcon, ListItemText } from '@mui/material';
import { LogoutOutlined, SettingOutlined, UserOutlined } from '@ant-design/icons';

import { useAppDispatch } from "@hooks/redux";
import { closeDropdownMenu } from "@store/reducers/dropdownMenu";

interface IProfileTab {
  handleLogout: () => void;
}

const ProfileTab = ({handleLogout}: IProfileTab) => {
  const theme = useTheme();
  const dispatch = useAppDispatch();

  const [selectedIndex, setSelectedIndex] = useState(-1);
  const handleListItemClick = (_event: SyntheticEvent, index: number) => {
    setSelectedIndex(index);
    dispatch(closeDropdownMenu());
  };

  return (
    <List component="nav" sx={{p: 0, '& .MuiListItemIcon-root': {minWidth: 32, color: theme.palette.grey[500]}}}>
      <ListItemButton component={Link} selected={selectedIndex === 0} onClick={(event) => handleListItemClick(event, 0)}
                      to="/user-profile">
        <ListItemIcon>
          <UserOutlined />
        </ListItemIcon>
        <ListItemText primary="User Profile" />
      </ListItemButton>
      <ListItemButton component={Link} selected={selectedIndex === 1} onClick={(event) => handleListItemClick(event, 1)}
                      to="/user-password">
        <ListItemIcon>
          <SettingOutlined />
        </ListItemIcon>
        <ListItemText primary="Settings" />
      </ListItemButton>
      <ListItemButton selected={selectedIndex === 2} onClick={handleLogout}>
        <ListItemIcon>
          <LogoutOutlined />
        </ListItemIcon>
        <ListItemText primary="Logout" />
      </ListItemButton>
    </List>
  );
};

export default ProfileTab;