import { SyntheticEvent, useEffect, useRef, useState } from 'react';
import { useTheme } from '@mui/material/styles';
import {
  Avatar,
  Box,
  ButtonBase,
  CardContent,
  ClickAwayListener,
  Grid,
  IconButton,
  Paper,
  Popper,
  Stack,
  Typography
} from '@mui/material';
import { useSignOut } from "@nhost/react";
import { CaretDownOutlined, LogoutOutlined } from '@ant-design/icons';

import Transitions from "@components/utility/Transitions";
import MainCard from "@components/utility/MainCard";
import { GravatarProps } from "@shared/utils/AvatarText";
import { getAvatar } from "@shared/utils/GravatarUtill";
import { closeDropdownMenu, openDropdownMenu, selectDropdownMenuOpen } from "@store/reducers/dropdownMenu";
import { activeItem } from "@store/reducers/menu";
import ProfileTab from './ProfileTab';
import { useAppDispatch, useAppSelector } from "@hooks/redux";
import { selectUser } from "@store/reducers/authSlice";

const Profile = () => {
  const theme = useTheme();
  const {signOut} = useSignOut();
  const dispatch = useAppDispatch();
  const user = useAppSelector(selectUser);
  const dropdownMenuOpen = useAppSelector(selectDropdownMenuOpen);

  const [gravatar, setGravatar] = useState<GravatarProps | null>(null);
  const anchorRef = useRef<HTMLAnchorElement>(null);

  const handleToggle = (e: SyntheticEvent) => {
    e.preventDefault();
    dispatch(openDropdownMenu({dropdownMenuOpen: !dropdownMenuOpen}));
  };

  const handleClose = (event: Event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target as Node)) {
      return;
    }
    dispatch(closeDropdownMenu());
  };

  const handleSignOut = () => {
    dispatch(closeDropdownMenu());
    dispatch(activeItem({openItem: ['dashboard']}));
    signOut();
  }

  const iconBackColorOpen = 'grey.300';
  const customShadow = theme.customShadows.z1;

  useEffect(() => {
    if (user) {
      getAvatar(user, setGravatar).then();
    }
  }, [user?.displayName, user?.metadata?.avatar_url]);

  return (
    <Box sx={{flexShrink: 0, ml: 0.75}}>
      <ButtonBase
        sx={{
          p: 0.25,
          bgcolor: dropdownMenuOpen ? iconBackColorOpen : 'transparent',
          borderRadius: 1,
          '&:hover': {bgcolor: 'secondary.lighter'}
        }}
        aria-label="open profile"
        ref={anchorRef}
        aria-controls={dropdownMenuOpen ? 'profile-grow' : undefined}
        aria-haspopup="true"
        onClick={handleToggle}
        href="#"
      >
        <Stack direction="row" spacing={2} alignItems="center" sx={{p: 0.5}}>
          <Avatar alt={user?.displayName} {...gravatar} />
          <Typography variant="subtitle1">{user?.displayName}</Typography>
          <CaretDownOutlined />
        </Stack>
      </ButtonBase>
      <Popper
        placement="bottom-end"
        open={dropdownMenuOpen || false}
        anchorEl={anchorRef.current}
        role={undefined}
        transition
        disablePortal
        popperOptions={{
          modifiers: [
            {
              name: 'offset',
              options: {
                offset: [0, 9]
              }
            }
          ]
        }}
      >
        {({TransitionProps}) => (
          <Transitions type="fade" in={dropdownMenuOpen} {...TransitionProps}>
            {dropdownMenuOpen && (
              <Paper
                sx={{
                  boxShadow: customShadow,
                  width: 290,
                  minWidth: 240,
                  maxWidth: 290,
                  [theme.breakpoints.down('md')]: {
                    maxWidth: 250
                  }
                }}
              >
                <ClickAwayListener onClickAway={handleClose}>
                  <MainCard elevation={0} border={false} content={false}>
                    <CardContent sx={{px: 2.5, pt: 3}}>
                      <Grid container justifyContent="space-between" alignItems="center">
                        <Grid item>
                          <Stack direction="row" spacing={1.25} alignItems="center">
                            <Avatar alt={user?.displayName} {...gravatar} />
                            <Stack sx={{
                              maxWidth: 160,
                              [theme.breakpoints.down('md')]: {
                                maxWidth: 120
                              }
                            }}>
                              <Typography variant="h6" noWrap title={user?.displayName}>{user?.displayName}</Typography>
                              <Typography variant="body2" color="textSecondary" noWrap title={user?.email}>
                                {user?.email}
                              </Typography>
                            </Stack>
                          </Stack>
                        </Grid>
                        <Grid item>
                          <IconButton size="large" color="secondary" onClick={handleSignOut}>
                            <LogoutOutlined />
                          </IconButton>
                        </Grid>
                      </Grid>
                    </CardContent>
                    {dropdownMenuOpen && (
                      <>
                        <Box sx={{borderBottom: 1, borderColor: 'divider'}}>

                        </Box>
                        <ProfileTab handleLogout={handleSignOut} />
                      </>
                    )}
                  </MainCard>
                </ClickAwayListener>
              </Paper>
            )}
          </Transitions>
        )}
      </Popper>
    </Box>
  );
};

export default Profile;
