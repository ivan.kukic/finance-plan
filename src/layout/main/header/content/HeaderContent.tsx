import { Box, IconButton, Link, Theme, useMediaQuery } from "@mui/material";
import { GitlabOutlined } from "@ant-design/icons";

import Search from "./Search";
import Profile from "./profile/Profile";
import MobileSection from "./MobileSection";
import YearDropdown from "@components/utility/YearDropdown";

const HeaderContent = () => {
  const matchesXs = useMediaQuery((theme: Theme) => theme.breakpoints.down('md'));

  return (
    <Box sx={{display: 'flex', alignItems: 'center', width: '100%'}}>
      {!matchesXs && <Search />}
      {matchesXs && <Box sx={{width: '100%', ml: 1}} />}

      <YearDropdown />

      <IconButton
        component={Link}
        href="https://gitlab.com/ivan.kukic/finance-plan"
        target="_blank"
        disableRipple
        color="secondary"
        title="Download Free Version"
        sx={{
          color: 'text.primary',
          bgcolor: 'grey.100',
          '&:hover': {
            bgcolor: 'grey.200'
          }
        }}
      >
        <GitlabOutlined />
      </IconButton>

      {!matchesXs && <Profile />}
      {matchesXs && <MobileSection />}
    </Box>
  );
};

export default HeaderContent;