import { AppBar, IconButton, Toolbar, useMediaQuery, useTheme } from "@mui/material";
import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons';
import { ThemeCustomOptions } from "@themes/ThemesIndex";

import { MainDrawerProps } from "../drawer";
import AppBarStyled from "./AppBarStyled";
import HeaderContent from "./content/HeaderContent";

const Header = ({open, handleDrawerToggle}: MainDrawerProps) => {
  const theme: ThemeCustomOptions = useTheme();
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-expect-error
  const matchDownMD = useMediaQuery(theme.breakpoints.down('lg'));

  const iconBackColor = 'grey.200';
  const iconBackColorOpen = 'transparent';

  const mainHeader = (
    <Toolbar>
      <IconButton
        disableRipple
        aria-label="open drawer"
        onClick={handleDrawerToggle}
        edge="start"
        color="secondary"
        sx={{color: 'text.primary', bgcolor: open ? iconBackColorOpen : iconBackColor, ml: {xs: 0, lg: -2}}}
      >
        {!open ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
      </IconButton>
      <HeaderContent />
    </Toolbar>
  );

  // app-bar params
  const appBar = {
    elevation: 0,
    sx: {
      borderBottom: `1px solid ${theme.palette.divider}`,
      boxShadow: theme.customShadows.z1,
      backgroundColor: 'white',
      color: 'inherit',
      position: 'fixed'
    }
  };

  return (
    <>
      {!matchDownMD ? (
        <AppBarStyled open={open} {...appBar}>
          {mainHeader}
        </AppBarStyled>
      ) : (
        <AppBar {...appBar}>{mainHeader}</AppBar>
      )}
    </>
  );
};

export default Header;
