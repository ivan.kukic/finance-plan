import { UserOutlined, SettingOutlined } from '@ant-design/icons';

import { NavigationItem } from "./Menu";

const icons = {
  UserOutlined,
  SettingOutlined
};

const profileSettings: NavigationItem = {
  id: 'group-profileSettings',
  title: 'Account',
  type: 'group',
  children: [
    {
      id: 'user-profile',
      title: 'User Profile',
      type: 'item',
      url: '/user-profile',
      icon: icons.UserOutlined,
      breadcrumbs: false
    },
    {
      id: 'Profile',
      title: 'Settings',
      type: 'item',
      url: '/user-password',
      icon: icons.SettingOutlined,
      breadcrumbs: false
    }
  ]
};

export default profileSettings;
