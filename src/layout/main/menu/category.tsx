import { TagsOutlined, ProductOutlined } from '@ant-design/icons';

import { NavigationItem } from "./Menu";

const icons = {
  TagsOutlined, ProductOutlined,
};

const category: NavigationItem = {
  id: 'group-categories',
  title: 'Categories',
  type: 'group',
  children: [
    {
      id: 'categories',
      title: 'Categories',
      type: 'item',
      url: '/categories',
      icon: icons.ProductOutlined,
      breadcrumbs: false
    },
    // {
    //   id: 'category',
    //   title: 'Add Category',
    //   type: 'item',
    //   url: '/category',
    //   icon: icons.TagsOutlined,
    //   breadcrumbs: false
    // },
  ]
};

export default category;
