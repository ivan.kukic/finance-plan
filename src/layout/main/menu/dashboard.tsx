import { DashboardOutlined } from '@ant-design/icons';

import { NavigationItem } from "./Menu";

const icons = {
  DashboardOutlined
};

const dashboard: NavigationItem = {
  id: 'group-dashboard',
  title: 'Home',
  type: 'group',
  children: [
    {
      id: 'dashboard',
      title: 'Dashboard',
      type: 'item',
      url: '/dashboard',
      icon: icons.DashboardOutlined,
      breadcrumbs: false
    }
  ]
};

export default dashboard;
