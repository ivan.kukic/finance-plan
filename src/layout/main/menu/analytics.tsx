import { FundViewOutlined } from '@ant-design/icons';

import { NavigationItem } from "./Menu";

const icons = {
  FundViewOutlined
};

const analytics: NavigationItem = {
  id: 'group-analytics',
  title: 'Analytics',
  type: 'group',
  children: [
    {
      id: 'analytics',
      title: 'Analytics',
      type: 'item',
      url: '/analytics',
      icon: icons.FundViewOutlined,
      breadcrumbs: false
    }
  ]
};

export default analytics;