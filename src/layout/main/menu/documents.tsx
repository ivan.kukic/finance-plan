import { FolderOutlined } from '@ant-design/icons';

import { NavigationItem } from "./Menu";

const icons = {
  FolderOutlined
};

const documents: NavigationItem = {
  id: 'group-documents',
  title: 'Documents',
  type: 'group',
  children: [
    {
      id: 'Documents',
      title: 'Documents',
      type: 'item',
      url: '/documents',
      icon: icons.FolderOutlined,
      breadcrumbs: false
    }
  ]
};

export default documents;
