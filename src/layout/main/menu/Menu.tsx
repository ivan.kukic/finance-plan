import { ForwardRefExoticComponent, RefAttributes } from "react";
import { ChipProps } from "@mui/material";
import { AntdIconProps } from "@ant-design/icons/lib/components/AntdIcon";

import dashboard from "./dashboard";
import finance from "./finance";
import documents from "./documents";
import category from "./category";
import analytics from "./analytics";

export interface ChildItem {
  id: string;
  title: string;
  type: string;
  url: string;
  icon: ForwardRefExoticComponent<Omit<AntdIconProps, 'ref'> & RefAttributes<HTMLSpanElement>>;
  breadcrumbs?: boolean;
  external?: boolean;
  target?: boolean;
  disabled?: boolean;
  chip?: ChipProps;
  children?: ChildItem[];
}

export interface NavigationItem {
  id: string;
  title: string;
  type: string;
  children: ChildItem[];
}

export interface MenuItems {
  items: NavigationItem[]
}

const menuItems: MenuItems = {
  items: [dashboard, finance, category, documents, analytics]
};

export default menuItems;
