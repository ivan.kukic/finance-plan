import { BarChartOutlined, RiseOutlined } from '@ant-design/icons';

import { NavigationItem } from "./Menu";

const icons = {
  BarChartOutlined,
  RiseOutlined
};

const finance: NavigationItem = {
  id: 'group-finance',
  title: 'Finance',
  type: 'group',
  children: [
    {
      id: 'Finance',
      title: 'Finance Plan',
      type: 'item',
      url: '/finance',
      icon: icons.BarChartOutlined,
      breadcrumbs: false
    },
    {
      id: 'Goal',
      title: 'Goals',
      type: 'item',
      url: '/goals',
      icon: icons.RiseOutlined,
      breadcrumbs: false
    }
  ]
};

export default finance;
