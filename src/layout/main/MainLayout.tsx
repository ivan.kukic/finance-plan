import { useEffect, useState } from "react";
import { Outlet } from "react-router-dom";
import { Box, Toolbar, useMediaQuery, useTheme } from "@mui/material";

import Header from "./header/Header";
import MainDrawer from "./drawer";
import Breadcrumbs from "@components/utility/Breadcrumbs";
import navigation from '@layout/main/menu/Menu';
import { useAppDispatch, useAppSelector } from "@hooks/redux";
import { openDrawer, selectMenuState } from "@store/reducers/menu";

const MainLayout = () => {
  const theme = useTheme();
  const matchDownLG = useMediaQuery(theme.breakpoints.down('lg'));
  const dispatch = useAppDispatch();

  const {drawerOpen} = useAppSelector(selectMenuState);

  // drawer toggle
  const [open, setOpen] = useState(drawerOpen);
  const handleDrawerToggle = () => {
    setOpen(!open);
    dispatch(openDrawer({drawerOpen: !open}));
  };

  useEffect(() => {
    setOpen(!matchDownLG);
    dispatch(openDrawer({drawerOpen: !matchDownLG}));
  }, [matchDownLG]);

  useEffect(() => {
    if (open !== drawerOpen) setOpen(drawerOpen);
  }, [drawerOpen]);

  return (
    <Box sx={{display: 'flex', width: '100%'}} className="MainLayout">
      <Header open={open} handleDrawerToggle={handleDrawerToggle} />
      <MainDrawer open={open} handleDrawerToggle={handleDrawerToggle} />
      <Box component="main" sx={{width: '100%', flexGrow: 1, p: {xs: 2, sm: 3}}}>
        <Toolbar />
        <Breadcrumbs navigation={navigation} title />
        <Outlet />
      </Box>
    </Box>
  );
};

export default MainLayout;